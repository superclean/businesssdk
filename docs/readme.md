## 商业化模块SDK

当前版本1.0.0.005-SNAPSHOT

### SDK接入方式

在项目根目录的build.gradle中，添加maven仓库

```groovy
allprojects {
    maven {
        url "https://artifactory.minfindata.com/artifactory/mvn"
    }
}
```


在主应用的build.gradle中，添加依赖 

```java                       
implementation (cn.instreet:business-sdk:1.0.0.004-SNAPSHOT)

```

初始化SDK，在Application或者Activity中添加如下代码

```java
        BusinessSDK.InitSDK(MainActivity.this, new BusinessSDK.OnSDKInitListener() {
            // 当SDK初始化完成，会通过onInit方法传回已创建的实例。
            @Override
            public void onInit(BusinessSDK sdk) {
                MainActivity.this.sdk = sdk;
                sdk.enableLockScreen(R.mipmap.logo_clean, "Super Clean", CenterCustomView.class, BottomCustomView.class);
            }
        });
```

### 一、锁屏功能使用
锁屏功能最好能够让用户自行控制开关，因此建议在应用中添加锁屏功能的设置选项。例如《记事本》就有在设置中允许用户开关锁屏功能的选项。

![](images/lockscreen_pref.png)


锁屏功能的开启只需一行代码，支持主应用自定义Icon，标题和中间的视图和底部视图。

```java
// 开启锁屏，支持如下自定义设置
// 左上角图标资源id，默认为0
// 左上角标题，默认为空字符串
// 中间自定义View的class，需要为View类型的class，传null则不显示。
// 底部自定义View的class，需要为View类型的class，传null则不显示。
sdk.enableLockScreen(R.mipmap.logo_clean, "Super Clean", CenterCustomView.class, BottomCustomView.class);

// 关闭锁屏
sdk.disableLockScreen();

```

锁屏效果如下图

![](images/lockscreen.png)

### 二、应用安装和卸载监听功能使用
主应用会将想要监听的app包名列表传给sdk，sdk会开启服务监听指定应用是否安装，安装时会根据广告规则来显示弹窗广告。
代码如下

```java
// 开启应用监听，参数为想要监听的包名列表（自己的应用无法监听），当被监听的应用安装和卸载时，会触发广告。
sdk.listenAppChange(new String[]{"com.example.halor.myapplication", "cn.instreet.superclean"});
                    
// 关闭应用监听
sdk.disableListenAppChange();   
```


开启监听后，如果用户进行了指定应用的安装，会根据广告显示规则弹窗显示广告，如下图所示

![](images/appinstall.png)

### 三、来电提醒功能使用
主应用会监听手机的来电和挂断行为，然后根据广告规则来决定是否显示插屏或者弹窗广告。


```java
// 主应用可以通过参数来设置自定义来电提醒页面所要展示的自定义视图。
sdk.enablePhoneAlert(CustomView.class);
                    
// 关闭来电提醒
sdk.disablePhoneAlert();
```

### 电量监听
电量监听功能为默认开启功能，该功能会监听以下三种状态变化：

- 手机电量低于50%时，大于20%时，会提示电量较低。当手机电量低于20%时，会提示手机电量低。
- 充电器插拔时，会弹出提示。
- 以及当电话挂断时，如果电量低于50%，会弹出提示

![](images/battery_low.png)
![](images/battery_medium.png)
### 全能模块

全能模块包括Home按键事件的监听，网络连接事件的监听，耳机插拔事件的监听，蓝牙模块开关事件的监听。
这些功能通过远程参数，而不需要写代码来开启或者关闭，

### 广告模块
要访问AdvertiseManger模块，可以通过单例模式来使用，要注意判断AdvertiseManger初始化是否完成，
同时要注意需要在应用的主进程中对其调用。

``` java
	if (AdvertiseManager.isInit()) {
		AdvertiseManager adsManager = AdvertiseManager.getInstance();
		...
	}

```

1. 显示原生广告
首先判断原生广告是否有缓存，然后展示广告。如果想要把广告展示在某一个Activity中，则需要传入一个ViewGroup
作为原生广告的容器，如果想弹窗显示，则group的参数传为null。另外还可以选择想要展示的广告样式风格，共有四种风格可选:
MODE\_STYLE\_1, MODE\_STYL\_2, MODE\_STYLE\_3, MODE\_STYLE\_4，四种风格的广告示意图如下：

![](images/style1.png) 
![](images/style2.png)
![](images/style3.png) 
![](images/style4.png)

广告加载代码如下：

``` java
   // grou表示Activity中的一个View
	View group;
	if (AdvertiseManager.isInit()) {
		AdvertiseManager adsManager = AdvertiseManager.getInstance();
		if (adsManager.isNativeAdsCached()) {
		    // 
		    adsManager.showNativeAds(group, AdvertiseManager.MODE_STYLE_1);
		}
	}

```

2. 显示插屏广告
首先要判断插屏广告是否有缓存，然后再展示广告。插屏广告展示需要设置插屏的退出模式，如果为FrontAdvertiseActivity.MODE_EXIT，则广告退出仅会关闭Activity; 如果为FrontAdvertiseActivity.MODE_HIDE，则广告退出会关闭当前Activity并把应用切后台

``` java
   
	View group;
	if (AdvertiseManager.isInit()) {
		AdvertiseManager adsManager = AdvertiseManager.getInstance();
		if (adsManager.isInterstitialCached()) {
		    // 广告退出的模式

		    adsManager.showInterstitialAds(FrontAdvertiseActivity.MODE_EXIT);
		}
	}

```

### Demo代码
见[https://gitlab.minfindata.com/yihao/BusinessSDK/tree/master/BusinessSDK/businessdemo](https://gitlab.minfindata.com/yihao/BusinessSDK/tree/master/BusinessSDK/businessdemo)


### ReleaseNotes

####2018.12.21

version 1.0.0.001-SNAPSHOT

添加锁屏功能

####2018.12.27

version 1.0.0.002-SNAPSHOT

添加对应用安装和卸载的监听，并进行弹窗的功能。

添加了对悬浮窗权限的检测和申请。

####2019.1.7

version 1.0.0.003-SNAPSHOT

添加了Firebase远程配置的功能。

添加了来电提醒功能，以及前置插屏广告，后置插屏广告，原生广告的显示。

添加了对全能模块Home按键，蓝牙模块，网络连接，耳机插拔的监听，及相应广告的弹出显示。

优化了锁屏，应用安装对于进程被杀的处理。

####2019.1.15

version 1.0.0.004-SNAPSHOT

完善了应用安装监听和来电提醒的自动化兼容处理

广告模块增加了缓存和频度控制

其他优化和问题修复。

###2019.1.22

version 1.0.0.005-SNAPSHOT

增加了低电量和充电的提醒功能。

弹窗广告增加4种样式。

广告模块增加使用接口

### 已知问题
- 锁屏右上角菜单还无法使用。
- 弹窗未实现推荐App的功能。
- 按下菜单键也会触发Home键监听事件。
