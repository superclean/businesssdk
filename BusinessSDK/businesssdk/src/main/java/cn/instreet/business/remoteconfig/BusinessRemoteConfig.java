package cn.instreet.business.remoteconfig;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import cn.instreet.business.BuildConfig;
import cn.instreet.business.Constants;
import cn.instreet.business.R;
import cn.instreet.business.advertise.SplashResourceInfo;
import cn.instreet.business.utils.Asserts;
import cn.instreet.business.utils.BusinessLog;

/**
 * 商业化模块远程配置接口
 * created by yihao 2019/1/9
 */
public class BusinessRemoteConfig {
    private static FirebaseRemoteConfig config;

    public static void init(Context context) {
        if (null != config) {
            return;
        }
        Asserts.checkNotNull(context);
        FirebaseApp.initializeApp(context);
        config = FirebaseRemoteConfig.getInstance();
        // 开启开发者模式，可以缩短刷新配置缓存的时间。默认的缓存刷新时间为12小时
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG).build();
        config.setConfigSettings(configSettings);
        config.setDefaults(R.xml.remote_config_defaults);


    }

    public static void updateConfig() {
        // 在debug模式下，设置缓存失效时间为30秒
        config.fetch(BuildConfig.DEBUG ? 30 : 3600).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Log.d(Constants.TAG, "fetch  success");
                    config.activateFetched();
                    if (BuildConfig.DEBUG) {
                        BusinessLog.d("switch_embeded_ads " + config.getBoolean("switch_embeded_ads"));
                    }
                } else {
                    Log.d(Constants.TAG, "fetch  fail");
                }
            }
        });
    }

    /**
     * 返回插屏广告id
     *
     * @return
     */
    public static String getInterstitialAdId() {
        if (null != config) {
            return config.getString("interstitial_ad_id");
        } else {
            return "unknown";
        }
    }

    /**
     * 返回原生广告id
     *
     * @return
     */
    public static String getNativeAdId() {
        if (null != config) {
            return config.getString("native_ad_id");
        } else {
            return "unknown";
        }
    }

    /**
     * 返回admob app id
     *
     * @return
     */
    public static String getAdmobAppId() {
        if (null != config) {
            return config.getString("admob_app_id");
        } else {
            return "unknown";
        }
    }

    /**
     * 返回锁屏广告是否开启
     *
     * @return
     */
    public static boolean isLockscreenAdsOn() {
        if (BuildConfig.testMode) {
            return true;
        }
        if (null != config) {
            return config.getBoolean("switch_lockscreen_ads");
        } else {
            return false;
        }
    }


    /**
     * 返回来电广告模式
     * 可选值为 "front","back","native"
     *
     * @return
     */
    public static String getPhoneAdsMode() {
        if (BuildConfig.testMode) {
            String result = "none";
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("mnt/sdcard/phoneAdsMode.txt"), "UTF-8"));
                result = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;

        } else {
            if (null != config) {
                return config.getString("phonecall_ads_mode");
            }
        }
        return "none";
    }

    /**
     * 改变当前来电广告模式，仅供测试状态使用
     *
     * @param mode 可选值为 "front","back","native"
     * @return
     */
    public static void changePhoneAdsMode(String mode) {
        if (BuildConfig.testMode) {
            try {
                FileWriter fileWriter = new FileWriter("mnt/sdcard/phoneAdsMode.txt");
                fileWriter.write(mode);
                fileWriter.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 获取包安装监听时间间隔，单位为秒
     *
     * @return
     */
    public static long getPackageListenIntervalTime() {
        if (BuildConfig.testMode) {
            return 5;
        } else {
            if (null != config) {
                return config.getLong("package_listen_interval_time");
            }
        }
        return 5;
    }

    /**
     * 获取闪屏资源
     *
     * @return
     */
    public static SplashResourceInfo getSplashResourceInfo() {
        if (null != config) {
            SplashResourceInfo info = new Gson().fromJson(config.getString("splash_resource"),
                    SplashResourceInfo.class);
            return info;
        }
        return null;
    }

    /**
     * 返回广告展示间隔，单位为秒
     *
     * @return
     */
    public static long getAdsDisplayInterval() {
        if (!BuildConfig.DEBUG && null != config) {
            return config.getLong("ads_show_interval");
        }
        return 10;
    }

    public static boolean isEmbededAdsEnable() {
        if (BuildConfig.DEBUG) {
            return true;
        } else {
            if (null != config) {
                return config.getBoolean("switch_embeded_ads");
            }
            return false;
        }
    }

    public static boolean isInterstitialAdsEnable() {
        if (BuildConfig.DEBUG) {
            return true;
        } else {
            if (null != config) {
                return config.getBoolean("switch_interstitial_ads");
            }
            return false;
        }
    }

    public static boolean isPopupAdsEnable() {
        if (BuildConfig.DEBUG) {
            return true;
        } else {
            if (null != config) {
                return config.getBoolean("switch_popup_ads");
            }
            return false;
        }
    }
}
