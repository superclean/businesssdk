package cn.instreet.business.advertise;

/**
 * 闪屏资源信息
 * Created by 易昊 on 2019/2/11.
 */
public class SplashResourceInfo {
    // 滑动页所用的图片地址
    public String[] imageUrls;

    // 图片md5值
    public String[] imageMD5s;

    // 视频页所用的视频文件地址
    public String videoUrl;

    // 视频md5值
    public String videoMD5;

    // 资源版本号
    public int versionCode;
}
