package cn.instreet.business.advertise;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * created by yihao 2019/6/13
 */
public class MediumNativeAdView extends RelativeLayout {
    public MediumNativeAdView(Context context) {
        super(context);
    }

    public MediumNativeAdView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MediumNativeAdView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MediumNativeAdView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
