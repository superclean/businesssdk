package cn.instreet.business.packages;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import cn.instreet.business.BuildConfig;
import cn.instreet.business.BusinessSDK;
import cn.instreet.business.Constants;
import cn.instreet.business.IGlobal;
import cn.instreet.business.advertise.AdvertiseManager;
import cn.instreet.business.global.GlobalService;
import cn.instreet.business.remoteconfig.BusinessRemoteConfig;
import cn.instreet.business.utils.BusinessLog;
import cn.instreet.business.utils.Utils;

/**
 * 安装包变化监听广播接收器
 * created by yihao 2018/12/24
 */
public class PackageChangeReceiver extends BroadcastReceiver {
    private static final int BATTERY_LEVEL_M = 30;
    private Context context;
    //    private AppChangeListener listener;
    private final ArrayList<String> interestPackageNames;
    private final HashMap<String, Long> packageUninstallTimes = new HashMap<>();
    private long intervalTime;
    private Timer timer;
    private TimerTask timerTask;
    private final long scanInterval = 10000;
    private AppInfo[] recommendApps;
    private IGlobal global;
    private ServiceConnection serviceConnection;
    //    private static class MyHandler extends Handler{
//        @Override
//        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
//            if(msg.what == 1){
//
//            }
//        }
//    }
    private boolean isRegistered = false;
    private SharedPreferences sharedPreferences;
    private boolean isReceiveAdded = false;
    private boolean isReceiveRemoved = false;
    private Handler handler;

    public PackageChangeReceiver() {
        this.interestPackageNames = new ArrayList<>();
        intervalTime = BusinessRemoteConfig.getPackageListenIntervalTime();
        handler = new Handler();
    }

    /**
     * 注册监听器
     *
     * @param context
     */
    public void register(Context context) {
        if (null == context) {
            throw new IllegalArgumentException("context is null");
        }
        if (isRegistered) {
            return;
        }
        sharedPreferences = context.getSharedPreferences(Constants.PREF_NAME_BUSINESS, Context.MODE_PRIVATE);
        Set<String> pkgNames = sharedPreferences.getStringSet(Constants.PREF_KEY_PACKAGE_NAMES, new HashSet<String>());
        if (null != pkgNames) {
            interestPackageNames.clear();
            interestPackageNames.addAll(pkgNames);
        }


        isRegistered = true;
        this.context = context;
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        intentFilter.addAction(Intent.ACTION_PACKAGE_FULLY_REMOVED);
        intentFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        intentFilter.addDataScheme("package");
        context.registerReceiver(this, intentFilter);


        Intent intent = new Intent(context, GlobalService.class);
        context.bindService(intent, serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                global = IGlobal.Stub.asInterface(service);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        }, Context.BIND_AUTO_CREATE);
    }

    /**
     * 取消注册
     */
    public void unregister() {
        if (!isRegistered) {
            return;
        }
        isRegistered = false;
        context.unregisterReceiver(this);
        context.unbindService(serviceConnection);
        timer.cancel();
        timerTask.cancel();
    }

    /**
     * 监听应用包安装或者卸载
     *
     * @param packageNames 包名列表
     * @param listener     监听回调
     */
    public void listenPackageChanged(String[] packageNames, AppChangeListener listener) {
        if (null != packageNames && null != listener) {
            interestPackageNames.addAll(Arrays.asList(packageNames));
//            this.listener = listener;
        }
    }

    public void setInterestPackageNames(String[] packageNames) {
        Set<String> pkgNames = sharedPreferences.getStringSet(
                Constants.PREF_KEY_PACKAGE_NAMES, new HashSet<String>());
        if (null != pkgNames) {
            interestPackageNames.clear();
            interestPackageNames.addAll(pkgNames);
        }
    }

    public void setIntervalTime(long time) {
        intervalTime = time;
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        BusinessLog.d("PackageChangeReceiver receive intent");
        if (null == intent.getData()) {
            return;
        }
        Uri uri = intent.getData();
        if (null == uri) {
            return;
        }
        final String packageName = uri.getSchemeSpecificPart();
        if (TextUtils.isEmpty(packageName)) {
            return;
        }

        if (Intent.ACTION_PACKAGE_ADDED.equals(intent.getAction())) {
            BusinessLog.d("package added");
            isReceiveAdded = true;

        } else if (Intent.ACTION_PACKAGE_REMOVED.equals(intent.getAction())) {
            BusinessLog.d("package removed");
            isReceiveRemoved = true;
        }

        if (isReceiveAdded ^ isReceiveRemoved) {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(isReceiveAdded ^ isReceiveRemoved){
                        BusinessSDK.getInstance().addPackage(packageName);
                        String appName = Utils.getAppName(context, packageName);
                        Intent newIntent = new Intent(isReceiveAdded?BusinessSDK.ACTION_PACKAGE_ADDED:BusinessSDK.ACTION_PACKAGE_REMOVED);
                        newIntent.putExtra("package_name", packageName);
                        newIntent.putExtra("app_name", appName);
                        LocalBroadcastManager.getInstance(context).sendBroadcast(newIntent);
                    }
                    isReceiveAdded = false;
                    isReceiveRemoved = false;
                }
            }, 500);
        }


    }

    private boolean isInterestAppOnDevice() {
        List<PackageInfo> packages = getAppList();
        for (PackageInfo info : packages) {
            for (String interestPackageName : interestPackageNames) {
                if (interestPackageName.equals(info.packageName)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void setRecommendApps(AppInfo[] recommendApps) {
        this.recommendApps = recommendApps;
    }

    private void popupWindow() {
        AdvertiseManager.showNativeAdsRemote(context);
    }

    private List<PackageInfo> getAppList() {
        PackageManager pm = context.getPackageManager();
        return pm.getInstalledPackages(0);
    }

    /**
     * 判断当前是否允许弹窗
     *
     * @return
     */
    private boolean isPopupUnavailable() {
        if (BuildConfig.DEBUG) {
            return false;
        }
        if (null != global) {
            try {
                if (global.isCharging() || global.isLockScreenOn() || global.isOnCalling() ||
                        global.getBatteryLevel() < BATTERY_LEVEL_M) {
                    return true;
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            return false;
        } else {
            return false;
        }
    }

    private boolean packageOnDevice(String packageName) {
        List<PackageInfo> packages = getAppList();
        for (PackageInfo info : packages) {
            if (packageName.equals(info.packageName)) {
                return true;
            }
        }
        return false;
    }

    private boolean isEnable() {
        return sharedPreferences.getBoolean(Constants.PREF_KEY_PACKAGE, false);
    }
}
