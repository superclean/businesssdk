package cn.instreet.business.advertise;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;

import cn.instreet.business.BuildConfig;
import cn.instreet.business.Constants;
import cn.instreet.business.IGlobal;
import cn.instreet.business.R;
import cn.instreet.business.battery.BatteryReminderView;
import cn.instreet.business.global.GlobalService;
import cn.instreet.business.remoteconfig.BusinessRemoteConfig;
import cn.instreet.business.utils.Asserts;
import cn.instreet.business.utils.BusinessLog;
import cn.instreet.business.utils.SDKPopupWindow;
import ezy.assist.compat.SettingsCompat;

import static cn.instreet.business.lockscreen.LockScreen.ACTION_LOCKSCREEN_OFF;
import static cn.instreet.business.lockscreen.LockScreen.ACTION_LOCKSCREEN_ON;

/**
 * 广告管理器，必须在主进程中创建
 * created by yihao 2019/1/2
 * <p>
 * 如果在主应用进程显示广告，直接调用{@link #showInterstitialAds(String)}来显示插屏广告。调用{@link #showNativeAds(ViewGroup)}
 * 来显示原生广告。如果在其他进程显示广告，需要发送广播。
 * <p>
 * https://codelabs.developers.google.com/codelabs/admob-native-advanced-feed-android/index.html?hl=zh-cn#13
 * <p>
 * 注意，此模块内存占用较高，约40M
 *
 * 该模块比较臃肿，后期会拆分成多个类型的AdsManager，以支持Native，Banner，Interstitial类型的广告
 */

@Deprecated
public class AdvertiseManager {
    // 由于Admob广告不允许多进程调用，所以只允许在主应用进程中弹出广告，其他进程想要拉起广告可以通过广播的方式
    // 展示插屏广告的ACTION
    private static final String ACTION_SHOW_INTER_ADS = "business.intent.action.show_inter_ads";

    // 展示原生广告的ACTION
    private static final String ACTION_SHOW_NATIVE_ADS = "business.intent.action.show_native_ads";

    // 展示电池提醒的ACTION
    private static final String ACTION_SHOW_BATTERY_REMINDER = "business.intent.action.show_battery_reminder";

    // 展示内存占用的ACTION
    private static final String ACTION_SHOW_MEMORY_REMINDER = "business.intent.action.show_memory_reminder";

    // 电池模式
    public static final String EXTRA_BATTERY_MODE = "cn.instreet.advertise.extra_battery_mode";

    // 电量字段
    public static final String EXTRA_BATTERY_LEVEL = "cn.instreet.advertise.extra_battery_level";

    // 充电器类型
    private static final String EXTRA_CHARGER_TYPE = "cn.instreet.advertise.extra_charger_type";

    // 内存占比
    private static final String EXTRA_MEMORY_PERCENT = "cn.instreet.advertise.extra_memory_percent";

    // 进程数
    private static final String EXTRA_PROCESS_COUNT = "cn.instreet.advertise.extra_process_count";


    public static final String EXTRA_EXIT_MODE = "cn.instreet.advertise.extra_exit_mode";
    public static final String MODE_EXIT = "exit";
    public static final String MODE_HIDE = "hide";

    public static final int MODE_STYLE_1 = 0;
    public static final int MODE_STYLE_2 = 1;
    public static final int MODE_STYLE_3 = 2;
    public static final int MODE_STYLE_4 = 3;
    public static final int MODE_STYLE_5 = 4;

    private static final int MAX_RETRY_TIMES = 3;
    private static long ADS_DISPLAY_INTERVAL;
    private static final int BATTERY_LEVEL_M = 50;
    private int interstitialRetryTimes = 0;
    private int nativeRetryTimes = 0;
    private long lastInterstitialDisplayTime = 0;
    private long lastNativeDisplayTime = 0;

    private static AdvertiseManager instance = null;
    private PublisherInterstitialAd interstitialAd;
    private AdLoader nativeAdLoader;
    private NativeAdWrapper nativeAdsView;
    private UnifiedNativeAd nativeAds;
    private final Context context;
    private SDKPopupWindow popupWindow;
    private IGlobal global;
    private ServiceConnection serviceConnection;
    private SDKPopupWindow batteryPopupWindow;
    private boolean isLockScreenOn = false;
    private Handler handler;
    private InitTask task;

    private static class InitTask extends AsyncTask {
        private AdvertiseManager manager;

        public InitTask(AdvertiseManager manager) {
            this.manager = manager;
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            MobileAds.initialize(manager.context, "ca-app-pub-4414232724432396~4174842405");//BusinessRemoteConfig.getAdmobAppId());
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            manager.config();
        }
    }


    private AdvertiseManager(Context context) {
        Asserts.checkNotNull(context);
        this.context = context.getApplicationContext();
        handler = new Handler();

        task = new InitTask(this);

        task.execute();
    }


    public static boolean isInit() {
        if (null != instance) {
            return (null != instance.interstitialAd);
        }
        return false;
    }

    public static void init(final Context context) {
        Asserts.checkNotNull(context);
        instance = new AdvertiseManager(context);
    }

    private void config() {
        bindGlobalService();

        initInterstitialAds();

        loadInterstitialAds();

        initNativeAds();
//
        loadNativeAds();

        listenRemoteInvoke();
    }

    public void release() {
        context.unbindService(serviceConnection);
        instance = null;
    }

    public static AdvertiseManager getInstance() {
        return instance;
    }

    public void loadInterstitialAds() {
        interstitialRetryTimes = 0;
        if (null != interstitialAd && !interstitialAd.isLoading() && !isInterstitialCached()) {
            Log.d(Constants.TAG, "load interstitial ads");
            requestInterstitialAds();
        }
    }


    public void loadNativeAds() {
        nativeRetryTimes = 0;
        if (null != nativeAdLoader && !nativeAdLoader.isLoading() && !isNativeAdsCached()) {
            requestNativeAds();
        }
    }

    /**
     * 显示插屏广告
     *
     * @param exitMode 广告退出的模式
     *                 如果为{@link #MODE_EXIT}，则广告退出仅会关闭Activity
     *                 如果为{@link #MODE_HIDE}，则广告退出会关闭当前Activity并把应用切后台
     */
    public void showInterstitialAds(String exitMode) {
        if (!BusinessRemoteConfig.isInterstitialAdsEnable()) {
            return;
        }
        ADS_DISPLAY_INTERVAL = BusinessRemoteConfig.getAdsDisplayInterval();
        long now = System.currentTimeMillis();
        if ((now - lastInterstitialDisplayTime) < ADS_DISPLAY_INTERVAL * 1000) {
            return;
        }

        if (!isPhoneStateValid()) {
            return;
        }

        if (!isInterstitialCached()) {
            loadInterstitialAds();
            return;
        }

        if (interstitialAd.isLoaded()) {
            Intent intent = new Intent(context, FrontAdvertiseActivity.class);
            intent.putExtra("action", "show");
            intent.putExtra(EXTRA_EXIT_MODE, exitMode);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            lastInterstitialDisplayTime = now;
        } else if (!interstitialAd.isLoading()) {
            loadInterstitialAds();
        }
    }

    public boolean showNativeAds(final ViewGroup container) {
        return showNativeAds(container, MODE_STYLE_2);
    }

    /**
     * 显示原生广告
     *
     * @param container 如果不为空，则广告显示在container中，如果为空则弹窗显示
     * @param styleMode 广告模式，可选值为MODE_STYLE_1，MODE_STYLE_2，MODE_STYLE_3，MODE_STYLE_4
     */
    public boolean showNativeAds(final ViewGroup container, int styleMode) {
        BusinessLog.d("try show native ads");
        if (null == container && !SettingsCompat.canDrawOverlays(context)) {
            BusinessLog.w("can't show float window because SYSTEM_ALERT_WINDOW permission not granted");
            return false;
        }
        long now = System.currentTimeMillis();
        if (!isNativeAdsCached()) {
            BusinessLog.d("no ads cached");
            loadNativeAds();
            return false;
        }

        BusinessLog.d("show native ads");
        lastNativeDisplayTime = now;
        buildNativeAdsStyle(styleMode);
        if (null != container && BusinessRemoteConfig.isEmbededAdsEnable()) {
            ViewGroup group = ((ViewGroup) nativeAdsView.getParent());
            if (null != group) {
                group.removeAllViews();
            }
            container.addView(nativeAdsView);
            return true;
        } else if (null == container && BusinessRemoteConfig.isPopupAdsEnable()) {
            popupWindow.show(false);
            popupWindow.setView(nativeAdsView);
            return true;
        }
        return false;
    }

    private void buildNativeAdsStyle(int styleMode) {
        FrameLayout frameLayout = nativeAdsView.
                findViewById(R.id.fl_adplaceholder);
        UnifiedNativeAdView adView;
        switch (styleMode) {
            case MODE_STYLE_1:
                adView = (UnifiedNativeAdView) View.inflate(AdvertiseManager.this.context
                        , R.layout.ad_unified1, null);
                break;

            case MODE_STYLE_2:
                adView = (UnifiedNativeAdView) View.inflate(AdvertiseManager.this.context
                        , R.layout.ad_unified2, null);
                break;

            case MODE_STYLE_3:
                adView = (UnifiedNativeAdView) View.inflate(AdvertiseManager.this.context
                        , R.layout.ad_unified3, null);
                break;

            case MODE_STYLE_4:
                adView = (UnifiedNativeAdView) View.inflate(AdvertiseManager.this.context
                        , R.layout.ad_unified4, null);
                break;
            case MODE_STYLE_5:
                adView = (UnifiedNativeAdView) View.inflate(AdvertiseManager.this.context
                        , R.layout.ad_unified5, null);
                break;
            default:
                adView = (UnifiedNativeAdView) View.inflate(AdvertiseManager.this.context
                        , R.layout.ad_unified2, null);
                break;
        }
//        if (styleMode == MODE_STYLE_5) {
//            nativeAdsView.populateUnifiedNativeAdViewPureText(nativeAds, adView);
//        } else {
//            nativeAdsView.populateUnifiedNativeAdView(nativeAds, adView);
//        }
        frameLayout.removeAllViews();
        frameLayout.addView(adView);

    }

    public static void showBatteryReminderRemote(Context context, int mode, int batteryLevel, int chargerType) {
        Asserts.checkNotNull(context);
        Intent intent = new Intent(ACTION_SHOW_BATTERY_REMINDER);
        intent.putExtra(EXTRA_BATTERY_MODE, mode);
        intent.putExtra(EXTRA_BATTERY_LEVEL, batteryLevel);
        intent.putExtra(EXTRA_CHARGER_TYPE, chargerType);
        context.sendBroadcast(intent);
    }

    public static void showMemoryReminderRemote(Context context, int memoryPercent, int processCount) {
        Asserts.checkNotNull(context);
        Intent intent = new Intent(ACTION_SHOW_MEMORY_REMINDER);
        intent.putExtra(EXTRA_MEMORY_PERCENT, memoryPercent);
        intent.putExtra(EXTRA_PROCESS_COUNT, processCount);
        context.sendBroadcast(intent);
    }

    /**
     * 跨进程显示插屏广告
     *
     * @param context  句柄
     * @param exitMode 退出模式 {@link #MODE_EXIT} {@link #MODE_HIDE}
     */
    public static void showInterstitialAdsRemote(Context context, String exitMode) {
        Asserts.checkNotNull(context);
        Intent intent = new Intent(ACTION_SHOW_INTER_ADS);
        intent.putExtra(EXTRA_EXIT_MODE, exitMode);
        context.sendBroadcast(intent);
    }

    /**
     * 跨进程显示原生广告
     *
     * @param context
     * @param styleMode 广告风格
     */
    public static void showNativeAdsRemote(Context context, int styleMode) {
        Asserts.checkNotNull(context);
        Intent intent = new Intent(ACTION_SHOW_NATIVE_ADS);
        intent.putExtra("styleMode", styleMode);
        context.sendBroadcast(intent);
    }

    /**
     * 跨进程显示原生广告
     *
     * @param context
     */
    public static void showNativeAdsRemote(Context context) {
        showNativeAdsRemote(context, MODE_STYLE_2);
    }

    /**
     * 判断插屏广告是否缓存
     *
     * @return
     */
    public boolean isInterstitialCached() {
        if (BuildConfig.testMode) {
            return true;
        }

        if (null != interstitialAd) {
            BusinessLog.d("isInterstitialCached " + interstitialAd.isLoaded());
            return interstitialAd.isLoaded();
        } else {
            return false;
        }
    }

    /**
     * 判断原生广告是否缓存
     *
     * @return
     */
    public boolean isNativeAdsCached() {
        return null != nativeAds;
    }

    PublisherInterstitialAd getAdsHandle() {
        return interstitialAd;
    }

    private void requestInterstitialAds() {
        PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
        interstitialAd.loadAd(adRequest);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                BusinessLog.d("on interstitial load failed errorCode " + errorCode);
                if (BuildConfig.DEBUG) {
                    Toast.makeText(AdvertiseManager.this.context, "Failed to load native ad errorCode "
                            + errorCode, Toast.LENGTH_SHORT).show();
                }
                if (interstitialRetryTimes <= MAX_RETRY_TIMES) {
                    BusinessLog.d("retry loading interstitial ads times " + interstitialRetryTimes);
                    interstitialRetryTimes++;
                    requestInterstitialAds();
                }
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                BusinessLog.d("on interstitial ads loaded");
            }
        });
    }

    private void cacheInterstitialAds() {
        BusinessLog.d("interstitial ads cached");
    }

    private boolean isPhoneStateValid() {
        if (BuildConfig.DEBUG) {
            return true;
        }
        if (null != global) {
            try {
                if (global.isCharging() || global.isOnCalling() ||
                        global.getBatteryLevel() < BATTERY_LEVEL_M || isLockScreenOn) {
                    return false;
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            return true;
        } else {
            return false;
        }
    }

    private void listenRemoteInvoke() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_SHOW_INTER_ADS);
        filter.addAction(ACTION_SHOW_NATIVE_ADS);
        filter.addAction(ACTION_SHOW_BATTERY_REMINDER);
        filter.addAction(ACTION_SHOW_MEMORY_REMINDER);
        this.context.registerReceiver(new AdsBroadcastReceiver(), filter);
    }

    private void initInterstitialAds() {
        String adUnitId;
        if (!"unknown".equals(adUnitId = BusinessRemoteConfig.getInterstitialAdId())) {
            interstitialAd = new PublisherInterstitialAd(this.context);
            interstitialAd.setAdUnitId(adUnitId);
            BusinessLog.d("Interstitial ads init success!");
        } else {
            BusinessLog.w("Interstitial ads init failed!");
        }
    }

    private void initNativeAds() {
        popupWindow = new SDKPopupWindow(this.context);

        AdLoader.Builder builder = new AdLoader.Builder(context, "ca-app-pub-4414232724432396/1785364667");//BusinessRemoteConfig.getNativeAdId());

        builder.forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
            @Override
            public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
//                BusinessLog.d("Native ads loaded");
                nativeAds = unifiedNativeAd;
            }
        });
        NativeAdOptions adOptions = new NativeAdOptions.Builder().setAdChoicesPlacement(NativeAdOptions.ADCHOICES_TOP_RIGHT).build();
        builder.withNativeAdOptions(adOptions);

        nativeAdLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                BusinessLog.d("Failed to load native ad: " + errorCode);
                if (BuildConfig.DEBUG) {
                    Toast.makeText(AdvertiseManager.this.context, "Failed to load native ad: "
                            + errorCode, Toast.LENGTH_SHORT).show();
                }
                if (nativeRetryTimes <= MAX_RETRY_TIMES) {
                    BusinessLog.d("retry loading native ads times " + nativeRetryTimes);
                    nativeRetryTimes++;
                    requestNativeAds();
                }

            }

            @Override
            public void onAdClicked() {
                super.onAdClicked();
            }
        }).build();
        nativeAdsView = new NativeAdWrapper(context);
    }

    private void requestNativeAds() {
        nativeAds = null;
        nativeAdLoader.loadAd(new AdRequest.Builder().build());
    }


    private class AdsBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_SHOW_NATIVE_ADS.equals(action)) {
                showNativeAds(null);
            } else if (ACTION_SHOW_INTER_ADS.equals(action)) {
                String exitMode = intent.getStringExtra(EXTRA_EXIT_MODE);
                showInterstitialAds(exitMode);
            } else if (ACTION_SHOW_BATTERY_REMINDER.equals(action)) {
                showBatteryReminder(intent.getIntExtra(EXTRA_BATTERY_MODE, 0),
                        intent.getIntExtra(EXTRA_BATTERY_LEVEL, 0),
                        intent.getIntExtra(EXTRA_CHARGER_TYPE, 0));
            } else if (ACTION_SHOW_MEMORY_REMINDER.equals(action)) {
                showMemoryReminder(intent.getIntExtra(EXTRA_MEMORY_PERCENT, 50),
                        intent.getIntExtra(EXTRA_PROCESS_COUNT, 10));
            }
        }
    }

    private void showMemoryReminder(int memoryPercent, int processCount) {
        if (!BusinessRemoteConfig.isPopupAdsEnable()) {
            return;
        }
        if (!SettingsCompat.canDrawOverlays(context)) {
            BusinessLog.w("can't show float window because SYSTEM_ALERT_WINDOW permission not granted");
            return;
        }
        long now = System.currentTimeMillis();
        ADS_DISPLAY_INTERVAL = BusinessRemoteConfig.getAdsDisplayInterval();
        if ((now - lastNativeDisplayTime) < ADS_DISPLAY_INTERVAL * 1000) {
            return;
        }
        if (null != batteryPopupWindow) {
            return;
        }

        if (isLockScreenOn) {
            return;
        }

        BusinessLog.d("show memory reminder");
        lastNativeDisplayTime = now;
        batteryPopupWindow = new SDKPopupWindow(context);
        BatteryReminderView view = new BatteryReminderView(context);
        view.setBatteryReminderCallback(new BatteryReminderView.BatteryReminderCallback() {
            @Override
            public void onClose() {
                batteryPopupWindow.dismiss();
                batteryPopupWindow = null;
            }

            @Override
            public void onOptimize() {
                batteryPopupWindow.dismiss();
                batteryPopupWindow = null;
            }
        });
        view.setMemoryUsingHighMode(memoryPercent, processCount);
        batteryPopupWindow.setView(view);
        batteryPopupWindow.show(false);

        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (null == batteryPopupWindow) {
                    return;
                }
                batteryPopupWindow.dismiss();
                batteryPopupWindow = null;
            }
        }, 10000);
    }

    private void showBatteryReminder(int mode, int batteryLevel, int chargerType) {
        BusinessLog.d("show battery reminder");
        if (!BusinessRemoteConfig.isPopupAdsEnable()) {
            return;
        }

        if (!SettingsCompat.canDrawOverlays(context)) {
            BusinessLog.w("can't show float window because SYSTEM_ALERT_WINDOW permission not granted");
            return;
        }
        long now = System.currentTimeMillis();
        ADS_DISPLAY_INTERVAL = BusinessRemoteConfig.getAdsDisplayInterval();
        if ((now - lastNativeDisplayTime) < ADS_DISPLAY_INTERVAL * 1000) {
            return;
        }
        if (null != batteryPopupWindow) {
            return;
        }
        if (isLockScreenOn) {
            return;
        }
        lastNativeDisplayTime = now;
        batteryPopupWindow = new SDKPopupWindow(context);
        BatteryReminderView view = new BatteryReminderView(context);
        view.setBatteryReminderCallback(new BatteryReminderView.BatteryReminderCallback() {
            @Override
            public void onClose() {
                batteryPopupWindow.dismiss();
                batteryPopupWindow = null;
            }

            @Override
            public void onOptimize() {
                batteryPopupWindow.dismiss();
                batteryPopupWindow = null;
            }
        });
        if (mode == BatteryReminderView.MODE_LOW_BATTERY) {
            view.setLowBatteryMode(batteryLevel);
        } else if (mode == BatteryReminderView.MODE_CHARGING) {
            view.setChargingMode(batteryLevel, chargerType);
        }
        batteryPopupWindow.setView(view);
        batteryPopupWindow.show(false);
    }

    private void bindGlobalService() {
        Intent intent = new Intent(context, GlobalService.class);
        context.bindService(intent, serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                global = IGlobal.Stub.asInterface(service);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        }, Context.BIND_AUTO_CREATE);

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_LOCKSCREEN_ON);
        filter.addAction(ACTION_LOCKSCREEN_OFF);
        context.registerReceiver(new LockscreenBroadCastReceiver(), filter);
    }

    private class LockscreenBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(null == action){
                return;
            }
            if (ACTION_LOCKSCREEN_ON.equals(action)) {
                isLockScreenOn = true;
            } else if (action.equals(ACTION_LOCKSCREEN_OFF)) {
                isLockScreenOn = false;
            }
        }
    }
}
