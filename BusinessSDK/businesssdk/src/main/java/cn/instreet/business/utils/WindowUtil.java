package cn.instreet.business.utils;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.view.View;
import android.view.WindowManager;

import cn.instreet.business.BuildConfig;

import static android.content.Context.WINDOW_SERVICE;

/**
 * created by yihao 2019/2/25
 */
public class WindowUtil {
    public static void popupWindow(Context context, View view, int width, int height){
        Asserts.checkNotNull(context);
        Asserts.checkNotNull(view);
        WindowManager.LayoutParams windowParams;
        int flag = BuildConfig.testMode?WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                :WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            windowParams = new WindowManager.LayoutParams(
                    width, height,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    flag,
                    PixelFormat.TRANSLUCENT);
        } else {
            windowParams = new WindowManager.LayoutParams(
                    width, height,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    flag,
                    PixelFormat.TRANSLUCENT);
        }
        WindowManager windowManager = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        windowManager.addView(view, windowParams);
    }
}
