package cn.instreet.business.lockscreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;

import java.util.List;

import cn.instreet.business.R;

/**
 * created by yihao 2019/2/20
 * <p>
 * 手势解锁页面，用于锁屏解锁和修改密码时进行验证两个场景
 */
public class PatternLockActivity extends Activity {
    private PatternLockView patternLockView;
    private TextView tvTips;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pattern_lock);
        patternLockView = findViewById(R.id.pattern_lock_view);
        patternLockView.addPatternLockListener(patternLockViewListener);
        patternLockView.setWrongStateColor(0xffff0000);
        tvTips = findViewById(R.id.tv_tips);
        Intent intent = getIntent();
        String mode = intent.getStringExtra("mode");
        if ("validate".equals(mode)) {
            tvTips.setText(R.string.pattern_validate_title);
        } else if ("unlock_screen".equals(mode)) {
            tvTips.setText(R.string.pattern_unlock_title);
        }
    }

    private PatternLockViewListener patternLockViewListener = new PatternLockViewListener() {
        @Override
        public void onStarted() {
            Log.d(getClass().getName(), "Pattern drawing started");
        }

        @Override
        public void onProgress(List<PatternLockView.Dot> progressPattern) {
        }

        @Override
        public void onComplete(List<PatternLockView.Dot> pattern) {
            String patternString = PatternLockUtils.patternToString(patternLockView, pattern);
            String saveString = getSharedPreferences("setting", MODE_PRIVATE).getString("patternCode", "");
            if (patternString.equals(saveString)) {
                tvTips.setText(R.string.unlock_success);
                setResult(RESULT_OK);
                finish();
            } else {
                patternLockView.setViewMode(PatternLockView.PatternViewMode.WRONG);
                tvTips.setText(R.string.pattern_wrong);
            }
        }

        @Override
        public void onCleared() {
        }
    };
}
