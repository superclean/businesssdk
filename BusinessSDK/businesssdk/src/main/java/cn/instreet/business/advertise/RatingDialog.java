package cn.instreet.business.advertise;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import cn.instreet.business.R;
import cn.instreet.business.utils.Asserts;
import cn.instreet.business.utils.CustomRatingBar;
import cn.instreet.business.utils.Utils;

/**
 * 评价弹窗
 * created by yihao 2019/2/15
 */
public class RatingDialog extends Dialog {

    private CustomRatingBar bar;
    private Float ratingCount = null;
    private Button btnOK;
    private Button btnCancel;
    private TextView tvDesc;
    private Handler handler;

    public interface OnRatingListener {

        void onDontAskAgain();

        void onNotNow();

        void onFeedback();

        void onRate(float ratingCount);

        void onCancel();
    }

    public RatingDialog(Context context, final OnRatingListener listener) {
        super(context, R.style.Theme_AppCompat_Dialog);
        Asserts.checkNotNull(listener);
        handler = new Handler();
        setContentView(R.layout.rating_dialog);
        Window window = getWindow();
        if (null != window) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        tvDesc = findViewById(R.id.tv_desc);
        tvDesc.setText(getContext().getString(R.string.rating_content, Utils.getAppName(getContext())));
        bar = findViewById(R.id.stars);
        bar.setOnRatingChangeListener(new CustomRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChange(float ratingCount) {
                listener.onRate(ratingCount);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dismiss();
                    }
                }, 500);


            }
        });
        btnOK = findViewById(R.id.btn_OK);
        btnOK.setText(getContext().getString(R.string.not_now));
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        btnCancel = findViewById(R.id.btn_Cancel);
        btnCancel.setText(getContext().getString(R.string.dont_ask_again));
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }
}
