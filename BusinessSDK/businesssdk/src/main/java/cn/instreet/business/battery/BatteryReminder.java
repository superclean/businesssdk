package cn.instreet.business.battery;

import android.content.Context;

import cn.instreet.business.advertise.AdvertiseManager;
import cn.instreet.business.utils.Asserts;
import cn.instreet.business.utils.SDKPopupWindow;

/**
 * 电池提醒模块
 * created by yihao 2019/1/16
 */
public class BatteryReminder {
    private Context context;
    private BatteryLevelObserver batteryLevelObserver;
    public BatteryReminder(Context context) {
        Asserts.checkNotNull(context);
        this.context = context.getApplicationContext();
    }

    public void listenBatteryChanged(BatteryLevelObserver.BatteryLevelListener levelListener){
        batteryLevelObserver = new BatteryLevelObserver(context, levelListener);
        batteryLevelObserver.start();
    }
}
