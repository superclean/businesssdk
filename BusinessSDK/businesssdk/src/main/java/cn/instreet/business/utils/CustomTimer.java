package cn.instreet.business.utils;

import android.os.Handler;
import android.os.Looper;

/**
 * created by yihao 2019/1/24
 */
public class CustomTimer {
    private final int STATE_RUN = 0;
    private final int STATE_PAUSE = 1;
    private final int STATE_STOP = 2;
    private long delay;
    private long period;
    private Runnable runnable;
    private Handler handler;
    private int state = STATE_STOP;
    private long periodBegin;
    private long periodElapsed;

    /**
     * @param delay    延迟执行的毫秒数
     * @param period   循环执行的毫秒数
     * @param runnable 要执行的任务
     */
    public CustomTimer(long delay, long period, Runnable runnable) {
        this.delay = delay;
        this.period = period;
        this.runnable = runnable;
        handler = new Handler(Looper.getMainLooper());
    }

    public void start() {
        state = STATE_RUN;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                loop.run();
            }
        }, delay);
    }

    public void stop() {
        state = STATE_STOP;
        handler.removeCallbacksAndMessages(null);
    }

    public void pause() {
        if (state == STATE_RUN) {
            handler.removeCallbacksAndMessages(null);
            periodElapsed = Math.min(period, System.currentTimeMillis() - periodBegin);
            state = STATE_PAUSE;
        }
    }

    public void resume() {
        if (state == STATE_PAUSE) {
            state = STATE_RUN;
            handler.postDelayed(loop, period - periodElapsed);
        }
    }

    private Runnable loop = new Runnable() {
        @Override
        public void run() {
            periodBegin = System.currentTimeMillis();
            handler.postDelayed(loop, period);
            runnable.run();
        }
    };
}
