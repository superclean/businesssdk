package cn.instreet.business.utils;

import android.Manifest;
import android.app.ActivityManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cn.instreet.business.BusinessSDK;
import pub.devrel.easypermissions.EasyPermissions;

import static android.content.Context.ACTIVITY_SERVICE;
import static android.content.pm.PackageManager.MATCH_UNINSTALLED_PACKAGES;

/**
 * Created by 易昊 on 2018/12/31.
 */
public class Utils {
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    public static int dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    public static float getEstimateBatteryDuration(Context context) {
        int screenWidth = getScreenWidth(context);
        Random random = new Random();
        if (screenWidth <= 240) {
            return 20 + random.nextFloat();
        } else if (screenWidth <= 320) {
            return 18 + random.nextFloat();
        } else if (screenWidth <= 480) {
            return 16 + random.nextFloat();
        } else if (screenWidth <= 720) {
            return 14 + random.nextFloat();
        } else if (screenWidth <= 1080) {
            return 12 + random.nextFloat();
        } else {
            return 10 + random.nextFloat();
        }
    }

    public static int getScreenWidth(Context context) {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;
    }

    public static int getUsedPercentValue(Context context) {
        String dir = "/proc/meminfo";
        try {
            FileReader fr = new FileReader(dir);
            BufferedReader br = new BufferedReader(fr, 2048);
            String memoryLine = br.readLine();
            String subMemoryLine = memoryLine.substring(memoryLine.indexOf("MemTotal:"));
            br.close();
            long totalMemorySize = Integer.parseInt(subMemoryLine.replaceAll("\\D+", ""));
            long availableSize = (long) (getMemInfo("MemFree")) + (long) (getMemInfo("Buffers"))
                    + (long) (getMemInfo("Cached"));
            int percent = (int) ((totalMemorySize - availableSize) / (float) totalMemorySize * 100);
            return percent;
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static float getMemInfo(String type) {
        try {
            FileReader reader = new FileReader("/proc/meminfo");
            BufferedReader buffer = new BufferedReader(reader, 4 * 1024);
            String str = null;
            while ((str = buffer.readLine()) != null) {
                if (str.contains(type)) {
                    break;
                }
            }
            buffer.close();
            if (TextUtils.isEmpty(str)) {
                return 0;
            }
            /* \\s表示   空格,回车,换行等空白符,
            +号表示一个或多个的意思     */
            if (TextUtils.isEmpty(str)) {
                return 0;
            }
            String[] array = str.split("\\s+");
            return Integer.valueOf(array[1]);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException exp) {
            exp.printStackTrace();
        }
        return 0;
    }

    /**
     * 获取最近一小时内正在运行的应用
     *
     * @param context
     * @return
     */
    public static List<String> getRecentRunningApps(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            UsageStatsManager manager = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
            if (manager == null) {
                return new ArrayList<>();
            }
            long now = System.currentTimeMillis();
            List<UsageStats> stats = manager.queryUsageStats(UsageStatsManager.INTERVAL_BEST, now - 3600 * 1000, now);
            if (stats == null || stats.isEmpty()) {
                return new ArrayList<>();
            }
            List<String> strings = new ArrayList<>();
            for (int i = 0; i < stats.size(); i++) {
                if ((now - stats.get(i).getLastTimeUsed()) < 3600 * 1000) {
                    strings.add(stats.get(i).getPackageName());
                }
            }
            return strings;
        } else {
            ActivityManager am = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> processes = am.getRunningAppProcesses();
            List<String> strings = new ArrayList<>();
            for (ActivityManager.RunningAppProcessInfo process : processes) {
                strings.add(process.processName);
            }
            return strings;
        }
    }

    public static long getAppRunningTimes(Context context, String packageName, long time) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            UsageStatsManager manager = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
            if (manager == null) {
                return 0;
            }
            long now = System.currentTimeMillis();
            List<UsageStats> stats = manager.queryUsageStats(UsageStatsManager.INTERVAL_BEST, now - time, now);
            if (stats == null || stats.isEmpty()) {
                return 0;
            }
            for (int i = 0; i < stats.size(); i++) {
                if (packageName.equals(stats.get(i).getPackageName())) {
                    return stats.get(i).getTotalTimeInForeground();
                }

            }
            return 0;
        } else {
            return 0;
        }
    }

    /**
     * get file md5
     *
     * @param file
     * @return md5 value
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    public static String getFileMD5(File file) throws NoSuchAlgorithmException, IOException {
        if (!file.isFile()) {
            return null;
        }
        MessageDigest digest;
        FileInputStream in;
        byte buffer[] = new byte[1024];
        int len;
        digest = MessageDigest.getInstance("MD5");
        in = new FileInputStream(file);
        while ((len = in.read(buffer, 0, 1024)) != -1) {
            digest.update(buffer, 0, len);
        }
        in.close();
        BigInteger bigInt = new BigInteger(1, digest.digest());
        return bigInt.toString(16);
    }

    public static synchronized String getAppName(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            if (null == packageManager) {
                return null;
            }
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);
            if (null == packageInfo) {
                return null;
            }
            int labelRes = packageInfo.applicationInfo.labelRes;
            return context.getResources().getString(labelRes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取手机厂商名称
     *
     * @return
     */
    public static String getDeviceBrand() {
        return Build.BRAND;
    }

    /**
     * 获取手机型号
     *
     * @return 手机型号
     */
    public static String getSystemModel() {
        return android.os.Build.MODEL;
    }

    public static Location getLocation(Context context) {
        Asserts.checkNotNull(context);
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        String locationProvider;
        if (providers.contains(LocationManager.NETWORK_PROVIDER)) {
            //如果是网络定位
            locationProvider = LocationManager.NETWORK_PROVIDER;
        } else if (providers.contains(LocationManager.GPS_PROVIDER)) {
            //如果是GPS定位
            locationProvider = LocationManager.GPS_PROVIDER;
        } else {
            return null;
        }
        Location location = null;
        if (EasyPermissions.hasPermissions(context, Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            location = locationManager.getLastKnownLocation(locationProvider);
        }
        return location;
    }

    /**
     * 根据包名获取App的Icon
     *
     * @param pkgName 包名
     */
    public static Drawable getAppIcon(Context context, String pkgName) {
        try {
            if (null != pkgName) {
                PackageManager pm = context.getPackageManager();
                ApplicationInfo info = pm.getApplicationInfo(pkgName, 0);
                return info.loadIcon(pm);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据包名获取App的名字
     *
     * @param pkgName 包名
     */
    public static String getAppName(Context context, String pkgName) {
        BusinessSDK sdk = BusinessSDK.getInstance();
        PackageManager pm = context.getPackageManager();
        ApplicationInfo info = null;
        if (null != sdk) {
            info = sdk.getApplicationInfo(pkgName);
        } else {
            try {
                PackageInfo packageInfo = pm.getPackageInfo(pkgName, MATCH_UNINSTALLED_PACKAGES);
                info = packageInfo.applicationInfo;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        if(null != info){
            return info.loadLabel(pm).toString();
        }
        return "";
    }
}
