package cn.instreet.business.utils;

import android.os.Looper;
import android.util.Log;

/**
 * created by yihao 2019/1/4
 */
public final class Asserts {
    public static void checkNull(Object var0) {
        if (var0 != null) {
            throw new IllegalArgumentException("non-null reference");
        }
    }

    public static void checkNotNull(Object var0) {
        if (var0 == null) {
            throw new IllegalArgumentException("null reference");
        }
    }

    public static void checkNotNull(Object var0, Object var1) {
        if (var0 == null) {
            throw new IllegalArgumentException(String.valueOf(var1));
        }
    }

    public static void checkState(boolean var0) {
        if (!var0) {
            throw new IllegalStateException();
        }
    }

    public static void checkState(boolean var0, Object var1) {
        if (!var0) {
            throw new IllegalStateException(String.valueOf(var1));
        }
    }

    public static void checkMainThread(String var0) {
        if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
            String var1 = String.valueOf(Thread.currentThread());
            String var2 = String.valueOf(Looper.getMainLooper().getThread());
            Log.e("Asserts", (new StringBuilder(57 + String.valueOf(var1).length() + String.valueOf(var2).length())).append("checkMainThread: current thread ").append(var1).append(" IS NOT the main thread ").append(var2).append("!").toString());
            throw new IllegalStateException(var0);
        }
    }

    public static void checkNotMainThread(String var0) {
        if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
            String var1 = String.valueOf(Thread.currentThread());
            String var2 = String.valueOf(Looper.getMainLooper().getThread());
            Log.e("Asserts", (new StringBuilder(56 + String.valueOf(var1).length() + String.valueOf(var2).length())).append("checkNotMainThread: current thread ").append(var1).append(" IS the main thread ").append(var2).append("!").toString());
            throw new IllegalStateException(var0);
        }
    }

    private Asserts() {
        throw new AssertionError("Uninstantiable");
    }
}
