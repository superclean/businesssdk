package cn.instreet.business.utils;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

/**
 * created by yihao 2019/2/19
 */
public class WeatherUtils {
    private static final String KEY = "d6d5f159d61f423d83360902191802";

    public interface WeatherCallback {
        void onResult(String result);
    }

    public static void getCurrentWeather(final Context context, final WeatherCallback callback) {
        Asserts.checkNotNull(context);
        Asserts.checkNotNull(callback);
        Location location = Utils.getLocation(context);
        if(null == location){
            return;
        }
        String URL = "http://api.apixu.com/v1/current.json?key=" + KEY + "&q=";
        URL += location.getLatitude() + "," + location.getLongitude();
        URL += "&lang=" + Locale.getDefault().getLanguage();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject current = response.getJSONObject("current");
                    if (null == current) {
                        callback.onResult(null);
                        return;
                    }
                    String tempC = current.getString("temp_c");
                    if (TextUtils.isEmpty(tempC)) {
                        callback.onResult(null);
                        return;
                    }
                    tempC += "\u2103";

                    JSONObject condition = current.getJSONObject("condition");
                    if (null == condition) {
                        callback.onResult(null);
                        return;
                    }
                    String textCond = condition.getString("text");
                    if(TextUtils.isEmpty(textCond)){
                        callback.onResult(null);
                        return;
                    }
                    callback.onResult(textCond + ", " + tempC);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(context, new HurlStack());
        requestQueue.add(request);
    }
}
