package cn.instreet.business.advertise;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

import com.google.android.gms.ads.AdListener;

import static cn.instreet.business.advertise.AdvertiseManager.EXTRA_EXIT_MODE;
import static cn.instreet.business.advertise.AdvertiseManager.MODE_EXIT;
import static cn.instreet.business.advertise.AdvertiseManager.MODE_HIDE;

/**
 * 前置插屏广告Activity，必须得和主应用在同一进程中
 */
public class FrontAdvertiseActivity extends Activity {

    private String exitMode = "exit";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (null != intent) {
            String action = intent.getStringExtra("action");
            if ("create".equals(action)) {
//                AdvertiseManager.attachToActivity(this);
                finish();
            } else if ("show".equals(action)) {
                if (AdvertiseManager.isInit()) {
                    String strExitMode = getIntent().getStringExtra(EXTRA_EXIT_MODE);
                    if (MODE_EXIT.equals(strExitMode) || MODE_HIDE.equals(strExitMode)) {
                        exitMode = strExitMode;
                    }
                    AdvertiseManager.getInstance().getAdsHandle().
                            setAdListener(new AdListener() {
                                              @Override
                                              public void onAdClosed() {
                                                  super.onAdClosed();
                                                  AdvertiseManager.getInstance().loadInterstitialAds();
                                                  if (MODE_EXIT.equals(exitMode)) {
                                                      finish();
                                                  } else if (MODE_HIDE.equals(exitMode)) {
                                                      // 关闭当前Activity，否则会覆盖上一个Activity
                                                      finish();
                                                      moveTaskToBack(true);
                                                  }

                                              }
                                          }
                            );
                    AdvertiseManager.getInstance().getAdsHandle().show();
                }
            }
        }

    }

    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (MODE_EXIT.equals(exitMode)) {
                finish();
            } else if (MODE_HIDE.equals(exitMode)) {
                moveTaskToBack(true);
            }
        }
        return true;
    }
}
