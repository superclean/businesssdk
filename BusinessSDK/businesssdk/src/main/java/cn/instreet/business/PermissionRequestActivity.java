package cn.instreet.business;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import cn.instreet.business.utils.PermissionUtil;
import ezy.assist.compat.SettingsCompat;
import pub.devrel.easypermissions.EasyPermissions;

import static android.provider.Settings.ACTION_USAGE_ACCESS_SETTINGS;
import static cn.instreet.business.Constants.TAG;

/**
 * created by yihao 2018/12/20
 * <p>
 * 透明页面，用于申请权限
 * <p>
 * TODO USAGE_STAT 增加提示
 * TODO 更换权限判断第三方库
 */
public class PermissionRequestActivity extends AppCompatActivity {
    public static final String ACTION_SEND_PERMISSION_GRANT_RESULT = "send_permission_grant_result";
    private static final int REQUEST_CODE = 1;

    private String[] permissionNames;
    private boolean hasRequestedFloatWindow = false;
    private String explain;
    private String rationale;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if (null != intent) {
            ArrayList<String> nameList = new ArrayList<>();
            String permissionName = intent.getStringExtra("permission_name");
            if (!TextUtils.isEmpty(permissionName)) {
                nameList.add(permissionName);
            }

            String[] names = intent.getStringArrayExtra("permission_names");
            if (null != names) {
                nameList.addAll(Arrays.asList(names));
            }
            this.permissionNames = nameList.toArray(new String[]{});

            rationale = intent.getStringExtra("rationale");

            requestPermission();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE:
                EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults);
                sendGrantedResult(permissionNames, grantResults);
                break;
            default:
                break;
        }
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v(TAG, "onActivityResult");
        finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onResume() {
        super.onResume();

    }

    private boolean containsUsageStatPermission() {
        for (String name : permissionNames) {
            if (name.equals(Manifest.permission.PACKAGE_USAGE_STATS)) {
                return true;
            }
        }
        return false;
    }

    private void sendGrantedResult(String permissionName, boolean isGranted) {
        Intent intent = new Intent(ACTION_SEND_PERMISSION_GRANT_RESULT);
        intent.putExtra("permission_name", permissionName);
        intent.putExtra("result", isGranted);
        sendBroadcast(intent);
    }

    private void sendGrantedResult(String[] permissionNames, int[] isGranted) {
        Intent intent = new Intent(ACTION_SEND_PERMISSION_GRANT_RESULT);
        intent.putExtra("permission_names", permissionNames);
        intent.putExtra("result", isGranted);
        sendBroadcast(intent);
    }

    private boolean containsFloatWindowPermission() {
        for (String name : permissionNames) {
            if ("float_window".equals(name)) {
                return true;
            }
        }
        return false;
    }

    private void requestPermission() {
        if (null == permissionNames) {
            return;
        }
        Intent intent;
        if (containsFloatWindowPermission()) {
            if (!hasRequestedFloatWindow) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                    intent.setData(Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, REQUEST_CODE);
                }
                hasRequestedFloatWindow = true;
            } else {
                sendGrantedResult("float_window", SettingsCompat.canDrawOverlays(this));
                finish();
            }
        } else if (containsUsageStatPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (PermissionUtil.hasUsageStatsPermission(this)) {
                    return;
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                intent = new Intent(ACTION_USAGE_ACCESS_SETTINGS);
                startActivityForResult(intent, REQUEST_CODE);
            }
        } else {
            EasyPermissions.requestPermissions(this, rationale, REQUEST_CODE, permissionNames);
        }
    }
}
