package cn.instreet.business.advertise;

import android.Manifest;
import android.content.Context;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import cn.instreet.business.utils.Asserts;
import cn.instreet.business.utils.BusinessLog;
import cn.instreet.business.utils.PermissionUtil;
import cn.instreet.business.utils.Utils;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * 闪屏资源下载器
 * created by yihao 2019/2/12
 */
public class SplashDownloader {

    private Context context;
    private SplashResourceInfo resourceInfo;
    private DownloadListener listener;
    private HashMap<String, FileDownloadRequest> requestHashMap;

    public interface DownloadListener {
        void onFinished(SplashResourceInfo info);

        void onFail();
    }

    public SplashDownloader(Context context, DownloadListener listener) {
        Asserts.checkNotNull(context);
        Asserts.checkNotNull(listener);
        this.context = context;
        this.listener = listener;
        resourceInfo = new SplashResourceInfo();
        requestHashMap = new HashMap<>();
        String directoryPath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator +
                context.getApplicationContext().getPackageName();
        File directory = new File(directoryPath);
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }

    public void startDownload(final String[] imageUrls, final String[] imageMD5s, final String videoUrl,
                              final String videoMD5, int versionCode) {
        if (imageUrls.length != imageMD5s.length) {
            return;
        }

        resourceInfo.imageUrls = new String[imageUrls.length];
        resourceInfo.versionCode = versionCode;

        if (!EasyPermissions.hasPermissions(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            PermissionUtil.requestPermissions(context, "We need write file permission to download splash resource file",
                    "We need write file permission to download splash resource file", new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}
                    , new PermissionUtil.PermissionCallback() {
                        @Override
                        public void onResult(String[] permission, int[] results) {
                            startImageDownload(imageUrls, imageMD5s);

                            startVideoDownload(videoUrl, videoMD5);
                        }
                    });
        } else {
            startImageDownload(imageUrls, imageMD5s);

            startVideoDownload(videoUrl, videoMD5);
        }

    }

    private void startImageDownload(final String[] imageUrls, final String[] imageMD5s) {
        for (int i = 0; i < imageUrls.length; i++) {
            final int index = i;
            FileDownloadRequest request = new FileDownloadRequest(Request.Method.GET, imageUrls[i], new Response.Listener<byte[]>() {
                @Override
                public void onResponse(byte[] response) {
                    requestHashMap.remove(imageUrls[index]);
                    String filePath = saveFile(imageUrls[index], response);
                    if (!isFileValidate(filePath, imageMD5s[index])) {
                        listener.onFail();
                        cancelTask();
                        return;
                    }
                    resourceInfo.imageUrls[index] = filePath;
                    if (isComplete()) {
                        saveInfo();
                        listener.onFinished(resourceInfo);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    BusinessLog.w("Splash download fail ");
                    error.printStackTrace();
                }
            }, null);

            RequestQueue requestQueue = Volley.newRequestQueue(context, new HurlStack());
            requestQueue.add(request);

            requestHashMap.put(imageUrls[i], request);
        }
    }

    private void startVideoDownload(final String videoUrl, final String videoMD5) {
        FileDownloadRequest request = new FileDownloadRequest(Request.Method.GET, videoUrl, new Response.Listener<byte[]>() {
            @Override
            public void onResponse(byte[] response) {
                requestHashMap.remove(videoUrl);
                String filePath = saveFile(videoUrl, response);
                if (!isFileValidate(filePath, videoMD5)) {
                    listener.onFail();
                    return;
                }
                resourceInfo.videoUrl = filePath;
                if (isComplete()) {
                    saveInfo();
                    listener.onFinished(resourceInfo);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                BusinessLog.w("Splash download fail ");
                error.printStackTrace();
            }
        }, null);
        RequestQueue requestQueue = Volley.newRequestQueue(context, new HurlStack());
        requestQueue.add(request);
        requestHashMap.put(videoUrl, request);
    }

    private void cancelTask() {
        for (FileDownloadRequest request : requestHashMap.values()) {
            request.cancel();
        }
    }

    private boolean isComplete() {
        if (TextUtils.isEmpty(resourceInfo.videoUrl)) {
            return false;
        }
        for (int i = 0; i < resourceInfo.imageUrls.length; i++) {
            if (TextUtils.isEmpty(resourceInfo.imageUrls[i])) {
                return false;
            }
        }
        return true;
    }

    private void saveInfo() {
        BusinessContentProvider.setSplashResourceInfo(context, resourceInfo);
    }

    private String saveFile(String url, byte[] bytes) {
        try {
            if (bytes != null) {
                String[] strs = url.split("/");
                String fileName = Environment.getExternalStorageDirectory().getPath() + File.separator +
                        context.getApplicationContext().getPackageName() + File.separator + strs[strs.length - 1];
                FileOutputStream outputStream;
                outputStream = new FileOutputStream(fileName);
                outputStream.write(bytes);
                outputStream.close();
                return fileName;
            }
            return null;
        } catch (Exception e) {
            BusinessLog.w("Splash download fail");
            e.printStackTrace();
            return null;
        }
    }

    private boolean isFileValidate(@NonNull String filePath, @NonNull String MD5) {
        File file = new File(filePath);
        try {
            String fileMD5 = Utils.getFileMD5(file);
            return MD5.equals(fileMD5);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private class FileDownloadRequest extends Request<byte[]> {
        private final Response.Listener<byte[]> mListener;
        private Map<String, String> mParams;
        public Map<String, String> responseHeaders;

        public FileDownloadRequest(int method, String mUrl, Response.Listener<byte[]> listener,
                                   Response.ErrorListener errorListener, HashMap<String, String> params) {
            super(method, mUrl, errorListener);
            // this request would never use cache.
            setShouldCache(false);
            mListener = listener;
            mParams = params;
        }

        @Override
        protected Map<String, String> getParams() {
            return mParams;
        }

        @Override
        protected void deliverResponse(byte[] response) {
            mListener.onResponse(response);
        }

        @Override
        protected Response<byte[]> parseNetworkResponse(NetworkResponse response) {
            responseHeaders = response.headers;
            return Response.success(response.data, HttpHeaderParser.parseCacheHeaders(response));
        }
    }
}
