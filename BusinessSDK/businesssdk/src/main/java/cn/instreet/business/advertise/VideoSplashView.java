package cn.instreet.business.advertise;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import java.io.File;

import cn.instreet.business.R;
import cn.instreet.business.utils.Asserts;
import cn.instreet.business.utils.CustomButton;
import cn.instreet.business.utils.CustomTimer;

/**
 * 视频闪屏页，参考@link https://www.jianshu.com/p/2b489bb119cb}的设计
 * created by yihao 2019/2/13
 */
public class VideoSplashView extends RelativeLayout {
    private VideoView videoView;
    private CustomTimer timer;
    private CustomButton buttonSkip;
    private int skipTimes = 10;
    SplashViewCallback callback;

    public VideoSplashView(Context context, String defaultVideoPath, final SplashViewCallback callback) {
        super(context);
        Asserts.checkNotNull(callback);
        this.callback = callback;
        LayoutInflater.from(getContext()).inflate(R.layout.activity_video_splash, this);
        videoView = findViewById(R.id.video_view);

        SplashResourceInfo info = BusinessContentProvider.getSplashResourceInfo(getContext());
        if (null == info) {
            callback.onError(SplashViewCallback.RESOURCE_NOT_FOUND, SplashViewCallback.MESSAGE_RESOURCE_NOT_FOUND);
            videoView.setVideoPath(defaultVideoPath);
        } else {
            String videoPath = info.videoUrl;
            File file = new File(videoPath);
            if (!file.exists()) {
                callback.onError(SplashViewCallback.BAD_RESOURCE, SplashViewCallback.MESSAGE_BAD_RESOURCE);
                videoView.setVideoPath(defaultVideoPath);
            } else {
                videoView.setVideoPath(videoPath);
            }
        }
        videoView.start();

        buttonSkip = findViewById(R.id.btn_skip);
        buttonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onSkip();
            }
        });

        timer = new CustomTimer(0, 1000, new Runnable() {
            @Override
            public void run() {
                if (skipTimes == 0) {
                    buttonSkip.setEnabled(true);
                    buttonSkip.setText("Skip");
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            callback.onFinish();
                        }
                    }, 4000);
                } else {
                    buttonSkip.setEnabled(false);
                    buttonSkip.setText("Skip in " + skipTimes-- + "s");
                }
            }
        });
        timer.start();
    }
}
