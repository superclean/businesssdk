package cn.instreet.business.global;

import android.content.Context;
import android.content.Intent;

import java.util.Timer;
import java.util.TimerTask;

import cn.instreet.business.BuildConfig;
import cn.instreet.business.utils.BusinessLog;
import cn.instreet.business.utils.Utils;

/**
 * 系统内存和进程数监控
 * created by yihao 2019/1/23
 */
public class SystemMonitor extends BaseObserver {
    private Timer timer;
    private TimerTask task;
    private static final int ALERT_LEVEL = 65;

    public SystemMonitor(final Context context) {
        super(context);
        timer = new Timer();
        task = new TimerTask() {
            @Override
            public void run() {
                int percent = Utils.getUsedPercentValue(context);
                int processCount = Utils.getRecentRunningApps(context).size();
                BusinessLog.d("memory using percent " + percent + " process count " + processCount);
                if(percent > ALERT_LEVEL){
                    Intent intent = new Intent();
                    intent.setAction(GlobalService.ACTION_GLOBAL_MEMORY_USING_HIGH);
                    intent.putExtra(GlobalService.EXTRA_MEMORY_USING_PERCENT, percent);
                    intent.putExtra(GlobalService.EXTRA_PROCESS_COUNT, processCount);
                    context.sendBroadcast(intent);
                }
            }
        };
    }

    @Override
    public void start() {
        long interval = BuildConfig.DEBUG ? 20 * 1000 : 300 * 1000;
        timer.schedule(task, 0, interval);
    }

    @Override
    public void stop() {
        task.cancel();
        timer.cancel();
    }
}
