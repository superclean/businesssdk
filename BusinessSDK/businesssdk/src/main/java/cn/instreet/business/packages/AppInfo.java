package cn.instreet.business.packages;

/**
 * 推荐App信息
 * created by yihao 2018/12/25
 */
public class AppInfo {
    // 关联的安装包包名
    public String relatedPackage;
    // 名称
    public String name;
    // 应用图标
    public String iconUrl;
    // 应用描述
    public String description;
    // 应用下载地址
    public String packageUrl;
}
