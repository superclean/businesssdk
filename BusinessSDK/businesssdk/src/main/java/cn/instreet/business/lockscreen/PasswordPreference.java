package cn.instreet.business.lockscreen;

import android.content.Context;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.ListPreferenceDialogFragmentCompat;
import android.util.AttributeSet;

/**
 * Created by 易昊 on 2019/2/23.
 */
public class PasswordPreference extends ListPreference {

    public PasswordPreference(Context context) {
        super(context);
    }

    public PasswordPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onClick() {
    }

    public void showDialog(){
        super.onClick();

        ListPreferenceDialogFragmentCompat fragmentCompat = (ListPreferenceDialogFragmentCompat) ListPreferenceDialogFragmentCompat.instantiate(
                getContext(), "");
    }
}