package cn.instreet.business;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import cn.instreet.business.global.GlobalService;
import cn.instreet.business.lockscreen.LockScreen;
import cn.instreet.business.lockscreen.LockScreenPreferenceActivity;
import cn.instreet.business.utils.Asserts;
import cn.instreet.business.utils.PermissionUtil;
import ezy.assist.compat.SettingsCompat;

import static android.content.pm.PackageManager.MATCH_UNINSTALLED_PACKAGES;
import static cn.instreet.business.PermissionRequestActivity.ACTION_SEND_PERMISSION_GRANT_RESULT;
import static cn.instreet.business.utils.PermissionUtil.hasUsageStatsPermission;

/**
 * created by yihao 2018/12/20
 * <p>
 * 商业化SDK总接口
 */
public class BusinessSDK {

    private static String masterPackageName = "";

    private static BusinessSDK sSDK;

    private Context context;

    private IGlobal globalInterface;

    private ServiceConnection serviceConnection;

    private SharedPreferences sharedPreferences;

    public static final String ACTION_PACKAGE_ADDED = "instreet.intent.action.PACKAGE_ADDED";
    public static final String ACTION_PACKAGE_REMOVED = "instreet.intent.action.PACKAGE_REMOVED";
    public static final String ACTION_CHARGER_PLUG_IN = "instreet.intent.action.CHARGER_PLUG_IN";
    public static final String ACTION_CHARGER_PLUG_OFF = "instreet.intent.action.CHARGER_PLUG_OFF";

    private HashMap<String, ApplicationInfo> totalPackages = new HashMap<>();

    public interface OnSDKInitListener {
        void onInit(BusinessSDK sdk);
    }

    /**
     * 初始化SDK，SDK初始化是个异步过程，会通过{@link OnSDKInitListener}进行回调
     *
     * @param context  sdk初始化需要的上下文，传入Activity是因为系统弹窗的需要，普通Context无法进行弹窗
     * @param listener sdk初始化回调接口
     */
    public static void InitSDK(Context context, final OnSDKInitListener listener) {
        new BusinessSDK(context, listener);
    }

    /**
     * 通过单例的方式获取SDK实例，但需要判断是否为空
     *
     * @return
     */
    public static BusinessSDK getInstance() {
        return sSDK;
    }

    /**
     * 拉起锁屏设置页面
     */
    public void startLockScreenPreferenceActivity() {
        if (null != context) {
            Intent intent = new Intent(context, LockScreenPreferenceActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    /**
     * 设置普通锁屏的样式
     *
     * @param iconResId       icon资源
     * @param title           标题
     * @param centerViewClass 中间视图类名称
     * @param bottomViewClass 底部视图类名称
     */
    public void setNormalLockScreenStyle(int iconResId, String title, Class<? extends View> centerViewClass, Class<? extends View> bottomViewClass) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(Constants.PREF_KEY_LOCKSCREEN_ICON, iconResId);
        editor.putString(Constants.PREF_KEY_LOCKSCREEN_TITLE, title);
        editor.putString(Constants.PREF_KEY_LOCKSCREEN_CENTER_VIEW, centerViewClass != null ? centerViewClass.getCanonicalName() : "");
        editor.putString(Constants.PREF_KEY_LOCKSCREEN_BOTTOM_VIEW, bottomViewClass != null ? bottomViewClass.getCanonicalName() : "");
        editor.apply();
    }

    /**
     * 判断锁屏是否开启
     *
     * @return
     */
    public boolean isLockScreenOn() {
        String currentMode = sharedPreferences.getString(Constants.PREF_KEY_LOCKSCREEN_MODE, LockScreen.MODE_NONE);
        if (LockScreen.MODE_NONE.equals(currentMode)) {
            return false;
        } else if (LockScreen.MODE_NORMAL.equals(currentMode)) {
            return true;
        } else if (LockScreen.MODE_SIMPLE.equals(currentMode)) {
            return true;
        }
        return false;
    }

    /**
     * 判断来电提醒是否开启
     *
     * @return
     */
    public boolean isPhoneRemindOn() {
        return sharedPreferences.getBoolean(Constants.PREF_KEY_PHONE, false);
    }

    /**
     * 判断充电提醒是否开启
     *
     * @return
     */
    public boolean isBatteryRemindOn() {
        return sharedPreferences.getBoolean(Constants.PREF_KEY_BATTERY, false);
    }

    /**
     * 监听指定包的安装卸载情况，该方法已经废弃，间隔时间改为由远程参数控制
     *
     * @param packageNames   想要监听的应用包名列表
     * @param intervalMinute 指定App卸载后的间隔时间(分钟)，该时间段内如果重新安装指定应用，会通过onAppReinstalled回调
     */
    @Deprecated
    public void listenAppChange(String[] packageNames, int intervalMinute) {
        requestFloatWindowPermissions();
        sharedPreferences.edit().putBoolean(Constants.PREF_KEY_PACKAGE, true).apply();
        try {
            globalInterface.listenAppChange(packageNames);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * 开启来电提醒，
     * 如果远程配置中开启了前置插屏广告，则挂断电话后会展示插屏广告，不展示挂机界面
     * 如果开启了后置插屏广告，则挂断电话会先展示挂机界面，再关闭挂机界面后，再显示后置插屏广告
     * 如果开启了原生广告，则挂断电话后会展示挂机界面，并展示原生广告，（原生广告会覆盖自定义视图）
     */
    public void enablePhoneRemind(final Class<? extends View> customViewClass) {
        sharedPreferences.edit().putBoolean(Constants.PREF_KEY_PHONE, true).apply();
        try {
            if (null != customViewClass) {
                globalInterface.enableCallReminder(customViewClass.getCanonicalName());
            } else {
                globalInterface.enableCallReminder(null);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * 关闭来电提醒
     */
    public void disablePhoneRemind() {
        sharedPreferences.edit().putBoolean(Constants.PREF_KEY_PHONE, false).apply();
        try {
            globalInterface.disableCallReminder();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * 监听应用变化，包括应用的安装和卸载
     *
     * @param packageNames 想要监听的应用的包名
     */
    public void listenAppChange(String[] packageNames) {
        requestFloatWindowPermissions();
        sharedPreferences.edit().putBoolean(Constants.PREF_KEY_PACKAGE,
                true).apply();

        HashSet<String> pkgNames = new HashSet<>(Arrays.asList(packageNames));
        sharedPreferences.edit().putStringSet(Constants.PREF_KEY_PACKAGE_NAMES,
                pkgNames).apply();
        try {
            globalInterface.listenAppChange(packageNames);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * 关闭应用安装监听
     */
    public void disableListenAppChange() {
        sharedPreferences.edit().putBoolean(Constants.PREF_KEY_PACKAGE, false).apply();
        try {
            globalInterface.unlistenAppChange();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * 开启充电提醒
     */
    public void enableBatteryReminder() {
//        requestFloatWindowPermissions();
        requestUsageStatsPermission();
        sharedPreferences.edit().putBoolean(Constants.PREF_KEY_BATTERY, true).apply();
    }

    /**
     * 关闭充电提醒
     */
    public void disableBatteryReminder() {
        sharedPreferences.edit().putBoolean(Constants.PREF_KEY_BATTERY, false).apply();
    }

    /**
     * 开启全能模块
     */
    public void enableGlobalModules() {
        // 申请蓝牙模块需要此权限
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermissionUtil.requestPermission(context, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION});
        }
        sharedPreferences.edit().putBoolean(Constants.PREF_KEY_GLOBAL, true).apply();
    }

    /**
     * 关闭全能模块
     */
    public void disableGlobalModules() {
        sharedPreferences.edit().putBoolean(Constants.PREF_KEY_GLOBAL, false).apply();
    }

    /**
     * 释放sdk
     */
    public void release() {
        context.getApplicationContext().unbindService(serviceConnection);
        Intent intent = new Intent(context, GlobalService.class);
        context.getApplicationContext().stopService(intent);
    }

    public float getBatteryTemperature() {
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, filter);
        if (batteryStatus == null) {
            return -1f;
        } else {
            int temperature = batteryStatus.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0);
            return temperature / 10.f;
        }
    }

    public void addPackage(String packageName){
        PackageManager packageManager = context.getPackageManager();
        if(null != packageManager){
            try {
                ApplicationInfo info = packageManager.getApplicationInfo(packageName, MATCH_UNINSTALLED_PACKAGES);
                totalPackages.put(packageName, info);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public ApplicationInfo getApplicationInfo(String packageName){
        return totalPackages.get(packageName);
    }

    private BusinessSDK(Context context, final OnSDKInitListener listener) {
        Asserts.checkNotNull(context);
        Asserts.checkNotNull(listener);
        this.context = context;
        sharedPreferences = context.getSharedPreferences(Constants.PREF_NAME_BUSINESS, Context.MODE_PRIVATE);
        final Intent intent = new Intent();

        // 如果主应用包名为空或者当前应用就是主应用，则拉起服务
        if (TextUtils.isEmpty(masterPackageName) || context.getPackageName().equals(masterPackageName)) {
            intent.setClass(context, GlobalService.class);
        } else {
            String className = GlobalService.class.getCanonicalName();
            if (null != className) {
                intent.setComponent(new ComponentName(masterPackageName, className));
            }
        }

        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                globalInterface = IGlobal.Stub.asInterface(service);
                listener.onInit(BusinessSDK.this);
                sSDK = BusinessSDK.this;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                BusinessSDK.this.context.getApplicationContext().bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE);
            }
        };
        context.getApplicationContext().bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE);

        getTotalPackages();
    }

    private void getTotalPackages() {
        PackageManager packageManager = context.getPackageManager();
        if(null != packageManager){
            List<ApplicationInfo> infos = packageManager.getInstalledApplications(PackageManager.GET_META_DATA);
            for (ApplicationInfo info : infos) {
                totalPackages.put(info.packageName, info);
            }
        }
    }

    private void requestFloatWindowPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // 申请锁屏，悬浮窗权限
            if (!SettingsCompat.canDrawOverlays(context)) {
                String title = context.getString(R.string.please_check_lockscreen_floatwindow_permission);
                AlertDialog alertDialog = new AlertDialog.Builder(context).setTitle(title)
                        .setPositiveButton(context.getString(R.string.OK), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    Intent intent = new Intent(context, PermissionRequestActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.putExtra("permission_name", "float_window");
                                    context.startActivity(intent);
                                } catch (ActivityNotFoundException exp) {
                                    exp.printStackTrace();
                                }
                            }
                        }).setNegativeButton(context.getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).create();
                alertDialog.show();
            }

            context.getApplicationContext().registerReceiver(permissionRequestReceiver, new IntentFilter(ACTION_SEND_PERMISSION_GRANT_RESULT));
        }
    }

    private final BroadcastReceiver permissionRequestReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (null == intent) {
                return;
            }
            if (!intent.getBooleanExtra("result", false) && BuildConfig.DEBUG) {
                Toast.makeText(context, R.string.permission_request_denied, Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void requestUsageStatsPermission() {
        // 申请应用使用情况权限
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && !hasUsageStatsPermission(context)
                && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PermissionUtil.requestPermissions(context, context.getString(R.string.package_usage_stats_rationale),
                    context.getString(R.string.package_usage_stats_rationale), new String[]{Manifest.permission.PACKAGE_USAGE_STATS}, new PermissionUtil.PermissionCallback() {
                        @Override
                        public void onResult(String[] permission, int[] results) {
                        }
                    });
        }
    }
}
