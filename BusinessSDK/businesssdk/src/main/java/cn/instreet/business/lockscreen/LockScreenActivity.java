package cn.instreet.business.lockscreen;

import android.Manifest;
import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextClock;
import android.widget.TextView;

import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;

import java.lang.reflect.Constructor;

import cn.instreet.business.Constants;
import cn.instreet.business.R;
import cn.instreet.business.advertise.AdvertiseManager;
import cn.instreet.business.remoteconfig.BusinessRemoteConfig;
import cn.instreet.business.utils.Utils;
import cn.instreet.business.utils.WeatherUtils;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * 参考CMLocker和记事本应用风格开发的锁屏页面
 */
public class LockScreenActivity extends AppCompatActivity {

    private static final int REQ_PIN_LOCK = 1;
    private static final int REQ_PATTERN_LOCK = 2;
    private int UNLOCK_DISTANCE = 600;
    private ShimmerTextView unlockTextView;
    private float firstTouchX;
    private float touchMoveX;
    private Class<? extends View> centerViewClass;
    private Class<? extends View> bottomViewClass;
    private ViewGroup viewGroupTop;
    private ImageView iconTitle;
    private TextView tvTitle;
    private TextClock tvDate;
    private TextClock tvTime;
    private TextView tvWeather;
    private boolean isUp;
    private static final int REQ_LOCATION = 1;
    private int iconRes;
    private String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true);
        }

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.view_locokscreen);

        WindowManager wm = this.getWindowManager();
        Point point = new Point();
        wm.getDefaultDisplay().getSize(point);
        UNLOCK_DISTANCE = point.x / 4;

        initParams();

        settingLockView();

        disableSystemLockscreen();

        requestLocationPermission();
    }

    private void initParams() {
        SharedPreferences preferences = getSharedPreferences(Constants.PREF_NAME_BUSINESS, MODE_PRIVATE);
        iconRes = preferences.getInt(Constants.PREF_KEY_LOCKSCREEN_ICON, 0);
        title = preferences.getString(Constants.PREF_KEY_LOCKSCREEN_TITLE, "");
        Class cls;
        try {
            cls = Class.forName(preferences.getString(Constants.PREF_KEY_LOCKSCREEN_CENTER_VIEW, ""));
            if (View.class.isAssignableFrom(cls)) {
                centerViewClass = cls;
            }

            cls = Class.forName(preferences.getString(Constants.PREF_KEY_LOCKSCREEN_BOTTOM_VIEW, ""));
            if (View.class.isAssignableFrom(cls)) {
                bottomViewClass = cls;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void requestLocationPermission() {
        if (!EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            EasyPermissions.requestPermissions(this, getString(R.string.location_permission_request), REQ_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

    @AfterPermissionGranted(REQ_LOCATION)
    private void showWeather() {
        tvWeather = findViewById(R.id.tv_weather);
        WeatherUtils.getCurrentWeather(this, new WeatherUtils.WeatherCallback() {
            @Override
            public void onResult(String result) {
                tvWeather.setText(result);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(LockScreen.ACTION_LOCKSCREEN_ON);
        sendBroadcast(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Intent intent = new Intent(LockScreen.ACTION_LOCKSCREEN_OFF);
        sendBroadcast(intent);

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
    }

    private void disableSystemLockscreen() {
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("");
        keyguardLock.disableKeyguard();
    }

    private void settingLockView() {
        viewGroupTop = findViewById(R.id.layout_top);
        iconTitle = findViewById(R.id.icon_title);
        setupUnlockView();
        setupTopView();
        setupCenter();
        setupBottomView();
    }

    private void setupBottomView() {
        final ImageView btn = findViewById(R.id.btn_up);
        final RelativeLayout bottomCustomContainer = findViewById(R.id.bottom_container);
        if (null != bottomViewClass) {
            btn.setVisibility(View.VISIBLE);
            try {
                Constructor<? extends View> constructor = bottomViewClass.getConstructor(Context.class);
                View view = constructor.newInstance(this);
                bottomCustomContainer.addView(view);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            btn.setVisibility(View.GONE);
        }
        final RelativeLayout container = findViewById(R.id.layout_bottom);
        ViewTreeObserver observer = container.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                container.setTranslationY(bottomCustomContainer.getHeight());
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int translateY = bottomCustomContainer.getHeight() * (isUp ? 1 : -1);
                container.animate().translationYBy(translateY).setDuration(500).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        btn.setRotation(isUp ? 180 : 0);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                isUp = !isUp;
            }
        });
    }

    private void setupCenter() {
        if (BusinessRemoteConfig.isLockscreenAdsOn()) {
            if (AdvertiseManager.isInit()) {
                final ViewGroup group = findViewById(R.id.center_container);
                AdvertiseManager.getInstance().showNativeAds(group);
            }
        } else {
            if (null != centerViewClass) {
                try {
                    Constructor<? extends View> constructor = centerViewClass.getConstructor(Context.class);
                    View view = constructor.newInstance(this);
                    RelativeLayout centerContainer = findViewById(R.id.center_container);
                    centerContainer.addView(view);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void setupTopView() {
        Drawable drawable = null;
        if (-1 != iconRes) {
            try {
                drawable = getResources().getDrawable(iconRes);
            } catch (Resources.NotFoundException exp) {
                exp.printStackTrace();
            }
        }
        if (null != drawable) {
            iconTitle.setImageDrawable(drawable);
        } else {
            drawable = Utils.getAppIcon(this, getPackageName());
            if (null != drawable) {
                iconTitle.setImageDrawable(drawable);
            }
        }

        tvTitle = findViewById(R.id.tv_title);
        if (!TextUtils.isEmpty(title)) {
            tvTitle.setText(title);
        } else {
            String appName;
            if (!TextUtils.isEmpty(appName = Utils.getAppName(this, getPackageName()))) {
                tvTitle.setText(appName);
            }
        }

        tvDate = findViewById(R.id.tv_date);
        tvDate.setFormat24Hour("MM-dd, EEEE");
        tvDate.setFormat12Hour("MM-dd, EEEE");

        tvTime = findViewById(R.id.tv_time);
        tvTime.setFormat24Hour("HH:mm");
        tvTime.setFormat12Hour("a h:mm");

        if (!tvTime.is24HourModeEnabled()) {
            tvTime.setTextSize(TypedValue.COMPLEX_UNIT_SP, 60);
        } else {
            tvTime.setTextSize(TypedValue.COMPLEX_UNIT_SP, 80);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setupUnlockView() {
        unlockTextView = findViewById(R.id.shimmer_tv);
        unlockTextView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN: {
                        firstTouchX = event.getX();
                    }
                    break;
                    case MotionEvent.ACTION_MOVE: {
                        touchMoveX = (int) (event.getRawX() - firstTouchX);
                        float scale = 1 + (touchMoveX - firstTouchX) / 2000.0f;
                        viewGroupTop.setScaleX(scale);
                        viewGroupTop.setScaleY(scale);
                        viewGroupTop.setAlpha(1 - (touchMoveX - firstTouchX) / 800.0f);
                    }
                    break;
                    case MotionEvent.ACTION_UP: {
                        viewGroupTop.setScaleX(1);
                        viewGroupTop.setScaleY(1);
                        viewGroupTop.setAlpha(1);
                        if (touchMoveX > UNLOCK_DISTANCE) {
                            onUnlock();
                        }
                        firstTouchX = 0;
                        touchMoveX = 0;
                    }
                    break;
                    default:
                        break;
                }

                return true;
            }
        });

        new Shimmer().setDuration(3000).setStartDelay(500).start(unlockTextView);
    }

    private void onUnlock() {
        String mode = getSharedPreferences(Constants.PREF_NAME_BUSINESS, MODE_PRIVATE)
                .getString(Constants.PREF_KEY_LOCKSCREEN_PASSWORD_MODE, LockScreen.MODE_PASSWORD_NONE);
        if (LockScreen.MODE_PASSWORD_NONE.equals(mode)) {
            moveTaskToBack(true);
            finish();
        } else if (LockScreen.MODE_PASSWORD_PIN.equals(mode)) {
            Intent intent = new Intent(LockScreenActivity.this, PinLockActivity.class);
            intent.putExtra("mode", "unlock_screen");
            startActivityForResult(intent, REQ_PIN_LOCK);
        } else if (LockScreen.MODE_PASSWORD_PATTERN.equals(mode)) {
            Intent intent = new Intent(LockScreenActivity.this, PatternLockActivity.class);
            intent.putExtra("mode", "unlock_screen");
            startActivityForResult(intent, REQ_PATTERN_LOCK);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu); // 初始化加载菜单项
//        menu.add(1, Menu.FIRST, 1, "Change Site ID"); // 四个参数，groupid, itemid, orderid, title
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case 1: //Menu.FIRST对应itemid为1
                super.finish();
//                System.exit(0);
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_PIN_LOCK || requestCode == REQ_PATTERN_LOCK) {
            if (resultCode == RESULT_OK) {
                moveTaskToBack(true);
                finish();
            }
        }
    }
}
