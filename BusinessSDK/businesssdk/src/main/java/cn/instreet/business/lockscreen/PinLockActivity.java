package cn.instreet.business.lockscreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;

import cn.instreet.business.R;


/**
* created by yihao 2019/2/19
*/
public class PinLockActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_lock);

        final EditText editText = findViewById(R.id.tv_password);
        final TextView tvTips = findViewById(R.id.tv_tips);
        Intent intent = getIntent();
        String mode = intent.getStringExtra("mode");
        if("validate".equals(mode)){
            tvTips.setText(R.string.pin_validate_title);
        }
        else if("unlock_screen".equals(mode)){
            tvTips.setText(R.string.pin_unlock_title);
        }
        final PinLockView lockView = findViewById(R.id.pin_lock_view);
        lockView.setPinLength(4);
        lockView.setPinLockListener(new PinLockListener() {
            @Override
            public void onComplete(String pin) {
                editText.setText(pin);
                String srcPin = getSharedPreferences("setting", MODE_PRIVATE).getString("pinCode", "");
                if(pin.equals(srcPin)){
                    setResult(RESULT_OK);
                    finish();
                }
                else{
                    tvTips.setText(R.string.pin_error);
                    lockView.resetPinLockView();
                    editText.setText("");
                }
            }

            @Override
            public void onEmpty() {
                editText.setText("");
            }

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {
                tvTips.setText("");
                editText.setText(intermediatePin);
            }
        });

    }
}
