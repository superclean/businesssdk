package cn.instreet.business.lockscreen;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;

import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;

import cn.instreet.business.R;

/**
 * created by yihao 2019/2/19
 * Pin码设置页面
 */
public class PinSettingActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_setting);

        final EditText editText = findViewById(R.id.tv_password);
        PinLockView lockView = findViewById(R.id.pin_lock_view);
        lockView.setPinLength(4);
        lockView.setPinLockListener(new PinLockListener() {
            @Override
            public void onComplete(String pin) {
                editText.setText(pin);
                savePinCode(pin);
                finish();
            }

            @Override
            public void onEmpty() {
                editText.setText("");
            }

            @Override
            public void onPinChange(int pinLength, String intermediatePin) {
                editText.setText(intermediatePin);
            }
        });
    }

    private void savePinCode(String pin) {
        boolean result = getSharedPreferences("setting", MODE_PRIVATE).edit().putString("pinCode", pin).commit();
        if(result){
            setResult(RESULT_OK);
        }
        else{
            setResult(RESULT_CANCELED);
        }
    }
}
