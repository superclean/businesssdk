package cn.instreet.business.global;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import cn.instreet.business.Constants;

import static android.bluetooth.BluetoothAdapter.ACTION_STATE_CHANGED;

/**
 * 蓝牙设备检测
 * created by yihao 2019/1/4
 */
public class BluetoothObserver extends BaseObserver {
    private boolean isRegistered = false;

    public BluetoothObserver(Context context) {
        super(context);
    }

    public void start() {
        if (isRegistered) {
            return;
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_STATE_CHANGED);
        context.registerReceiver(bluetoothReceiver, filter);
        isRegistered = true;
    }

    public void stop() {
        if (!isRegistered) {
            return;
        }
        context.unregisterReceiver(bluetoothReceiver);
        isRegistered = false;
    }

    private final BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(!isEnable()){
               return;
            }
            final BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
            if (BluetoothAdapter.STATE_ON == adapter.getState()) {
                Intent newIntent = new Intent(GlobalService.ACTION_GLOBAL_BLUETOOTH_TURN_ON);
                context.sendBroadcast(newIntent);
            } else if (BluetoothAdapter.STATE_OFF == adapter.getState()) {
                Intent newIntent = new Intent(GlobalService.ACTION_GLOBAL_BLUETOOTH_TURN_OFF);
                context.sendBroadcast(newIntent);
            }
        }
    };

    private boolean isEnable() {
        return context.getSharedPreferences(Constants.PREF_NAME_BUSINESS, Context.MODE_PRIVATE)
                .getBoolean(Constants.PREF_KEY_GLOBAL, false);
    }
}
