package cn.instreet.business.battery;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import cn.instreet.business.R;
import cn.instreet.business.utils.Utils;

/**
 * created by yihao 2019/1/18
 */
public class BatteryReminderView extends RelativeLayout {
    public static final int MODE_LOW_BATTERY = 0;
    public static final int MODE_CHARGING = 1;
    public static final int MODE_OPTIMIZE = 2;
    public static final int TYPE_CHARGER_UNKNOWN = 0;
    public static final int TYPE_CHARGER_USB = 0;
    public static final int TYPE_CHARGER_AC = 1;

    private static final int LEVEL_HIGH = 90;
    private static final int LEVEL_MEDIUM = 50;
    private static final int LEVEL_LOW = 20;
    private ImageView ivBattery;
    private ImageView ivClose;
    private TextView tvPercent;
    private TextView tvTips;
    private TextView tvBatteryStatus;
    private TextView tvEstimateTime;
    private TextView tvMemoryTips;
    private TextView tvMemoryPercent;
    private TextView tvProcessCount;


    private Button btnAction;
    private BatteryReminderCallback callback;

    public interface BatteryReminderCallback {
        void onClose();

        void onOptimize();
    }

    public BatteryReminderView(Context context) {
        super(context);
        init();
    }

    public BatteryReminderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BatteryReminderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setBatteryReminderCallback(BatteryReminderCallback callback) {
        this.callback = callback;
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.battery_reminder, this);
        ivBattery = findViewById(R.id.iv_battery);
        ivClose = findViewById(R.id.btn_close);
        ivClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != callback) {
                    callback.onClose();
                }
            }
        });
        tvPercent = findViewById(R.id.tv_percent);
        tvTips = findViewById(R.id.tv_tips);
        tvBatteryStatus = findViewById(R.id.value_status);
        tvEstimateTime = findViewById(R.id.tv_time_estimate);
        tvMemoryTips = findViewById(R.id.tv_memory_tips);
        tvMemoryPercent = findViewById(R.id.value_memory_status);
        tvProcessCount = findViewById(R.id.value_process_count);
        btnAction = findViewById(R.id.btn_action);
    }

    /**
     * 设置低电量模式
     *
     * @param batteryLevel
     */
    public void setLowBatteryMode(int batteryLevel) {
        if (batteryLevel > LEVEL_MEDIUM) {
            return;
        }
        LinearLayout linearLayout = findViewById(R.id.layout_root_memory);
        linearLayout.setVisibility(View.GONE);
        if (batteryLevel < LEVEL_LOW) {
            ivBattery.setImageDrawable(getContext().getResources().getDrawable(R.mipmap.battery_low));
        } else {
            ivBattery.setImageDrawable(getContext().getResources().getDrawable(R.mipmap.battery_medium));
        }
        tvTips.setText(getContext().getString(R.string.low_battery_notice));
        tvPercent.setText(getContext().getString(R.string.battery_percent, batteryLevel));
        tvBatteryStatus.setText(getContext().getString(R.string.not_charging));

        // 设置文字显示
        int time = getUsingEstimateTime(batteryLevel);
        int hour = time / 60;
        int minute = time % 60;
        if (hour > 0) {
            tvEstimateTime.setText(getContext().getString(R.string.using_estimate_time_hour, hour, minute));
        } else {
            tvEstimateTime.setText(getContext().getString(R.string.using_estimate_time, minute));
        }

        setOptimizeActionUI(batteryLevel);
    }

    /**
     * 设置充电模式
     *
     * @param batteryLevel 电量百分比
     */
    public void setChargingMode(int batteryLevel, int chargerType) {
        LinearLayout linearLayout = findViewById(R.id.layout_root_memory);
        linearLayout.setVisibility(View.GONE);
        if (batteryLevel < LEVEL_LOW) {
            ivBattery.setImageDrawable(getContext().getResources().getDrawable(R.mipmap.battery_low));
        } else if (batteryLevel < LEVEL_MEDIUM) {
            ivBattery.setImageDrawable(getContext().getResources().getDrawable(R.mipmap.battery_medium));
        } else if (batteryLevel < LEVEL_HIGH) {
            ivBattery.setImageDrawable(getContext().getResources().getDrawable(R.mipmap.battery_high));
        } else {
            ivBattery.setImageDrawable(getContext().getResources().getDrawable(R.mipmap.battery_full));
        }
        tvTips.setText(getContext().getString(R.string.charging_notice));
        tvPercent.setText(getContext().getString(R.string.battery_percent, batteryLevel));
        tvBatteryStatus.setText(getContext().getString(R.string.charging));

        // 设置文字显示
        int time = getChargingEstimateTime(batteryLevel, chargerType);
        int hour = time / 60;
        int minute = time % 60;
        if (hour > 0) {
            tvEstimateTime.setText(getContext().getString(R.string.charge_estimate_time_hour, hour, minute));
        } else {
            tvEstimateTime.setText(getContext().getString(R.string.charge_estimate_time, minute));
        }
        setOptimizeActionUI(batteryLevel);
    }

    public void setMemoryUsingHighMode(int memoryPercent, int processCount){
        LinearLayout linearLayout = findViewById(R.id.layout_root_battery);
        linearLayout.setVisibility(View.GONE);
        tvMemoryTips.setText(getContext().getString(R.string.memory_notice));
        tvMemoryPercent.setText(String.format(getContext().getString(R.string.memory_percent), memoryPercent));
        tvProcessCount.setText(String.format("%d", processCount));
        setOptimizeActionUI(100);
    }

    private void setOptimizeActionUI(int batteryLevel) {
        // 电量优化界面
        if (hasOptimizeApp()) {
            btnAction.setVisibility(View.VISIBLE);
            if (batteryLevel < LEVEL_LOW) {
                btnAction.setBackgroundColor(getResources().getColor(R.color.batteryActionLow));
            } else if (batteryLevel < LEVEL_MEDIUM) {
                btnAction.setBackgroundColor(getResources().getColor(R.color.batteryActionMedium));
            } else {
                btnAction.setBackgroundColor(getResources().getColor(R.color.batteryActionHigh));
            }
            btnAction.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    startOptimizeActivity();
                }
            });
        } else {
            btnAction.setVisibility(View.GONE);
        }
    }

    private void startOptimizeActivity() {
        if(null != callback){
            callback.onOptimize();
        }
        Intent intent = new Intent();
        // FIXME 会被混淆，要添加处理
        intent.setAction("cn.instreet.superclean.BoostActivity");
        intent.putExtra("percent", 0.5f);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);
    }

    private boolean hasOptimizeApp() {
        PackageManager pm = getContext().getPackageManager();
        List<PackageInfo> packages = pm.getInstalledPackages(0);
        for (PackageInfo info : packages) {
            if ("cn.instreet.superclean".equals(info.packageName)) {
                return true;
            }
        }
        return false;
    }

    private int getUsingEstimateTime(int batteryLevel) {
        return (int) (batteryLevel * Utils.getEstimateBatteryDuration(getContext()) / 100f * 60);
    }

    private int getChargingEstimateTime(int batteryLevel, int chargerType) {
        int chargeScale = chargerType == TYPE_CHARGER_AC ? 6 : 3;
        return (int) ((100 - batteryLevel) * Utils.getEstimateBatteryDuration(getContext()) / (100f * chargeScale) * 60);
    }
}
