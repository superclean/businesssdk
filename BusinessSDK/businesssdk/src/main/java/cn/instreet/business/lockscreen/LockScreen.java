package cn.instreet.business.lockscreen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;

import cn.instreet.business.Constants;
import cn.instreet.business.PermissionRequestActivity;
import cn.instreet.business.R;

import static android.content.Context.KEYGUARD_SERVICE;
import static android.content.Context.WINDOW_SERVICE;

/**
 * created by yihao 2018/12/20
 *
 * 锁屏操作封装
 */

public class LockScreen {
    // 用于监听锁屏开启的广播动作
    public static final String ACTION_LOCKSCREEN_ON = "business.intent.action.lockscreen_on";

    // 用于监听锁屏关闭的广播动作
    public static final String ACTION_LOCKSCREEN_OFF = "business.intent.action.lockscreen_off";

    public static String MODE_NONE = "None";
    public static String MODE_NORMAL = "Normal";
    public static String MODE_SIMPLE = "Simple";

    public static String MODE_PASSWORD_NONE = "None";
    public static String MODE_PASSWORD_PIN = "PIN";
    public static String MODE_PASSWORD_PATTERN = "Pattern";

    private final int UNLOCK_DISTANCE = 100;
    private Context context;
    private WindowManager windowManager;
    private WindowManager.LayoutParams windowParams;
    private View lockScreenView;
    private ShimmerTextView unlockTextView;
    private float firstTouchX;
    private float touchMoveX;


    public LockScreen(Context context) {
        if (null == context) {
            throw new IllegalArgumentException("context could not be null !");
        }
        this.context = context.getApplicationContext();

        windowParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            windowParams.flags |= WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        }
        windowManager = ((WindowManager) context.getSystemService(WINDOW_SERVICE));
        lockScreenView = View.inflate(this.context, R.layout.view_locokscreen, null);
    }

    /**
     * 以Activity的方式拉起锁屏页面
     */
    public void showLockScreenActivity() {
        // 如果未设置锁屏，或者锁屏模式为None，则返回
        SharedPreferences preferences = context.getSharedPreferences(Constants.PREF_NAME_BUSINESS, Context.MODE_PRIVATE);
        String lockScreenMode = preferences.getString(Constants.PREF_KEY_LOCKSCREEN_MODE, MODE_NONE);
        if(TextUtils.isEmpty(lockScreenMode) || MODE_NONE.equals(lockScreenMode)){
            return;
        }
        // 普通锁屏模式
        Class cls = null;
        if(lockScreenMode.equals(MODE_NORMAL)){
            cls = LockScreenActivity.class;
            Intent intent = new Intent(context, LockScreenActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            context.startActivity(intent);
        }
        // 极简锁屏模式
        else if(lockScreenMode.equals(MODE_SIMPLE)){
            cls = AmoledLockScreenActivity.class;
            Intent intent = new Intent(context, cls);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            context.startActivity(intent);
        }
        if(null == cls){
            return;
        }
    }

    /**
     * 禁用系统锁屏
     */
    private void disableSystemKeyguard() {
        KeyguardManager manager = (KeyguardManager) context.getSystemService(KEYGUARD_SERVICE);
        if (null != manager && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // 隐藏锁屏界面
            dismissKeyguard(manager);
        }
    }

    /**
     * 以窗口的方式展示锁屏
     */
    public void showLockScreenWindow() {
        disableSystemKeyguard();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(context)) {
                Intent permissionActivityIntent = new Intent(context, PermissionRequestActivity.class);
                permissionActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(permissionActivityIntent);
                return;
            }
        }
        addLockScreenView();
    }

    private void addLockScreenView() {
        if (null != windowManager && null != lockScreenView && null != windowParams) {
            if (null == lockScreenView.getParent()) {
                windowManager.addView(lockScreenView, windowParams);
                settingLockView();
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void settingLockView() {
        unlockTextView = lockScreenView.findViewById(R.id.shimmer_tv);
        unlockTextView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN: {
                        firstTouchX = event.getX();
                    }
                    break;
                    case MotionEvent.ACTION_MOVE: {
                        touchMoveX = (int) (event.getRawX() - firstTouchX);
                    }
                    break;
                    case MotionEvent.ACTION_UP: {
                        if (touchMoveX > UNLOCK_DISTANCE) {
                            hideLockScreenView();
                        }
                        firstTouchX = 0;
                        touchMoveX = 0;
                    }
                    break;
                    default:
                        break;
                }

                return true;
            }
        });
        new Shimmer().setDuration(3000).setStartDelay(500).start(unlockTextView);
    }

    private void hideLockScreenView() {
        windowManager.removeView(lockScreenView);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void dismissKeyguard(KeyguardManager manager) {
        try {
            manager.requestDismissKeyguard((Activity) context, new KeyguardManager.KeyguardDismissCallback() {
                @Override
                public void onDismissError() {
                    super.onDismissError();
                }

                @Override
                public void onDismissSucceeded() {
                    super.onDismissSucceeded();
                }
            });
        }catch (ClassCastException exp){
            exp.printStackTrace();
        }
    }
}
