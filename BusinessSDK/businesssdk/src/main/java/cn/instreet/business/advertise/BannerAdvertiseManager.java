package cn.instreet.business.advertise;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.common.internal.Preconditions;

import java.util.concurrent.locks.Condition;

import cn.instreet.business.BuildConfig;
import cn.instreet.business.utils.BusinessLog;

/**
 * created by yihao 2019/5/30
 */
public class BannerAdvertiseManager {
    private static BannerAdvertiseManager instance = null;
    private Context context;

    public static BannerAdvertiseManager getInstance() {
        if (null == instance) {
            instance = new BannerAdvertiseManager();
        }
        return instance;
    }

    private BannerAdvertiseManager() {
    }

    public void init(Context context, String appId) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotEmpty(appId);
        this.context = context.getApplicationContext();
        MobileAds.initialize(this.context, appId);
    }

    public View showBanner(final String unitId) {
        Preconditions.checkNotEmpty(unitId);
        AdView adView = createAdView(unitId, AdSize.BANNER);
        AdRequest.Builder builder = new AdRequest.Builder();
        if (BuildConfig.DEBUG) {
            builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        }
        // Start loading the ad in the background.
        adView.loadAd(builder.build());
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdImpression() {
                super.onAdImpression();
                BusinessLog.d("on banner ad impression " + unitId);
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                BusinessLog.d("on banner ad fail error code " + i);
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                BusinessLog.d("on banner ad loaded " + unitId);
            }
        });
        return adView;
    }

    public View showMediumBanner(final String unitId) {
        Preconditions.checkNotEmpty(unitId);
        AdView adView = createAdView(unitId, AdSize.LARGE_BANNER);
        AdRequest.Builder builder = new AdRequest.Builder();
        if (BuildConfig.DEBUG) {
            builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        }
        // Start loading the ad in the background.
        adView.loadAd(builder.build());
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdImpression() {
                super.onAdImpression();
                BusinessLog.d("on large banner ad impression " + unitId);

            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                BusinessLog.d("on banner ad fail error code " + i);
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                BusinessLog.d("on banner ad loaded " + unitId);
            }
        });
        return adView;
    }

    public View showLargeBanner(final String unitId) {
        Preconditions.checkNotEmpty(unitId);
        AdView adView = createAdView(unitId, AdSize.MEDIUM_RECTANGLE);
        AdRequest.Builder builder = new AdRequest.Builder();
        if (BuildConfig.DEBUG) {
            builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        }
        // Start loading the ad in the background.
        adView.loadAd(builder.build());
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdImpression() {
                super.onAdImpression();
                BusinessLog.d("on large banner ad impression " + unitId);
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                BusinessLog.d("on banner ad fail error code " + i);
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                BusinessLog.d("on banner ad loaded " + unitId);
            }
        });
        return adView;
    }

    private AdView createAdView(String id, AdSize size) {
        Preconditions.checkNotEmpty(id);
        Preconditions.checkNotNull(size);
        AdView adView = new AdView(context);
        adView.setAdUnitId(id);
        adView.setAdSize(size);
        return adView;
    }
}
