package cn.instreet.business.lockscreen;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.andrognito.patternlockview.PatternLockView;
import com.andrognito.patternlockview.listener.PatternLockViewListener;
import com.andrognito.patternlockview.utils.PatternLockUtils;

import java.util.List;

import cn.instreet.business.R;

public class PatternSettingActivity extends Activity {
    private String firstPatternValue;
    private String secondPatternValue;
    private int settingTimes = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pattern_setting);

        final TextView tvTips = findViewById(R.id.tv_tips);
        final PatternLockView lockView = findViewById(R.id.pattern_lock_view);
        lockView.addPatternLockListener(new PatternLockViewListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(List<PatternLockView.Dot> progressPattern) {

            }

            @Override
            public void onComplete(List<PatternLockView.Dot> pattern) {
                if(settingTimes == 1){
                    firstPatternValue = PatternLockUtils.patternToString(lockView, pattern);
                    settingTimes++;
                    tvTips.setText(getString(R.string.set_pattern_again));
                    lockView.clearPattern();
                    return;
                }
                if(settingTimes == 2){
                    secondPatternValue = PatternLockUtils.patternToString(lockView, pattern);
                    if(secondPatternValue.equals(firstPatternValue)){
                        savePattern(secondPatternValue);
                        finish();
                    }
                    else{
                        settingTimes = 1;
                        tvTips.setText(getString(R.string.two_patterns_not_equal));
                        lockView.clearPattern();
                    }
                }
            }

            @Override
            public void onCleared() {

            }
        });
    }

    private void savePattern(String patternCode){
        boolean result = getSharedPreferences("setting", MODE_PRIVATE).edit().putString("patternCode", patternCode).commit();
        if(result){
            setResult(RESULT_OK);
        }
        else{
            setResult(RESULT_CANCELED);
        }
    }
}
