package cn.instreet.business.phone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.view.View;

import cn.instreet.business.Constants;
import cn.instreet.business.advertise.AdvertiseManager;
import cn.instreet.business.global.GlobalService;
import cn.instreet.business.remoteconfig.BusinessRemoteConfig;
import cn.instreet.business.utils.BusinessLog;

import static cn.instreet.business.advertise.AdvertiseManager.MODE_HIDE;


/**
 * 来电提醒模块
 * created by yihao 2018/12/28
 */
public class PhoneCallAlerter {
    private Context context;
    private BroadcastReceiver broadcastReceiver;
    private Class customViewClass;
    private boolean isRegistered = false;

    public PhoneCallAlerter(Context context) {
        if (null == context) {
            throw new IllegalArgumentException("context is null");
        }
        this.context = context.getApplicationContext();
    }

    public void setCustomViewClass(Class customViewClass) {
        if (null != customViewClass && View.class.isAssignableFrom(customViewClass)) {
            this.customViewClass = customViewClass;
        }
    }

    public void start() {
        if (isRegistered) {
            return;
        }
        context.registerReceiver(broadcastReceiver = new PhoneBroadcastReceiver(), new IntentFilter(GlobalService.ACTION_PHONE_HANGOFF));
        isRegistered = true;
    }

    public void stop() {
        if (!isRegistered) {
            return;
        }
        context.unregisterReceiver(broadcastReceiver);
        isRegistered = false;
    }

    private class PhoneBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(Constants.TAG, "PhoneBroadcastReceiver onReceive");
            if(!isEnable()){
                return;
            }
            if (isPreScreenEnable()) {
                showPreScreen();
            } else {
                boolean isPostAdsEnable = "back".equals(BusinessRemoteConfig.getPhoneAdsMode());
                boolean isNativeAdsEnable = "native".equals(BusinessRemoteConfig.getPhoneAdsMode());
                String phoneNumber = intent.getStringExtra(GlobalService.EXTRA_PHONE_NUMBER);
                showPhoneOffActivity(phoneNumber, isPostAdsEnable, isNativeAdsEnable);
            }
        }
    }

    private boolean isEnable() {
        return context.getSharedPreferences(Constants.PREF_NAME_BUSINESS, Context.MODE_PRIVATE).getBoolean(Constants.PREF_KEY_PHONE, false);
    }

    private void showPhoneOffActivity(String phoneNumber, boolean isPostAdsEnable, boolean isNativeAdsEnable) {
        Intent intent = new Intent(context.getApplicationContext(), PhoneCallAlertActivity.class);
        intent.putExtra(GlobalService.EXTRA_PHONE_NUMBER, phoneNumber);
        intent.putExtra(GlobalService.EXTRA_POST_ADS, isPostAdsEnable);
        intent.putExtra(GlobalService.EXTRA_NATIVE_ADS, isNativeAdsEnable);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("custom_view_class", customViewClass);
        context.startActivity(intent);
    }

    private void showPreScreen() {
        AdvertiseManager.showInterstitialAdsRemote(context, MODE_HIDE);
    }

    private boolean isPreScreenEnable() {
        BusinessLog.d("isPreAdsEnable " + "front".equals(BusinessRemoteConfig.getPhoneAdsMode()));
        return "front".equals(BusinessRemoteConfig.getPhoneAdsMode());
    }
}
