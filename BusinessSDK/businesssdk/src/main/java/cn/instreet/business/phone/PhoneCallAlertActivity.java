package cn.instreet.business.phone;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.InterstitialAd;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import cn.instreet.business.R;
import cn.instreet.business.advertise.AdvertiseManager;
import cn.instreet.business.global.GlobalService;
import cn.instreet.business.utils.BusinessLog;
import pub.devrel.easypermissions.EasyPermissions;

import static cn.instreet.business.advertise.AdvertiseManager.MODE_HIDE;


/**
 * 来电提醒页面
 */

public class PhoneCallAlertActivity extends Activity {

    private TextView tvNumber;
    private TextView tvCaller;
    private ImageView ivPhone;
    private ImageView ivMessage;
    private ImageView ivBack;
    private ViewGroup vgCustom;
    private InterstitialAd interstitialAd;
    private Class customViewClass;
    private String phoneNumber;
    private boolean isNativeAdsEnable = false;
    private boolean isPostAdsEnable = false;
    private View customView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call);
        tvNumber = findViewById(R.id.tv_number);
        tvCaller = findViewById(R.id.tv_caller);
        ivPhone = findViewById(R.id.iv_phone);
        ivMessage = findViewById(R.id.iv_message);
        vgCustom = findViewById(R.id.container);
        ivBack = findViewById(R.id.iv_back);

        Intent intent = getIntent();
        if (null != intent) {
            isNativeAdsEnable = intent.getBooleanExtra(GlobalService.EXTRA_NATIVE_ADS, false);
            isPostAdsEnable = intent.getBooleanExtra(GlobalService.EXTRA_POST_ADS, false);
            phoneNumber = intent.getStringExtra(GlobalService.EXTRA_PHONE_NUMBER);
            if(EasyPermissions.hasPermissions(this, Manifest.permission.READ_CONTACTS)
                    && EasyPermissions.hasPermissions(this, Manifest.permission.WRITE_CONTACTS)){
                Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
                if(null != cursor){
                    while (cursor.moveToNext()) {
                        tvCaller.setText(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
                    }
                    cursor.close();
                }
            }


            if (!TextUtils.isEmpty(phoneNumber)) {
                tvNumber.setText(phoneNumber);
            }
            ivPhone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:" + phoneNumber));
                    if (ActivityCompat.checkSelfPermission(PhoneCallAlertActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    startActivity(intent);
                }
            });
            ivMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:" + phoneNumber));
                    startActivity(intent);
                }
            });
            ivBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    exit();
                }
            });

            Object cls = intent.getSerializableExtra("custom_view_class");
            if (cls instanceof Class && View.class.isAssignableFrom((Class<?>) cls)) {
                customViewClass = (Class) cls;
                try {
                    Constructor constructor = customViewClass.getConstructor(Context.class);
                    customView = (View) constructor.newInstance(this);
                    vgCustom.addView(customView);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }

            // 如果启用原生广告，则用原生广告覆盖自定义区域
            BusinessLog.d("isNativeAdsEnable " + isNativeAdsEnable);
            if (isNativeAdsEnable) {
                if(AdvertiseManager.isInit()){
                    AdvertiseManager.getInstance().showNativeAds(vgCustom);
                }
            }
        } else {
            moveTaskToBack(true);
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                exit();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void exit() {
        BusinessLog.d("isPostAdsEnable " + isPostAdsEnable);
        if (isPostAdsEnable) {
            if (AdvertiseManager.isInit()) {
                AdvertiseManager.getInstance().showInterstitialAds(MODE_HIDE);
            }
        } else {
            moveTaskToBack(true);
            finish();
        }
    }
}
