package cn.instreet.business.utils;

import android.text.TextUtils;
import android.util.Log;

import cn.instreet.business.Constants;

/**
 * Log工具类
 * created by yihao 2019/1/4
 */
public class BusinessLog {
    private static final boolean isEnable = true;// BuildConfig.DEBUG;

    public static void d(String message) {
        if(TextUtils.isEmpty(message)){
            return;
        }
        if (isEnable) {
            Log.d(Constants.TAG, message);
        }
    }

    public static void w(String message) {
        if(TextUtils.isEmpty(message)){
            return;
        }
        if (isEnable) {
            Log.w(Constants.TAG, message);
        }
    }

    public static void timeStamp(String message){
        if(TextUtils.isEmpty(message)){
            return;
        }
        if (isEnable) {
            Log.d(Constants.TAG, message + " " + System.currentTimeMillis());
        }
    }

    public static void stack() {
        Log.d(Constants.TAG, Log.getStackTraceString(new Throwable()));
    }
}
