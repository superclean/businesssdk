package cn.instreet.business.utils;

import android.app.AppOpsManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;

import cn.instreet.business.PermissionRequestActivity;

import static cn.instreet.business.PermissionRequestActivity.ACTION_SEND_PERMISSION_GRANT_RESULT;

/**
 * created by yihao 2019/1/4
 */
public class PermissionUtil {

    private static final HashMap<String[], BroadcastReceiver> receiverHashMap = new HashMap<>();

    public static void requestPermission(Context context, String[] permissionNames) {
        Asserts.checkNotNull(context);
        Asserts.checkNotNull(permissionNames);

        Intent intent = new Intent(context, PermissionRequestActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("permission_names", permissionNames);
        context.startActivity(intent);
    }

    public static void requestPermissions(Context context, final String explain, final String rationale,
                                          final String[] permissionNames, final PermissionCallback callback) {
        Asserts.checkNotNull(context);
        Asserts.checkNotNull(permissionNames);
        Asserts.checkNotNull(callback);
        ArrayList<String> names = new ArrayList<>();
        for (String name : permissionNames) {
            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(context, name)) {
                names.add(name);
            }
        }
        if (names.isEmpty()) {
            int[] results = new int[permissionNames.length];
            for (int i = 0; i < results.length; i++) {
                results[i] = PackageManager.PERMISSION_GRANTED;
            }
            callback.onResult(permissionNames, results);
            return;
        }
        final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                callback.onResult(intent.getStringArrayExtra("permission_names"), intent.getIntArrayExtra("result"));
                context.unregisterReceiver(receiverHashMap.get(permissionNames));
            }
        };
        context.registerReceiver(broadcastReceiver, new IntentFilter(ACTION_SEND_PERMISSION_GRANT_RESULT));
        receiverHashMap.put(permissionNames, broadcastReceiver);

        Intent intent = new Intent(context, PermissionRequestActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("permission_names", permissionNames);
        if(!TextUtils.isEmpty(explain)){
            intent.putExtra("explain", explain);
        }
        if (!TextUtils.isEmpty(rationale)) {
            intent.putExtra("rationale", rationale);
        }
        context.startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean hasUsageStatsPermission(Context context) {
        Asserts.checkNotNull(context);
        AppOpsManager appOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                android.os.Process.myUid(), context.getPackageName());
        boolean granted;
        if (mode == AppOpsManager.MODE_DEFAULT) {
            granted = (context.checkCallingOrSelfPermission(android.Manifest.permission.PACKAGE_USAGE_STATS) == PackageManager.PERMISSION_GRANTED);
        } else {
            granted = (mode == AppOpsManager.MODE_ALLOWED);
        }
        return granted;
    }

    public interface PermissionCallback {
        void onResult(String[] permission, int[] results);
    }


}
