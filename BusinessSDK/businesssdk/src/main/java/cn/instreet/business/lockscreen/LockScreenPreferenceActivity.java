package cn.instreet.business.lockscreen;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.View;
import android.widget.ImageView;

import cn.instreet.business.Constants;
import cn.instreet.business.R;

import static cn.instreet.business.lockscreen.LockScreen.MODE_NONE;
import static cn.instreet.business.lockscreen.LockScreen.MODE_PASSWORD_PATTERN;
import static cn.instreet.business.lockscreen.LockScreen.MODE_PASSWORD_PIN;

public class LockScreenPreferenceActivity extends FragmentActivity {

    private static final int REQ_PIN_CODE = 1;
    private static final int REQ_PATTERN_CODE = 2;
    private static final int REQ_VALIDATE_PIN_CODE = 3;
    private static final int REQ_VALIDATE_PATTERN_CODE = 4;
    private static String[] lockscreen_mode_names;
    private static String[] password_mode_names;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock_screen_preference);
        getSupportFragmentManager().beginTransaction().replace(R.id.pref_container, new LockScreenPreferenceFragment())
                .commit();

        if(Build.MANUFACTURER.equalsIgnoreCase("xiaomi")){
            try {
                Intent intent = new Intent();
                intent.setAction("miui.intent.action.APP_PERM_EDITOR");
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.putExtra("extra_pkgname", getPackageName());
                startActivity(intent);
            } catch (ActivityNotFoundException exp) {
                exp.printStackTrace();
            }
        }

        lockscreen_mode_names = getResources().getStringArray(R.array.pref_lock_screen_mode);
        password_mode_names = getResources().getStringArray(R.array.pref_password_mode);

        ImageView ivBack = findViewById(R.id.iv_back);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class LockScreenPreferenceFragment extends PreferenceFragmentCompat {
        private boolean isValidate = true;
        private PasswordPreference prefPassword;

        @Override
        public void onCreatePreferences(Bundle bundle, String s) {
            addPreferencesFromResource(R.xml.pref_lock_screen);
            final Preference prefMode = getPreferenceManager().findPreference("lock_screen_mode");
            prefMode.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    if(o.equals(LockScreen.MODE_NONE)){
                        prefMode.setSummary(lockscreen_mode_names[0]);
                    }
                    else if(o.equals(LockScreen.MODE_NORMAL)){
                        prefMode.setSummary(lockscreen_mode_names[1]);
                    }
                    else if(o.equals(LockScreen.MODE_SIMPLE)){
                        prefMode.setSummary(lockscreen_mode_names[2]);
                    }
                    SharedPreferences preferences = getActivity().getSharedPreferences(Constants.PREF_NAME_BUSINESS, MODE_PRIVATE);
                    preferences.edit().putString(Constants.PREF_KEY_LOCKSCREEN_MODE, (String) o).apply();
                    return true;
                }
            });

            SharedPreferences preferences = getActivity().getSharedPreferences(Constants.PREF_NAME_BUSINESS, MODE_PRIVATE);
            String currentMode = preferences.getString(Constants.PREF_KEY_LOCKSCREEN_MODE, MODE_NONE);
            if("None".equals(currentMode)){
                prefMode.setSummary(lockscreen_mode_names[0]);
            }
            else if("Normal".equals(currentMode)){
                prefMode.setSummary(lockscreen_mode_names[1]);
            }
            else if("Simple".equals(currentMode)){
                prefMode.setSummary(lockscreen_mode_names[2]);
            }

            prefPassword = (PasswordPreference) getPreferenceManager().findPreference("password_mode");
            prefPassword.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    // 如果验证失败，则不处理
                    if (!isValidate) {
                        return false;
                    } else {
                        if(o.equals("None")){
                            prefPassword.setSummary(password_mode_names[0]);
                        }
                        else if(o.equals("PIN")){
                            prefPassword.setSummary(password_mode_names[1]);
                        }
                        else if(o.equals("Pattern")){
                            prefPassword.setSummary(password_mode_names[2]);
                        }
                        if (o.equals("PIN")) {
                            startActivityForResult(new Intent(getActivity(), PinSettingActivity.class), REQ_PIN_CODE);
                        } else if (o.equals("Pattern")) {
                            startActivityForResult(new Intent(getActivity(), PatternSettingActivity.class), REQ_PATTERN_CODE);
                        } else if (o.equals("None")) {
                            getActivity().getSharedPreferences(Constants.PREF_NAME_BUSINESS, MODE_PRIVATE).edit()
                                    .putString(Constants.PREF_KEY_LOCKSCREEN_PASSWORD_MODE, "None").apply();
                        }
                        return true;
                    }
                }
            });

            String passwordMode = getActivity().getSharedPreferences(Constants.PREF_NAME_BUSINESS, MODE_PRIVATE)
                    .getString(Constants.PREF_KEY_LOCKSCREEN_PASSWORD_MODE, "None");
            if(null != passwordMode){
                if(passwordMode.equals("None")){
                    prefPassword.setSummary(password_mode_names[0]);
                }
                else if(passwordMode.equals("PIN")){
                    prefPassword.setSummary(password_mode_names[1]);
                }
                else if(passwordMode.equals("Pattern")){
                    prefPassword.setSummary(password_mode_names[2]);
                }
            }

            prefPassword.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    String currentMode = getActivity().getSharedPreferences(Constants.PREF_NAME_BUSINESS, MODE_PRIVATE)
                            .getString(Constants.PREF_KEY_LOCKSCREEN_PASSWORD_MODE, "None");
                    if(null != currentMode){
                        if (currentMode.equals("PIN")) {
                            Intent intent = new Intent(getActivity(), PinLockActivity.class);
                            intent.putExtra("mode", "validate");
                            startActivityForResult(intent, REQ_VALIDATE_PIN_CODE);
                        } else if (currentMode.equals("Pattern")) {
                            Intent intent = new Intent(getActivity(), PatternLockActivity.class);
                            intent.putExtra("mode", "validate");
                            startActivityForResult(intent, REQ_VALIDATE_PATTERN_CODE);
                        } else if (currentMode.equals("None")) {
                            LockScreenPreferenceFragment.this.onDisplayPreferenceDialog(prefPassword);
                        }
                    }

                    return true;
                }
            });

        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            // 密码设置完成，更换密码模式
            if (resultCode == RESULT_OK) {
                if (requestCode == REQ_PIN_CODE) {
                    getActivity().getSharedPreferences(Constants.PREF_NAME_BUSINESS, MODE_PRIVATE).edit()
                            .putString(Constants.PREF_KEY_LOCKSCREEN_PASSWORD_MODE, MODE_PASSWORD_PIN).apply();
                } else if (requestCode == REQ_PATTERN_CODE) {
                    getActivity().getSharedPreferences(Constants.PREF_NAME_BUSINESS, MODE_PRIVATE).edit()
                            .putString(Constants.PREF_KEY_LOCKSCREEN_PASSWORD_MODE, MODE_PASSWORD_PATTERN).apply();
                } else if (requestCode == REQ_VALIDATE_PATTERN_CODE || requestCode == REQ_VALIDATE_PIN_CODE) {
                    isValidate = true;
                    this.onDisplayPreferenceDialog(prefPassword);
                }

            } else if (resultCode == RESULT_CANCELED) {
                if (requestCode == REQ_VALIDATE_PATTERN_CODE || requestCode == REQ_VALIDATE_PIN_CODE) {
                    isValidate = false;
                }
            }

        }
    }
}
