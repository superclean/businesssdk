package cn.instreet.business.utils;

import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import cn.instreet.business.BuildConfig;
import cn.instreet.business.R;
import cn.instreet.business.packages.AppInfo;

import static android.content.Context.WINDOW_SERVICE;

/**
 * SDK内置悬浮窗操作类
 * created by yihao 2018/12/25
 */
public class SDKPopupWindow {
    private final Context context;
    private WindowManager.LayoutParams windowParams;
    private final boolean hasPermission = true;
    private View rootView;
    private AppInfo[] recommendApps;

    public SDKPopupWindow(Context context) {
        this.context = context;

        if (null == context) {
            throw new IllegalArgumentException("context is null");
        }
        rootView = View.inflate(this.context, R.layout.sdk_popup_window, null);
        // 不同的Android系统上悬浮窗flag的参数不一样
        int flag = BuildConfig.testMode?WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN|WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                :WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            windowParams = new WindowManager.LayoutParams(
                    Utils.dip2px(context, 300), WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    flag,
                    PixelFormat.TRANSLUCENT);
        } else {
            windowParams = new WindowManager.LayoutParams(
                    Utils.dip2px(context, 300), WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    flag,
                    PixelFormat.TRANSLUCENT);
        }
    }

    /**
     * 显示悬浮窗
     *
     * @param isDisplayRecommendApps 是否显示推荐App
     */
    public void show(boolean isDisplayRecommendApps) {
        if (isDisplayRecommendApps) {
            ViewPager viewPager = rootView.findViewById(R.id.vp_recommend_app);
            viewPager.setAdapter(new PagerAdapter() {
                @Override
                public int getCount() {
                    if (null != recommendApps) {
                        return recommendApps.length;
                    } else {
                        return 0;
                    }
                }

                @Override
                public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
                    return (o.equals(view));
                }

            });


        }

        WindowManager windowManager = ((WindowManager) context.getSystemService(WINDOW_SERVICE));
        if(null != windowManager){
            try {
                windowManager.addView(rootView, windowParams);
            } catch (Exception exp) {
                exp.printStackTrace();
                windowManager.removeView(rootView);
            }
        }

    }

    /**
     * 隐藏悬浮窗
     */
    public void dismiss() {
        WindowManager windowManager = ((WindowManager) context.getSystemService(WINDOW_SERVICE));
        if (null != windowManager && hasPermission && rootView.getParent() != null) {
            windowManager.removeView(rootView);
        }
    }

    /**
     * 设置悬浮窗要显示的视图
     *
     * @param view
     */
    public void setView(View view) {
        if(null != view){
            ViewGroup viewGroup = rootView.findViewById(R.id.layout_advertise);
            viewGroup.removeAllViews();
            viewGroup.addView(view);
        }
    }

    /**
     * 设置推荐的App列表
     *
     * @param apps
     */
    public void setRecommandApps(AppInfo[] apps) {
        this.recommendApps = apps;
    }
}
