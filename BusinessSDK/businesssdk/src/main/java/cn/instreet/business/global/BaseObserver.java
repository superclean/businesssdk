package cn.instreet.business.global;

import android.content.Context;

import cn.instreet.business.utils.Asserts;

/**
 * created by yihao 2019/1/4
 */
public abstract class BaseObserver {
    protected final Context context;

    public BaseObserver(Context context){
        Asserts.checkNotNull(context);
        this.context = context.getApplicationContext();
    }

    public abstract void start();

    public abstract void stop();
}
