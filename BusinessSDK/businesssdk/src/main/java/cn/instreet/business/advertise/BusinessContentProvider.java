package cn.instreet.business.advertise;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import cn.instreet.business.utils.Asserts;
import cn.instreet.business.utils.BusinessLog;

import static cn.instreet.business.advertise.BusinessDBHelper.TABLE_NAME_AD_RES;
import static cn.instreet.business.advertise.BusinessDBHelper.TABLE_NAME_PREF;

public class BusinessContentProvider extends ContentProvider {
    public static String URI_AD_RES;
    public static String URI_PREFERENCE;
    public static String AUTHORITY;

    private BusinessDBHelper dbHelper;

    private static UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private static final int AD_RESOURCE = 1; // 对应广告资源表
    private static final int PREFERENCE = 2;  // 对应选项表

    static {

    }

    public BusinessContentProvider() {

    }

    @Override
    public boolean onCreate() {
        BusinessLog.d("ContentProvider onCreate");
        Context context = getContext();
        if (null == context) {
            return false;
        }
        String packageName = context.getPackageName();
        AUTHORITY = packageName + ".businessProvider";
        URI_AD_RES = "content://" + AUTHORITY + "/adResource";
        URI_PREFERENCE = "content://" + AUTHORITY + "/preference";
        sUriMatcher.addURI(AUTHORITY, "adResource", AD_RESOURCE);
        sUriMatcher.addURI(AUTHORITY, "preference", PREFERENCE);
        dbHelper = new BusinessDBHelper(getContext());
        return true;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(@NonNull Uri uri) {
        // TODO: Implement this to handle requests for the MIME type of the data
        // at the given URI_AD_RES.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        int match = sUriMatcher.match(uri);

        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.beginTransaction();
        if (match == AD_RESOURCE) {
            database.replace(TABLE_NAME_AD_RES, null, values);
        } else if (match == PREFERENCE) {
            database.replace(TABLE_NAME_PREF, null, values);
        }
        database.setTransactionSuccessful();
        database.endTransaction();
        Context context = getContext();
        if (null != context) {
            context.getContentResolver().notifyChange(uri, null);
        }
        return null;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        int match = sUriMatcher.match(uri);
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor = null;
        if (match == AD_RESOURCE) {
            cursor = database.query(TABLE_NAME_AD_RES, projection, selection, selectionArgs, null
                    , null, sortOrder);
        } else if (match == PREFERENCE) {
            cursor = database.query(TABLE_NAME_PREF, projection, selection, selectionArgs, null
                    , null, sortOrder);
        }
        return cursor;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public static void setSplashResourceInfo(Context context, SplashResourceInfo info) {
        Asserts.checkNotNull(context);
        Asserts.checkNotNull(info);
        ContentValues contentValues = new ContentValues();
        contentValues.put("content", new Gson().toJson(info));
        contentValues.put("version", info.versionCode);
        context.getApplicationContext().getContentResolver().insert(
                Uri.parse("content://" + context.getPackageName() + ".businessProvider" + "/adResource"), contentValues);
    }

    public static SplashResourceInfo getSplashResourceInfo(Context context) {
        Asserts.checkNotNull(context);
        Cursor cursor = context.getContentResolver().query(
                Uri.parse("content://" + context.getPackageName() + ".businessProvider" + "/adResource"), new String[]{"content"},
                null, null, null);
        if (null != cursor && cursor.getCount() > 0) {
            cursor.moveToLast();
            Gson gson = new Gson();
            String str = cursor.getString(0);
            SplashResourceInfo info = gson.fromJson(str, SplashResourceInfo.class);
            cursor.close();
            return info;
        }
        return null;
    }
}
