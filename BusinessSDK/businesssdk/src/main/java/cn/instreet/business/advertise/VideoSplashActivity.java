package cn.instreet.business.advertise;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.VideoView;

import cn.instreet.business.R;
import cn.instreet.business.utils.CustomButton;
import cn.instreet.business.utils.CustomTimer;

/**
 * Fixme:
 * 已知问题，切后台再切回来，videoView会因为surfaceDestroyed，黑屏
 */
public class VideoSplashActivity extends AppCompatActivity {
    private CustomTimer timer;
    private CustomButton buttonSkip;
    private int skipTimes = 10;
    private VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_splash);
        videoView = findViewById(R.id.video_view);
        videoView.setVideoPath("mnt/sdcard/splash_2.mp4");
        videoView.start();

        buttonSkip = findViewById(R.id.btn_skip);
        buttonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        timer = new CustomTimer(0, 1000, new Runnable() {
            @Override
            public void run() {
                if (skipTimes == 0) {
                    buttonSkip.setEnabled(true);
                    buttonSkip.setText("Skip");
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }, 4000);
                } else {
                    buttonSkip.setEnabled(false);
                    buttonSkip.setText("Skip in " + skipTimes-- + "s");
                }
            }
        });
        timer.start();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.stop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        timer.pause();
        videoView.pause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        timer.resume();
        videoView.resume();
    }
}
