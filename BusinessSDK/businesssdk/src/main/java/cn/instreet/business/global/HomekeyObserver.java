package cn.instreet.business.global;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import cn.instreet.business.Constants;


/**
 * created by yihao 2019/1/4
 */
public class HomekeyObserver extends BaseObserver {
    private boolean isRegistered = false;

    public HomekeyObserver(Context context) {
        super(context);
    }

    @Override
    public void start() {
        if(isRegistered){
            return;
        }
        this.context.registerReceiver(homeReceiver, new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS));
        isRegistered = true;
    }

    @Override
    public void stop() {
        if(!isRegistered){
            return;
        }
        context.unregisterReceiver(homeReceiver);
        isRegistered = false;
    }

    private final BroadcastReceiver homeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!isEnable()){
                return;
            }
            if(FirebaseRemoteConfig.getInstance().getBoolean(Constants.SWITCH_HOME)) {
                Intent newIntent = new Intent(GlobalService.ACTION_GLOBAL_HOMEKEY_PRESSED);
                context.sendBroadcast(newIntent);
            }
        }
    };

    private boolean isEnable() {
        return context.getSharedPreferences(Constants.PREF_NAME_BUSINESS, Context.MODE_PRIVATE).getBoolean(Constants.PREF_KEY_GLOBAL, false);
    }
}
