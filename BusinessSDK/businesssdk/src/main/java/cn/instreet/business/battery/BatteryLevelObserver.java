package cn.instreet.business.battery;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.BatteryManager;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.content.LocalBroadcastManager;

import cn.instreet.business.BusinessSDK;
import cn.instreet.business.Constants;
import cn.instreet.business.IGlobal;
import cn.instreet.business.global.BaseObserver;
import cn.instreet.business.global.GlobalService;
import cn.instreet.business.utils.Asserts;
import cn.instreet.business.utils.BusinessLog;

import static cn.instreet.business.battery.BatteryReminderView.TYPE_CHARGER_AC;
import static cn.instreet.business.battery.BatteryReminderView.TYPE_CHARGER_UNKNOWN;
import static cn.instreet.business.battery.BatteryReminderView.TYPE_CHARGER_USB;

/**
 * 电量监控
 * created by yihao 2019/1/16
 */
public class BatteryLevelObserver extends BaseObserver {

    private final int REMIND_BATTERY_LEVEL = 50;
    private Context context;
    private ServiceConnection globalServiceConnection;
    private IGlobal global;
    private boolean isCharging;
    private int batteryLevel;
    private int batteryScale;
    private BatteryLevelListener batteryLevelListener;
    private int chargerType;

    public interface BatteryLevelListener {
        void onHangUp(boolean isCharging, int batteryLevel, int chargerType);

        void lowBattery(int batteryLevel);

        void onChargerPluginIn(int batteryLevel, int chargerType);

        void onChargerPluginOff(int batteryLevel);
    }

    public BatteryLevelObserver(Context context, BatteryLevelListener levelListener) {
        super(context);
        Asserts.checkNotNull(context);
        this.context = context.getApplicationContext();
        batteryLevelListener = levelListener;

    }

    @Override
    public void start() {
        listenBatteryChange();
        listenPhoneCall();
    }

    @Override
    public void stop() {

    }

    /**
     * 监听电量变化
     */
    private void listenBatteryChange() {
        // 监听充电状态变化
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_POWER_CONNECTED);
        filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        context.registerReceiver(new ChargingStateReceiver(), filter);

        filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, filter);
        if (batteryStatus == null) {
            return;
        }
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        BusinessLog.d("battery EXTRA_STATUS " + status);
        isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

        if (isCharging) {
            int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            if (chargePlug == BatteryManager.BATTERY_PLUGGED_USB) {
                chargerType = TYPE_CHARGER_USB;
            } else if (chargePlug == BatteryManager.BATTERY_PLUGGED_AC) {
                chargerType = TYPE_CHARGER_AC;
            }
        }


    }

    /**
     * 监听通话
     */
    private void listenPhoneCall() {
        globalServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                global = IGlobal.Stub.asInterface(service);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                global = null;
            }
        };
        Intent intent = new Intent(this.context, GlobalService.class);
        this.context.bindService(intent, globalServiceConnection, Context.BIND_AUTO_CREATE);

        context.registerReceiver(new PhoneBroadcastReceiver(), new IntentFilter(GlobalService.ACTION_PHONE_HANGOFF));
    }

    /**
     * 判断是否在通话
     *
     * @return 返回通话状态
     */
    private boolean isOnCalling() {
        if (null != global) {
            try {
                return global.isOnCalling();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private int getCurrentBatteryLevel() {
        return batteryLevel * 100 / batteryScale;
    }

    private class ChargingStateReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_POWER_CONNECTED.equals(intent.getAction())) {
                isCharging = true;
                int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
                if (chargePlug == BatteryManager.BATTERY_PLUGGED_USB) {
                    chargerType = TYPE_CHARGER_USB;
                } else if (chargePlug == BatteryManager.BATTERY_PLUGGED_AC) {
                    chargerType = TYPE_CHARGER_AC;
                }
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(BusinessSDK.ACTION_CHARGER_PLUG_IN));

            } else if (Intent.ACTION_POWER_DISCONNECTED.equals(intent.getAction())) {
                isCharging = false;
                chargerType = TYPE_CHARGER_UNKNOWN;
                LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(BusinessSDK.ACTION_CHARGER_PLUG_OFF));

            }
        }
    }

    private class PhoneBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!isEnable()){
                return;
            }
            if(null == batteryLevelListener){
                return;
            }
            if (isCharging) {
                batteryLevelListener.onHangUp(true, getCurrentBatteryLevel(), chargerType);
            } else {
                if ((batteryLevel * 100 / batteryScale) < REMIND_BATTERY_LEVEL) {
                    batteryLevelListener.onHangUp(false, getCurrentBatteryLevel(), chargerType);
                }
            }
        }
    }

    private boolean isEnable() {
        return context.getSharedPreferences(Constants.PREF_NAME_BUSINESS, Context.MODE_PRIVATE)
                .getBoolean(Constants.PREF_KEY_BATTERY, false);
    }
}
