package cn.instreet.business.packages;

import cn.instreet.business.utils.SDKPopupWindow;

/**
 * created by yihao 2018/12/26
 */
public interface AppChangeListener {
    /**
     * 检测到指定App的安装
     *
     * @param window
     */
    void onAppInstalled(String packageName, SDKPopupWindow window);

    /**
     * 检测到其他App的安装
     *
     * @param otherPackageName 其他App的包名
     * @param isInstall   true表示安装，false表示卸载
     * @param window
     */
    void onOtherAppChanged(String otherPackageName, boolean isInstall, SDKPopupWindow window);

    /**
     * 检测到指定App被重装
     *
     * @param window
     */
    void onAppReinstalled(String packageName, SDKPopupWindow window);

    /**
     * 检测到指定App被卸载
     *
     * @param window
     */
    void onAppUninstalled(String packageName, SDKPopupWindow window);
}
