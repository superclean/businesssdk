package cn.instreet.business.global;

import android.bluetooth.BluetoothHeadset;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import cn.instreet.business.Constants;

import static cn.instreet.business.global.GlobalService.ACTION_GLOBAL_HEADSET_PLUG_IN;
import static cn.instreet.business.global.GlobalService.ACTION_GLOBAL_HEADSET_PLUG_OFF;


/**
 * created by yihao 2019/1/4
 */
public class HeadsetObserver extends BaseObserver{
    private boolean isRegistered = false;

    public HeadsetObserver(Context context){
        super(context);
    }

    public void start(){
        if(isRegistered){
            return;
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_HEADSET_PLUG);
        intentFilter.addAction(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED);
        context.registerReceiver(headsetReceiver, intentFilter);
        isRegistered = true;

    }

    public void stop(){
        if(!isRegistered){
            return;
        }
        context.unregisterReceiver(headsetReceiver);
        isRegistered = false;
    }

    private final BroadcastReceiver headsetReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(!isEnable()){
                return;
            }
            String action = intent.getAction();
            if (Intent.ACTION_HEADSET_PLUG.equals(action)) {
                if (intent.hasExtra("state")) {
                    int headsetState = intent.getIntExtra("state", 0);
                    Intent newIntent = new Intent(headsetState == 1?ACTION_GLOBAL_HEADSET_PLUG_IN:ACTION_GLOBAL_HEADSET_PLUG_OFF);
                    context.sendBroadcast(newIntent);
                }
            }
        }
    };

    private boolean isEnable() {
        return context.getSharedPreferences(Constants.PREF_NAME_BUSINESS, Context.MODE_PRIVATE).getBoolean(Constants.PREF_KEY_GLOBAL, false);
    }
}
