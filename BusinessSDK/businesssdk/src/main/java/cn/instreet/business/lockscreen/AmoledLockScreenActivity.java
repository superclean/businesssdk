package cn.instreet.business.lockscreen;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextClock;
import android.widget.TextView;

import cn.instreet.business.Constants;
import cn.instreet.business.R;
import cn.instreet.business.advertise.AdvertiseManager;
import cn.instreet.business.remoteconfig.BusinessRemoteConfig;
import cn.instreet.business.utils.CameraUtil;
import cn.instreet.business.utils.WeatherUtils;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Amoled风格锁屏界面
 * created by yihao 2019/2/15
 */
public class AmoledLockScreenActivity extends Activity {

    private static final int REQ_PIN_LOCK = 1;
    private static final int REQ_PATTERN_LOCK = 2;
    private static final int REQ_PERMISSION_PHONE = 3;
    private static final int REQ_PERMISSION_CAMERA = 4;
    private static final int REQ_PERMISSION_LOCATION = 5;
    private TextClock clockTime;
    private TextClock clockDate;
    private TextView tvWeather;
    private ImageView ivLock;
    private ImageView ivPhone;
    private ImageView ivCamera;
    private RelativeLayout layoutBackground;
    private Handler handler = new Handler();
    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.activity_amoled_lockscreen);

        initFunctional();

        initDateAndTime();

        initWeather();

        // 显示广告
        if (BusinessRemoteConfig.isLockscreenAdsOn() && AdvertiseManager.isInit()) {
            AdvertiseManager.getInstance().showNativeAds((ViewGroup) findViewById(R.id.layout_advertise),
                    AdvertiseManager.MODE_STYLE_5);
        }

        // 禁用系统锁屏
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("");
        keyguardLock.disableKeyguard();

    }

    @SuppressLint("ClickableViewAccessibility")
    private void initFunctional() {
        ivLock = findViewById(R.id.iv_lock);
        ivLock.setVisibility(View.INVISIBLE);
        ivLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unlock();
            }
        });
        try {
            AnimationDrawable drawable = (AnimationDrawable) ivLock.getDrawable();
            drawable.start();
        } catch (ClassCastException exp) {
            exp.printStackTrace();
        }

        ivPhone = findViewById(R.id.iv_phone);
        ivPhone.setVisibility(View.INVISIBLE);
        ivPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dial();
            }
        });

        ivCamera = findViewById(R.id.iv_camera);
        ivCamera.setVisibility(View.INVISIBLE);

        layoutBackground = findViewById(R.id.layout_background);
        layoutBackground.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                displayFunctionalView();
                return false;
            }
        });

        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePhoto();
            }
        });
    }

    private void initWeather() {
        tvWeather = findViewById(R.id.tv_weather);
        WeatherUtils.getCurrentWeather(this, new WeatherUtils.WeatherCallback() {
            @Override
            public void onResult(String result) {
                if (!TextUtils.isEmpty(result)) {
                    tvWeather.setText(result);
                }
            }
        });
        requestLocationPermission();
    }

    private void initDateAndTime() {
        clockDate = findViewById(R.id.tv_date);
        clockDate.setFormat24Hour("MM-dd, EEEE");
        clockDate.setFormat12Hour("MM-dd, EEEE");

        clockTime = findViewById(R.id.tv_time);
        clockTime.setFormat24Hour("HH:mm");
        clockTime.setFormat12Hour("a h:mm");

        if (!clockTime.is24HourModeEnabled()) {
            clockTime.setTextSize(TypedValue.COMPLEX_UNIT_SP, 60);
        } else {
            clockTime.setTextSize(TypedValue.COMPLEX_UNIT_SP, 80);
        }
    }

    private void requestLocationPermission() {
        if (!EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            EasyPermissions.requestPermissions(this, getString(R.string.location_permission_request), REQ_PERMISSION_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

    private void takePhoto() {
        boolean result = false;
        if (Build.MANUFACTURER.equalsIgnoreCase("huawei")) {
            result = CameraUtil.openCameraInHuawei(this);
        } else if (Build.MANUFACTURER.equalsIgnoreCase("xiaomi")) {
            result = CameraUtil.openCameraInXiaomi(this);
        } else {
            openCameraActivity();
        }

        if (!result) {
            openCameraActivity();
        }
    }

    @AfterPermissionGranted(REQ_PERMISSION_LOCATION)
    private void showWeather() {
        tvWeather = findViewById(R.id.tv_weather);
        WeatherUtils.getCurrentWeather(this, new WeatherUtils.WeatherCallback() {
            @Override
            public void onResult(String result) {
                tvWeather.setText(result);
            }
        });
    }

    @AfterPermissionGranted(REQ_PERMISSION_CAMERA)
    private void openCameraActivity() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA)) {
            CameraUtil.startCameraActivity(this);
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.request_camera_permission),
                    REQ_PERMISSION_CAMERA, Manifest.permission.CAMERA);
        }

    }

    private void unlock() {
        String mode = getSharedPreferences(Constants.PREF_NAME_BUSINESS, MODE_PRIVATE)
                .getString(Constants.PREF_KEY_LOCKSCREEN_PASSWORD_MODE, LockScreen.MODE_NONE);
        if (null == mode) {
            return;
        }
        if (mode.equals(LockScreen.MODE_PASSWORD_NONE)) {
            moveTaskToBack(true);
            finish();
        } else if (mode.equals(LockScreen.MODE_PASSWORD_PIN)) {
            Intent intent = new Intent(AmoledLockScreenActivity.this, PinLockActivity.class);
            intent.putExtra("mode", "unlock_screen");
            startActivityForResult(intent, REQ_PIN_LOCK);
        } else if (mode.equals(LockScreen.MODE_PASSWORD_PATTERN)) {
            Intent intent = new Intent(AmoledLockScreenActivity.this, PatternLockActivity.class);
            intent.putExtra("mode", "unlock_screen");
            startActivityForResult(intent, REQ_PATTERN_LOCK);
        }
    }

    @AfterPermissionGranted(REQ_PERMISSION_PHONE)
    public void dial() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.CALL_PHONE)) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.request_phone_permission),
                    REQ_PERMISSION_PHONE, Manifest.permission.CALL_PHONE);
        }

    }

    private void displayFunctionalView() {
        ivLock.setVisibility(View.VISIBLE);
        ivCamera.setVisibility(View.VISIBLE);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ivLock.setVisibility(View.INVISIBLE);
                ivPhone.setVisibility(View.INVISIBLE);
                ivCamera.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(LockScreen.ACTION_LOCKSCREEN_ON);
        sendBroadcast(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Intent intent = new Intent(LockScreen.ACTION_LOCKSCREEN_OFF);
        sendBroadcast(intent);

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_PIN_LOCK || requestCode == REQ_PATTERN_LOCK) {
            if (resultCode == RESULT_OK) {
                moveTaskToBack(true);
                finish();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }
}
