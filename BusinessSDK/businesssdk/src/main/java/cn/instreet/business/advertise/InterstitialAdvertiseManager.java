package cn.instreet.business.advertise;

import android.content.Context;
import android.text.TextUtils;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.google.android.gms.common.internal.Preconditions;

import java.util.HashMap;

import cn.instreet.business.utils.BusinessLog;

/**
 * created by yihao 2019/6/4
 */
public class InterstitialAdvertiseManager {
    private static InterstitialAdvertiseManager instance = null;
    private Context context;
    private HashMap<String, PublisherInterstitialAd> adHashMap = new HashMap<>();
    private HashMap<String, OnAdClosed> closedHashMap = new HashMap<>();

    public static InterstitialAdvertiseManager getInstance() {
        if (null == instance) {
            instance = new InterstitialAdvertiseManager();
        }
        return instance;
    }

    private InterstitialAdvertiseManager() {

    }

    public void init(Context context, String appId) {
        if (null == context) {
            throw new NullPointerException("context");
        }

        if (TextUtils.isEmpty(appId)) {
            throw new IllegalArgumentException("appId is empty");
        }
        this.context = context.getApplicationContext();
        MobileAds.initialize(context, appId);
    }

    public void loadAd(final String unitId) {
        if (TextUtils.isEmpty(unitId)) {
            return;
        }
        if (!adHashMap.containsKey(unitId)) {
            PublisherInterstitialAd ad = new PublisherInterstitialAd(this.context);
            ad.setAdUnitId(unitId);
            adHashMap.put(unitId, ad);
        }
        // PublisherInterstitialAd 必须复用，否则会因为触发gc导致AdActivity的崩溃
        PublisherInterstitialAd ad = adHashMap.get(unitId);
        if (ad == null) {
            return;
        }
        if (!(ad.isLoaded() || ad.isLoading())) {
            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
            ad.loadAd(adRequest);
            ad.setAdListener(new AdListener() {
                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);
                    BusinessLog.d("on interstitial load failed errorCode " + errorCode);
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    BusinessLog.d("on interstitial ads loaded");
                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    BusinessLog.d("on interstitial ads closed");
                    OnAdClosed closed = closedHashMap.get(unitId);
                    if (null != closed) {
                        closedHashMap.remove(unitId);
                        closed.execute();
                    }
                    loadAd(unitId);

                }
            });
        }

    }

    public void showAd(String unitId, OnAdClosed callback) {
        Preconditions.checkNotEmpty(unitId);
        PublisherInterstitialAd ad = adHashMap.get(unitId);
        if (null != ad && ad.isLoaded()) {
            if (null != callback) {
                closedHashMap.put(unitId, callback);
            }
            ad.show();
        } else {
            if (null != callback) {
                callback.execute();
            }
            loadAd(unitId);
        }
    }

    // 广告未加载，或者关闭时会执行此回调函数
    public interface OnAdClosed {
        void execute();
    }

}
