package cn.instreet.business.advertise;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.rd.PageIndicatorView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import cn.instreet.business.R;

/**
 * 滑动SplashView
 * created by yihao 2019/2/12
 *
 * 设计上参考  https://www.mockplus.com/blog/post/android-ios-app-splash-screen
 *
 *           http://www.woshipm.com/pd/1191243.html
 */
public class SlideSplashView extends RelativeLayout {

    private String[] defaultImageUrls;
    private String[] imageUrls;

    private PageIndicatorView pageIndicatorView;
    private ViewPager viewPager;
    private Button buttonStart;

    public SlideSplashView(Context context, String[] defaultImageUrls) {
        super(context);
        this.defaultImageUrls = defaultImageUrls;
        init();
    }

    // 从assets或者sdcard中加载图片
    private void init(){
        LayoutInflater.from(getContext()).inflate(R.layout.activity_pager_splsh, this);
        SplashResourceInfo info = BusinessContentProvider.getSplashResourceInfo(getContext());
        if(null != info) {
            if(!validateData(info)){
                imageUrls = defaultImageUrls;
            }
            else{
                imageUrls = info.imageUrls;
            }
        }
        else{
            imageUrls = defaultImageUrls;
        }
        buttonStart = findViewById(R.id.btn_start);

        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return imageUrls.length;
            }

            @Override
            public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
                return view.equals(o);
            }

            @NonNull
            @Override
            public Object instantiateItem(@NonNull ViewGroup container, int position) {
                ImageView imageView = new ImageView(getContext());
                imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                String url = imageUrls[position];
                if(url.contains("asset")){
                    try {
                        String[] path = url.split("/");
                        InputStream is = getContext().getAssets().open(path[path.length - 1]);
                        Bitmap bitmap = BitmapFactory.decodeStream(is);
                        imageView.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    if(url.contains("storage")||url.contains("sdcard")){
                        try {
                            InputStream is = new FileInputStream(url);
                            Bitmap bitmap = BitmapFactory.decodeStream(is);
                            imageView.setImageBitmap(bitmap);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
                container.addView(imageView);
                return imageView;
            }

            @Override
            public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
                container.removeView((View) object);
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if(i != imageUrls.length - 1){
                    buttonStart.setVisibility(View.INVISIBLE);
                }
                else{
                    buttonStart.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        viewPager.setCurrentItem(0);

        pageIndicatorView = findViewById(R.id.pageIndicatorView);
        pageIndicatorView.setCount(imageUrls.length); // specify total count of indicators
        pageIndicatorView.setSelection(0);


    }

    private boolean validateData(SplashResourceInfo info){
        if(null == info) {
            return false;
        }
        if(null == info.imageUrls){
            return false;
        }
        for (String url:info.imageUrls){
            File file = new File(url);
            if(!file.exists()){
                return false;
            }
        }
        return true;
    }

}
