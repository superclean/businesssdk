package cn.instreet.business.advertise;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;

import cn.instreet.business.R;
import cn.instreet.business.utils.CustomButton;
import cn.instreet.business.utils.CustomRatingBar;

public class NativeAdWrapper extends RelativeLayout {
    private Float ratio = null;

    public NativeAdWrapper(Context context) {
        super(context);
    }

    public NativeAdWrapper(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NativeAdWrapper(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setRatio(float ratio){
        this.ratio = ratio;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if(null != ratio){
            int width = MeasureSpec.getSize(widthMeasureSpec);
            int height = (int) (width * ratio);
            setMeasuredDimension(width, height);
            height = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
            super.onMeasure(widthMeasureSpec, height);
        }
        else{
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }
}

