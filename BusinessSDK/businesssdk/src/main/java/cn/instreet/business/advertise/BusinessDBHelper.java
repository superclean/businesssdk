package cn.instreet.business.advertise;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 商业化模块数据库
 * 包含资源表
 * created by yihao 2019/2/11
 */
public class BusinessDBHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "business_provider.db";
    public static final String TABLE_NAME_AD_RES = "adResource";
    public static final String TABLE_NAME_PREF = "preference";
    private String CREATE_TABLE_AD_RES = "CREATE TABLE "
            + TABLE_NAME_AD_RES + " (_id INTEGER PRIMARY KEY, content TEXT, version INTEGER)";
    private String CREATE_TABLE_PREF = "CREATE TABLE "
            + TABLE_NAME_PREF + " (k TEXT PRIMARY KEY, value TEXT)";
    private static final int DB_VERSION = 1;

    public BusinessDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_AD_RES);
        db.execSQL(CREATE_TABLE_PREF);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
