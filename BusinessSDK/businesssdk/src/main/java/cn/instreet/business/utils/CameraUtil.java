package cn.instreet.business.utils;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;

import java.io.File;

import pub.devrel.easypermissions.EasyPermissions;

/**
 * created by yihao 2019/5/7
 */
public class CameraUtil {
    /**
     * 打开小米手机上的相机应用
     *
     * @param activity
     * @return 如果成功，返回true，否则返回false
     */
    public static boolean openCameraInXiaomi(Activity activity) {
        if (null == activity) {
            return false;
        }
        Intent i = new Intent();
        ComponentName cn = new ComponentName("com.android.camera", "com.android.camera.Camera");
        i.setComponent(cn);
        i.setAction("android.intent.action.MAIN");
        try {
            activity.startActivity(i);
            return true;
        } catch (ActivityNotFoundException exp) {
            return false;
        }
    }

    /**
     * 打开华为手机上的相机应用
     *
     * @param activity
     * @return 如果成功，返回true，否则返回false
     */
    public static boolean openCameraInHuawei(Activity activity) {
        if (null == activity) {
            return false;
        }
        Intent i = new Intent();
        ComponentName cn = new ComponentName("com.huawei.camera", "com.huawei.camera.ThirdCamera");
        i.setComponent(cn);
        i.setAction("android.intent.action.MAIN");
        try {
            activity.startActivity(i);
            return true;
        } catch (ActivityNotFoundException exp) {
            return false;
        }
    }

    /**
     * 自己创建一个Activity来打开相机
     * 照片存储路径 SD卡目录下 /DCIM/Camera/
     * @param activity
     */
    public static void startCameraActivity(Activity activity) {
        if (EasyPermissions.hasPermissions(activity, Manifest.permission.CAMERA)) {
            Intent imageCaptureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            imageCaptureIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            String rootPath = Environment.getExternalStorageDirectory().getAbsolutePath();
            String name = System.currentTimeMillis() + ".jpg";
            File imagePath = new File(rootPath + "/DCIM/Camera/", name);

            Uri uri;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                uri = Uri.parse(imagePath.getAbsolutePath());
            } else {
                imageCaptureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                uri = FileProvider.getUriForFile(activity, activity.getPackageName() + ".fileProvider", imagePath);
            }
            if (!imagePath.exists()) {
                imagePath.getParentFile().mkdir();
            }
            imageCaptureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);// 设置图片输出路径
            imageCaptureIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1); // 图片质量

            try {
                activity.startActivityForResult(imageCaptureIntent, 1);
            } catch (ActivityNotFoundException exp) {
                exp.printStackTrace();
            }
        }
    }
}
