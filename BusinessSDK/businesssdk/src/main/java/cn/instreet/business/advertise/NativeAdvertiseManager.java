package cn.instreet.business.advertise;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.gms.common.internal.Preconditions;

import java.util.HashMap;

import cn.instreet.business.BuildConfig;
import cn.instreet.business.R;
import cn.instreet.business.remoteconfig.BusinessRemoteConfig;
import cn.instreet.business.utils.BusinessLog;
import cn.instreet.business.utils.CustomButton;
import cn.instreet.business.utils.CustomRatingBar;

/**
 * created by yihao 2019/5/20
 */
public class NativeAdvertiseManager {
    public static int STYLE_NORMAL = 0;
    public static int STYLE_MEDIUM = 1;
    private UnifiedNativeAd[] nativeAds;
    private final HashMap<String, Integer> ids = new HashMap<>();
    private Context context;
    private final Handler handler = new Handler();
    private static NativeAdvertiseManager instance;
    private TimeoutRequest[] timeoutRequests;

    /**
     * 超时不展示的广告，会重新请求
     */
    private class TimeoutRequest implements Runnable {
        private final String adUnitId;

        public TimeoutRequest(String adUnitId) {
            this.adUnitId = adUnitId;
        }

        @Override
        public void run() {
            BusinessLog.d("ad " + adUnitId + " cached time out, try load new one");
            destroyAd(adUnitId);
            loadNativeAd(adUnitId);
        }
    }

    public static NativeAdvertiseManager getInstance() {
        if (null == instance) {
            instance = new NativeAdvertiseManager();
        }
        return instance;
    }

    private NativeAdvertiseManager() {
    }

    public void init(Context context, final String appId, final String... unitIds) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotEmpty(appId);
        Preconditions.checkNotNull(unitIds);
        this.context = context.getApplicationContext();

        MobileAds.initialize(NativeAdvertiseManager.this.context, appId);
        nativeAds = new UnifiedNativeAd[unitIds.length];
        timeoutRequests = new TimeoutRequest[unitIds.length];
        for (int i = 0; i < unitIds.length; i++) {
            ids.put(unitIds[i], i);
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                // 预加载所有广告
                preloadAllAds();
            }
        });
    }

    private void preloadAllAds() {
        for (String id : ids.keySet()) {
            loadNativeAd(id);
        }
    }

    /**
     * 显示原生广告
     *
     * @param container 如果不为空，则广告显示在container中，如果为空则弹窗显示
     */
    public boolean showNativeAds(final String id, final ViewGroup container) {
        return showNativeAds(id, container, STYLE_NORMAL, null);
    }

    /**
     * 显示原生广告
     *
     * @param container 如果不为空，则广告显示在container中，如果为空则弹窗显示
     */
    public boolean showNativeAds(final String id, final ViewGroup container, int style, Integer buttonColor) {
        BusinessLog.d("try show native ads id: " + id);
        if (!BusinessRemoteConfig.isEmbededAdsEnable()) {
            return false;
        }
        if (null == container) {
            return false;
        }
        final Integer index = ids.get(id);
        if (null == index) {
            return false;
        }
        if (!isNativeAdsCached(id)) {
            loadNativeAd(id);
            return false;
        }
        BusinessLog.d("show native ads id: " + id);
        cancelTimeOut(index);
        UnifiedNativeAdView adView = buildNativeAdsStyle(index, style, buttonColor);
        NativeAdWrapper wrapper = new NativeAdWrapper(context);
        if(style == STYLE_MEDIUM){
            wrapper.setRatio(0.527f);
        }
        wrapper.addView(adView);
        wrapper.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        container.addView(wrapper);
        // 展示广告后再次进行加载，以提高缓存率
        loadNativeAd(id);
        return true;
    }

    public void loadNativeAd(String id) {
        Integer index = ids.get(id);
        if (null == index) {
            return;
        }

        VideoOptions videoOptions = new VideoOptions.Builder()
                .setStartMuted(false)
                .build();

        NativeAdOptions adOptions = new NativeAdOptions.Builder().
                setAdChoicesPlacement(NativeAdOptions.ADCHOICES_TOP_RIGHT).setVideoOptions(videoOptions).build();
        // 在断网的情况下，build()可能返回null
        AdLoader adLoader = new AdLoader.Builder(NativeAdvertiseManager.this.context, id)
                .forUnifiedNativeAd(new MyNativeAdLoadedListener(id)).withNativeAdOptions(adOptions)
                .withAdListener(new MyAdListener(id)).build();
        if (null != adLoader) {
            BusinessLog.d("load native ad " + id);
            adLoader.loadAd(new AdRequest.Builder().build());
        }
    }

    public boolean isNativeAdsCached(String id) {
        Integer index = ids.get(id);
        if (null == index) {
            return false;
        }
        return null != nativeAds[index];
    }

    private void cancelTimeOut(int index) {
        handler.removeCallbacks(timeoutRequests[index]);
    }

    private UnifiedNativeAdView buildNativeAdsStyle(int index, int style, Integer color) {
        UnifiedNativeAdView unifiedAdView = null;
        if (style == STYLE_NORMAL) {
            unifiedAdView = (UnifiedNativeAdView) View.inflate(context
                    , R.layout.ad_unified2, null);
            populateUnifiedNativeAdView(nativeAds[index], unifiedAdView, color);
        } else if (style == STYLE_MEDIUM) {
            unifiedAdView = (UnifiedNativeAdView) View.inflate(context
                    , R.layout.ad_unified_medium, null);
            populateUnifiedNativeAdViewMedium(nativeAds[index], unifiedAdView, color);
        } else{
            unifiedAdView = (UnifiedNativeAdView) View.inflate(context
                    , R.layout.ad_unified2, null);
            populateUnifiedNativeAdView(nativeAds[index], unifiedAdView, color);
        }
        return unifiedAdView;
    }

    private class MyNativeAdLoadedListener implements UnifiedNativeAd.OnUnifiedNativeAdLoadedListener {
        private String id;
        private Integer index;

        public MyNativeAdLoadedListener(String id) {
            this.id = id;
            index = ids.get(id);
        }

        @Override
        public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
            // 缓存服务器传回的广告，并在一定时间后刷新
            BusinessLog.d("Native ads loaded");
            if (index == null) {
                return;
            }
            nativeAds[index] = unifiedNativeAd;

            BusinessLog.d("AdLoaded body " + unifiedNativeAd.getBody());
            TimeoutRequest requestNewAds = new TimeoutRequest(id);
            timeoutRequests[index] = requestNewAds;
            handler.postDelayed(requestNewAds, BuildConfig.DEBUG ? 60000 : 3600000);
        }
    }

    private class MyAdListener extends AdListener {
        private String id;

        public MyAdListener(String id) {
            this.id = id;
        }

        @Override
        public void onAdFailedToLoad(int errorCode) {
            BusinessLog.d("Failed to load native ad: " + errorCode);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    BusinessLog.d("Retry loading native ad");
                    loadNativeAd(id);
                }
            }, BuildConfig.DEBUG ? 1000 : 5000);
        }

        @Override
        public void onAdClicked() {
            super.onAdClicked();
        }

        @Override
        public void onAdImpression() {
            super.onAdImpression();
            BusinessLog.d("on native ad impression " + id);
        }
    }

    private void destroyAd(String adUnitId) {
        Integer index = ids.get(adUnitId);
        if (null == index) {
            return;
        }
        UnifiedNativeAd ad = nativeAds[index];
        if (null != ad) {
            ad.destroy();
            nativeAds[index] = null;
        }
    }

    public void populateUnifiedNativeAdView(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView, Integer buttonColor) {
        MediaView mediaView = adView.findViewById(R.id.ad_media);
        adView.setMediaView(mediaView);
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        adView.setPriceView(adView.findViewById(R.id.ad_price));
        adView.setStarRatingView(adView.findViewById(R.id.ad_stars));
        adView.setStoreView(adView.findViewById(R.id.ad_store));
        adView.setAdvertiserView(adView.findViewById(R.id.ad_advertiser));
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());

        VideoController vc = nativeAd.getVideoController();

        if (vc.hasVideoContent()) {
            vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                @Override
                public void onVideoEnd() {
                    super.onVideoEnd();
                }
            });
        }
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }

        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((CustomButton) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
            if(null != buttonColor){

            ((CustomButton) adView.getCallToActionView()).setButtonColor(buttonColor);
            }

        }

        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.GONE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.GONE);
        } else {
            ((CustomRatingBar) adView.getStarRatingView())
                    .setStar(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        ((CustomRatingBar) adView.getStarRatingView())
                .setStar(4.5f);
        adView.getStarRatingView().setVisibility(View.VISIBLE);

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }

    public void populateUnifiedNativeAdViewMedium(UnifiedNativeAd nativeAd, UnifiedNativeAdView adView,
                                                  Integer buttonColor) {
        MediaView mediaView = adView.findViewById(R.id.ad_media);
        adView.setMediaView(mediaView);
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        VideoController vc = nativeAd.getVideoController();

        if (vc.hasVideoContent()) {
            vc.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks() {
                @Override
                public void onVideoEnd() {
                    super.onVideoEnd();
                }
            });
        }
        if (nativeAd.getBody() == null) {
            adView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            adView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }

        if (nativeAd.getCallToAction() == null) {
            adView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            adView.getCallToActionView().setVisibility(View.VISIBLE);
            ((CustomButton) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
            if(null != buttonColor){
            ((CustomButton) adView.getCallToActionView()).setButtonColor(buttonColor);
            }
        }

        if (nativeAd.getIcon() == null) {
            adView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        adView.setNativeAd(nativeAd);
    }
}
