package cn.instreet.business.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import cn.instreet.business.R;

/**
 * 自定义Button
 * created by yihao 2019/1/21
 */
public class CustomButton extends android.support.v7.widget.AppCompatButton {
    private float roundCornerSize;
    private int normalBgColor;
    private int normalTextColor;
    private float normalStrokeWidth;
    private int normalStrokeColor;

    private int pressedBgColor;
    private int pressedTextColor;
    private float pressedStrokeWidth;
    private int pressedStrokeColor;

    private int disableBgColor;
    private int disableTextColor;
    private float disableStrokeWidth;
    private int disableStrokeColor;
    public CustomButton(Context context) {
        super(context);
        init();
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseAttributes(attrs);
        init();

    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        parseAttributes(attrs);
        init();
    }

    private void parseAttributes(AttributeSet attrs){
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CustomButton);
        roundCornerSize = typedArray.getDimension(R.styleable.CustomButton_cornerSize, 0);
        normalBgColor = typedArray.getColor(R.styleable.CustomButton_normalBackgroundColor, Color.WHITE);
        normalTextColor = typedArray.getColor(R.styleable.CustomButton_normalTextColor, Color.WHITE);
        normalStrokeWidth = typedArray.getDimension(R.styleable.CustomButton_normalStrokeWidth, 0);
        normalStrokeColor = typedArray.getColor(R.styleable.CustomButton_normalStrokeColor, Color.TRANSPARENT);

        pressedBgColor = typedArray.getColor(R.styleable.CustomButton_pressedBackgroundColor, Color.WHITE);
        pressedTextColor = typedArray.getColor(R.styleable.CustomButton_pressedTextColor, Color.WHITE);
        pressedStrokeWidth = typedArray.getDimension(R.styleable.CustomButton_pressedStrokeWidth, 0);
        pressedStrokeColor = typedArray.getColor(R.styleable.CustomButton_pressedStrokeColor, Color.TRANSPARENT);

        disableBgColor = typedArray.getColor(R.styleable.CustomButton_disableBackgroundColor, Color.WHITE);
        disableTextColor = typedArray.getColor(R.styleable.CustomButton_disableTextColor, Color.WHITE);
        disableStrokeWidth = typedArray.getDimension(R.styleable.CustomButton_disableStrokeWidth, 0);
        disableStrokeColor = typedArray.getColor(R.styleable.CustomButton_disableStrokeColor, Color.TRANSPARENT);
        typedArray.recycle();
    }

    private void init() {
        setEnabled(isEnabled());
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:
                        GradientDrawable gd = new GradientDrawable();//创建drawable
                        gd.setColor(pressedBgColor);
                        gd.setCornerRadius(roundCornerSize);
                        gd.setStroke((int) pressedStrokeWidth, pressedStrokeColor);
                        setBackgroundDrawable(gd);
                        setTextColor(pressedTextColor);
                        break;
                    case MotionEvent.ACTION_UP:
                        gd = new GradientDrawable();//创建drawable
                        gd.setColor(normalBgColor);
                        gd.setCornerRadius(roundCornerSize);
                        gd.setStroke((int) normalStrokeWidth, normalStrokeColor);
                        setBackgroundDrawable(gd);
                        setTextColor(normalTextColor);
                        performClick();
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if(enabled){
            GradientDrawable gd = new GradientDrawable();//创建drawable
            gd.setColor(normalBgColor);
            gd.setCornerRadius(roundCornerSize);
            gd.setStroke((int) normalStrokeWidth, normalStrokeColor);
            setBackgroundDrawable(gd);
            setTextColor(normalTextColor);
        }
        else{
            GradientDrawable gd = new GradientDrawable();//创建drawable
            gd.setColor(disableBgColor);
            gd.setCornerRadius(roundCornerSize);
            gd.setStroke((int) disableStrokeWidth, disableStrokeColor);
            setBackgroundDrawable(gd);
            setTextColor(disableTextColor);
        }
    }

    public void setButtonColor(int color){
        GradientDrawable gd = new GradientDrawable();//创建drawable
        gd.setColor(color);
        gd.setCornerRadius(roundCornerSize);
        gd.setStroke((int) normalStrokeWidth, normalStrokeColor);
        setBackgroundDrawable(gd);
        setTextColor(normalTextColor);
    }
}
