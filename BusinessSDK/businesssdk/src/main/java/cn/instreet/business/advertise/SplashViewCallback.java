package cn.instreet.business.advertise;

/**
 * created by yihao 2019/2/14
 */
public interface SplashViewCallback {
    int RESOURCE_NOT_FOUND = 1;

    int BAD_RESOURCE = 2;

    String MESSAGE_RESOURCE_NOT_FOUND = "splash resource info not found";

    String MESSAGE_BAD_RESOURCE = "splash resource file not found or broken";

    void onSkip();

    void onFinish();

    void onError(int errorCode, String errorMessage);
}
