package cn.instreet.business.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import cn.instreet.business.R;


/**
 * Created by Jiali on 2019/2/21.
 */

public class DoubleChoicesDialog extends Dialog {

    private TextView mTvTitle;
    private TextView mTvLeftButton;
    private TextView mTvRightButton;
    private String mStringTitle;
    private String mStringLeftButton;
    private String mStringRightButton;
    private OnButtonClickListener mOnButtonClickListener;

    public DoubleChoicesDialog(@NonNull Context context) {
        super(context);
    }

    public DoubleChoicesDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected DoubleChoicesDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.double_choices_dialog_layout);
        initView();
        initData();
        initListener();
    }

    private void initView() {
        mTvTitle = findViewById(R.id.dialog_title);
        mTvLeftButton = findViewById(R.id.dialog_left_choice);
        mTvRightButton = findViewById(R.id.dialog_right_choice);
    }

    private void initData() {
        mTvTitle.setText(mStringTitle);
        mTvLeftButton.setText(mStringLeftButton);
        mTvRightButton.setText(mStringRightButton);
    }

    private void initListener() {
        if (mOnButtonClickListener != null) {
            mTvLeftButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnButtonClickListener.leftButtonClick();
                }
            });
            mTvRightButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnButtonClickListener.rightButtonClick();
                }
            });
        }
    }

    public void setTitle(@NonNull String title) {
        mStringTitle = title;
    }

    public void setLeftButtonText(@NonNull String text) {
        mStringLeftButton = text;
    }

    public void setRightButtonText(@NonNull String text) {
        mStringRightButton = text;
    }

    public interface OnButtonClickListener {
        void leftButtonClick();

        void rightButtonClick();
    }

    public void setOnButtonClickListener(OnButtonClickListener listener) {
        mOnButtonClickListener = listener;
    }


}
