package cn.instreet.business.global;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.app.KeyguardManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import cn.instreet.business.BuildConfig;
import cn.instreet.business.Constants;
import cn.instreet.business.IGlobal;
import cn.instreet.business.advertise.AdvertiseManager;
import cn.instreet.business.advertise.BusinessContentProvider;
import cn.instreet.business.advertise.SplashDownloader;
import cn.instreet.business.advertise.SplashResourceInfo;
import cn.instreet.business.battery.BatteryLevelObserver;
import cn.instreet.business.battery.BatteryReminder;
import cn.instreet.business.battery.BatteryReminderView;
import cn.instreet.business.lockscreen.LockScreen;
import cn.instreet.business.packages.PackageChangeReceiver;
import cn.instreet.business.phone.PhoneCallAlerter;
import cn.instreet.business.remoteconfig.BusinessRemoteConfig;
import cn.instreet.business.utils.BusinessLog;
import cn.instreet.business.utils.CustomTimer;
import pub.devrel.easypermissions.EasyPermissions;

import static cn.instreet.business.Constants.TAG;
import static cn.instreet.business.advertise.AdvertiseManager.MODE_HIDE;

/**
 * created by yihao 2018/12/20
 * 商业化SDK 后台服务
 */
public class GlobalService extends Service {

    public static final String ACTION_PHONE_HANGOFF = "business.intent.action.phone_hang_off";
    public static final String EXTRA_PHONE_NUMBER = "business.intent.extra.phone_number";
    public static final String EXTRA_PRE_ADS = "business.intent.extra.pre_ads";
    public static final String EXTRA_POST_ADS = "business.intent.extra.post_ads";
    public static final String EXTRA_NATIVE_ADS = "business.intent.extra.native_ads";
    public static final String EXTRA_MEMORY_USING_PERCENT = "business.intent.extra.memory_using_percent";
    public static final String EXTRA_PROCESS_COUNT = "business.intent.extra.process_count";
    public static final String ACTION_GLOBAL_CONNECT = "business.intent.action.global_connect";
    public static final String ACTION_GLOBAL_BLUETOOTH_TURN_ON = "business.intent.action.global_bluetooth_turn_on";
    public static final String ACTION_GLOBAL_BLUETOOTH_TURN_OFF = "business.intent.action.global_bluetooth_turn_off";
    public static final String ACTION_GLOBAL_HEADSET_PLUG_IN = "business.intent.action.global_headset_plug_in";
    public static final String ACTION_GLOBAL_HEADSET_PLUG_OFF = "business.intent.action.global_headset_plug_off";
    public static final String ACTION_GLOBAL_HOMEKEY_PRESSED = "business.intent.action.global_homekey_pressed";
    public static final String ACTION_GLOBAL_MEMORY_USING_HIGH = "business.intent.action.global_memory_using_high";

    private boolean isCharging = false;
    private boolean isOnCalling = false;
    private int batteryLevel;
    // 电量比例，默认100
    private int batteryScale;
    private final BatteryStateBroadcastReceiver batteryStateReceiver;
    private boolean isRinging;
    private CustomPhoneStateListener phoneStateListener;
    private BluetoothObserver bluetoothObserver;
    private ConnectionObserver connectionObserver;
    private HeadsetObserver headsetObserver;
    private HomekeyObserver homekeyObserver;
    private SystemMonitor systemMonitor;
    private ScreenOffReceiver screenOffReceiver;
    private PackageChangeReceiver packageChangeReceiver;
    private PhoneCallAlerter phoneCallAlerter;
    private BatteryReminder batteryReminder;

    private static LockScreen lockScreen;
    private CustomTimer phoneStateTimer = null;

    public GlobalService() {
        batteryStateReceiver = new BatteryStateBroadcastReceiver();
    }

    @Override
    public IBinder onBind(Intent intent) {
        BusinessLog.d("global service onBind");
        return new IGlobal.Stub() {
            @Override
            public boolean isCharging() {
                return isCharging;
            }

            @Override
            public boolean isOnCalling() {
                return isOnCalling;
            }

            @Override
            public int getBatteryLevel() {
                if (batteryScale != 0) {
                    return batteryLevel * 100 / batteryScale;
                } else {
                    return 50;
                }
            }

            @Override
            public boolean isLockScreenOn() {
                return isScreenLockOn();
            }

            @Override
            public void listenAppChange(String[] packageNames) {
                if (null != packageChangeReceiver) {
                    packageChangeReceiver.setInterestPackageNames(packageNames);
                }
            }

            @Override
            public void unlistenAppChange() {
                if (null != packageChangeReceiver) {
                    packageChangeReceiver.setInterestPackageNames(null);
                }
            }

            @Override
            public void enableCallReminder(final String customViewClass) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && EasyPermissions.hasPermissions(
                        GlobalService.this, Manifest.permission.READ_PHONE_STATE)) {
                    listenPhoneState();
                }
            }

            @Override
            public void disableCallReminder() {
                phoneCallAlerter.stop();
            }
        };
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        BusinessLog.d("GlobalService onStartCommand");
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        BusinessLog.d("GlobalService onCreate");
        initService();
    }



    private void initService() {
        initRemoteConfig();

        initGlobalModule();

        initLockscreenModule();

        initPackageModule();

        initPhoneModule();

        initBatteryModule();
    }

    private void initRemoteConfig() {
        BusinessRemoteConfig.init(getApplicationContext());

        long fetchInterval = BuildConfig.DEBUG ? 30 * 1000 : 3600 * 1000;

        CustomTimer timer = new CustomTimer(0, fetchInterval, new Runnable() {
            @Override
            public void run() {
                BusinessRemoteConfig.updateConfig();
            }
        });
        timer.start();
    }

    /**
     * 初始化全能模块
     */
    private void initGlobalModule() {
        bluetoothObserver = new BluetoothObserver(this);
        connectionObserver = new ConnectionObserver(this);
        headsetObserver = new HeadsetObserver(this);
        homekeyObserver = new HomekeyObserver(this);
        systemMonitor = new SystemMonitor(this);
        bluetoothObserver.start();
        connectionObserver.start();
        headsetObserver.start();
        homekeyObserver.start();
        systemMonitor.start();
    }

    private void initBatteryModule() {
        // 充电提醒模块初始化
        batteryReminder = new BatteryReminder(this);
        batteryReminder.listenBatteryChanged(null);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_BATTERY_CHANGED); // 监听充电状态
        registerReceiver(batteryStateReceiver, filter);
    }

    private void initLockscreenModule() {
        lockScreen = new LockScreen(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_USER_PRESENT);
        screenOffReceiver = new ScreenOffReceiver();
        getApplicationContext().registerReceiver(screenOffReceiver, filter);
    }

    private void initPackageModule() {
        packageChangeReceiver = new PackageChangeReceiver();
        packageChangeReceiver.register(getApplicationContext());
    }

    private void initPhoneModule() {
        phoneCallAlerter = new PhoneCallAlerter(this.getApplicationContext());
        boolean isEnable = getSharedPreferences(Constants.PREF_NAME_BUSINESS, Context.MODE_PRIVATE).getBoolean(Constants.PREF_KEY_PHONE, false);
        if (isEnable) {
            phoneCallAlerter.start();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            phoneStateTimer = new CustomTimer(0, 1000, new Runnable() {
                @Override
                public void run() {
                    if (EasyPermissions.hasPermissions(GlobalService.this, Manifest.permission.READ_PHONE_STATE)) {
                        listenPhoneState();
                        phoneStateTimer.stop();
                    }
                }
            });
            phoneStateTimer.start();
        }
//        else {
//            listenPhoneState();
//        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusinessLog.d("global service destroy");
        try {
            unregisterReceiver(batteryStateReceiver);
            bluetoothObserver.stop();
            connectionObserver.stop();
            headsetObserver.stop();
            homekeyObserver.stop();
        } catch (IllegalArgumentException exp) {
            exp.printStackTrace();
        }
    }

    private class BatteryStateBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_BATTERY_CHANGED.equals(intent.getAction())) {
                int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
                isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                        status == BatteryManager.BATTERY_STATUS_FULL;

                batteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 50);
                batteryScale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
                Log.d(TAG, "battery state changed isCharging: " + isCharging + " battery percent " + batteryLevel * 100 / batteryScale);
            }
        }
    }

    private class CustomPhoneStateListener extends PhoneStateListener {
        @Override
        public void onCallStateChanged(int state, String phoneNumber) {
            super.onCallStateChanged(state, phoneNumber);
            Log.d(TAG, "call state changed isOnCalling: " + state + " phoneNumber: " + phoneNumber);
            if (state == TelephonyManager.CALL_STATE_IDLE) {
                isOnCalling = false;
                if (isRinging) {
                    Intent intent = new Intent(ACTION_PHONE_HANGOFF);
                    intent.putExtra(EXTRA_PHONE_NUMBER, phoneNumber);
                    GlobalService.this.sendBroadcast(intent);
                    isRinging = false;
                }
            } else if (state == TelephonyManager.CALL_STATE_RINGING) {
                isRinging = true;
                isOnCalling = true;
            } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                isRinging = true;
                isOnCalling = true;
            }

        }
    }

    private boolean isScreenLockOn() {
        KeyguardManager mKeyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        if (mKeyguardManager != null) {
            return mKeyguardManager.isKeyguardLocked();
        } else {
            return false;
        }
    }

    private class ScreenOffReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_SCREEN_OFF.equals(intent.getAction())) {
                Log.d(TAG, "on screen off");
                if (null != lockScreen) {
                    lockScreen.showLockScreenActivity();
                }

            } else if (Intent.ACTION_SCREEN_ON.equals(intent.getAction())) {
                Log.d(TAG, "on screen on");
            } else if (Intent.ACTION_USER_PRESENT.equals(intent.getAction())) {
                Log.d(TAG, "on user present");
            }
        }
    }

    private void listenPhoneState() {
        BusinessLog.d("begin listenPhoneState");
        // 监听来电状态
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            phoneStateListener = new CustomPhoneStateListener();
            telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }
    }
}
