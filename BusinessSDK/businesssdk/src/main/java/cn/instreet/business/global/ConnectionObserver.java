package cn.instreet.business.global;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.os.Build;

import java.util.Timer;
import java.util.TimerTask;

import cn.instreet.business.Constants;
import cn.instreet.business.utils.BusinessLog;

import static cn.instreet.business.global.GlobalService.ACTION_GLOBAL_CONNECT;

/**
 * 全能模块网络连接检测
 * created by yihao 2019/1/4
 */
public class ConnectionObserver extends BaseObserver {
    private final String STATE_DISCONNECT = "disconnect";
    private final String STATE_CONNECT = "connect";
    private Timer timer;
    private TimerTask timerTask;
    private ConnectivityManager connectivityManager;
    private String connectState = STATE_DISCONNECT;

    public ConnectionObserver(Context context) {
        super(context);
        try {
            connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        } catch (ClassCastException exp) {
            exp.printStackTrace();
        }
    }

    public void start() {
        timer = new Timer();
        timerTask = new TimerTask() {
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if(!isEnable()){
                        return;
                    }

                    if (null == connectivityManager) {
                        return;
                    }

                    NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
                    if (null == networkCapabilities) {
                        connectState = STATE_DISCONNECT;
                    } else {
                        boolean isConnected = networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED);
                        if (STATE_DISCONNECT.equals(connectState) && isConnected) {
                            BusinessLog.d("observe internet connected");
                            Intent intent = new Intent(ACTION_GLOBAL_CONNECT);
                            context.sendBroadcast(intent);
                        }
                        connectState = isConnected ? STATE_CONNECT : STATE_DISCONNECT;
                    }

                }
            }
        };
        timer.schedule(timerTask, 0, 1000);
    }

    public void stop() {
        timer.cancel();
        timerTask.cancel();
    }

    private boolean isEnable() {
        return context.getSharedPreferences(Constants.PREF_NAME_BUSINESS, Context.MODE_PRIVATE)
                .getBoolean(Constants.PREF_KEY_GLOBAL, false);
    }
}
