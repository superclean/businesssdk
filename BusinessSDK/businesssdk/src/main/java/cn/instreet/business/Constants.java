package cn.instreet.business;

public class Constants {
    public static final String TAG = "Business";

    public static final String SWITCH_PACKAGE = "switch_package";
    public static final String SWITCH_LOCKSCREEN = "switch_lockscreen";
    public static final String SWITCH_HEADSET = "switch_headset";
    public static final String SWITCH_INTERNET = "switch_internet";
    public static final String SWITCH_BLUETOOTH = "switch_bluetooth";
    public static final String SWITCH_HOME = "switch_home";

    // 商业化SDK的Preference名称
    public static final String PREF_NAME_BUSINESS = "pref_name_business";
    // 来电提醒的Preference key
    public static final String PREF_KEY_PHONE = "pref_key_phone";
    // 电量提醒的Preference key
    public static final String PREF_KEY_BATTERY = "pref_key_battery";
    // 包安装监听的Preference key
    public static final String PREF_KEY_PACKAGE = "pref_key_package";
    // 要监听的所有包名的Preference key
    public static final String PREF_KEY_PACKAGE_NAMES = "pref_key_package_names";
    // 全能模块的Preference key
    public static final String PREF_KEY_GLOBAL = "pref_key_global";
    // 锁屏模式的Preference key
    public static final String PREF_KEY_LOCKSCREEN_MODE = "pref_key_lockscreen_mode";
    // 锁屏密码模式的Preference key
    public static final String PREF_KEY_LOCKSCREEN_PASSWORD_MODE = "pref_key_lockscreen_password_mode";
    // Pin码Value的Preference key
    public static final String PREF_KEY_PIN = "pref_key_pin";
    // 手势Value的Preference key
    public static final String PREF_KEY_PATTERN = "pref_key_pattern";
    // 普通模式锁屏icon资源的Preference key
    public static final String PREF_KEY_LOCKSCREEN_ICON = "pref_key_lockscreen_icon";
    // 普通模式锁屏的标题的Preference key
    public static final String PREF_KEY_LOCKSCREEN_TITLE = "pref_key_lockscreen_title";
    // 普通模式锁屏的center view class的Preference key
    public static final String PREF_KEY_LOCKSCREEN_CENTER_VIEW = "pref_key_lockscreen_center_view";
    // 普通模式锁屏的bottom view的Preference key
    public static final String PREF_KEY_LOCKSCREEN_BOTTOM_VIEW = "pref_key_lockscreen_bottom_view";

}
