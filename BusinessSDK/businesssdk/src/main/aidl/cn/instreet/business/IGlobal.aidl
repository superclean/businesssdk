// IGlobal.aidl
package cn.instreet.business;


interface IGlobal {
    boolean isCharging();

    boolean isOnCalling();

    int getBatteryLevel();

    boolean isLockScreenOn();

    void listenAppChange(in String[] packageNames);

    void unlistenAppChange();

    void enableCallReminder(String customViewClass);

    void disableCallReminder();

}
