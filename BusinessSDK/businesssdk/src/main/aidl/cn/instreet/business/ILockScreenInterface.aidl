// ILockScreenInterface.aidl
package cn.instreet.business;

// Declare any non-default types here with import statements

interface ILockScreenInterface {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat,
            double aDouble, String aString);

    void showLockScreen(int iconResId, String title, String centerFragmentClass);

    void showLockScreenWithBottom(int iconResId, String title, String bottomViewClass);

    void hideLockScreen();
}
