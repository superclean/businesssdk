package com.instreet.businessdemo;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.concurrent.Executors;

import cn.instreet.business.BusinessSDK;
import cn.instreet.business.advertise.BannerAdvertiseManager;
import cn.instreet.business.advertise.InterstitialAdvertiseManager;
import cn.instreet.business.advertise.NativeAdvertiseManager;
import cn.instreet.business.utils.BusinessLog;

import static cn.instreet.business.BusinessSDK.ACTION_CHARGER_PLUG_IN;
import static cn.instreet.business.BusinessSDK.ACTION_CHARGER_PLUG_OFF;

public class MainActivity extends AppCompatActivity {

    private BusinessSDK sdk;
    private NativeAdvertiseManager advertiseManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BusinessSDK.InitSDK(getApplicationContext(), new BusinessSDK.OnSDKInitListener() {
            @Override
            public void onInit(BusinessSDK sdk) {
                MainActivity.this.sdk = sdk;
            }
        });

        TextView lockscreen = findViewById(R.id.tv_lockscreen);
        lockscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sdk.startLockScreenPreferenceActivity();
            }
        });

        final Switch appChange = findViewById(R.id.switch_package);
        appChange.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (sdk == null) {
                    return;
                }
                if (isChecked) {
                    // 监听halor和superclean这两个应用的变化
                    sdk.listenAppChange(new String[]{"com.example.halor.myapplication", "cn.instreet.superclean"});
                } else {
                    sdk.disableListenAppChange();
                }
            }
        });

        Switch phone = findViewById(R.id.switch_phone);
        phone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (sdk == null) {
                    return;
                }
                if (isChecked) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                    }
                    sdk.enablePhoneRemind(CustomView.class);
                } else {
                    sdk.disablePhoneRemind();
                }
            }
        });

        Switch battery = findViewById(R.id.switch_battery_reminder);
        battery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (sdk == null) {
                    return;
                }
                if (isChecked) {
                    sdk.enableBatteryReminder();
                } else {
                    sdk.disableBatteryReminder();
                }
            }
        });

        Switch global = findViewById(R.id.switch_global);
        global.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (sdk == null) {
                    return;
                }
                if (isChecked) {
                    sdk.enableGlobalModules();
                } else {
                    sdk.disableGlobalModules();
                }
            }
        });

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BusinessSDK.ACTION_PACKAGE_ADDED);
        intentFilter.addAction(BusinessSDK.ACTION_PACKAGE_REMOVED);
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                BusinessLog.d("package " + action + " " + intent.getStringExtra("package_name") + " " + intent.getStringExtra("app_name"));
            }
        }, intentFilter);

        final ViewGroup group = findViewById(R.id.ads_container);
        NativeAdvertiseManager.getInstance().init(this, "ca-app-pub-3940256099942544~3347511713", "ca-app-pub-3940256099942544/2247696110");
        Executors.newSingleThreadExecutor().submit(new Runnable() {
            @Override
            public void run() {
                while(!NativeAdvertiseManager.getInstance().isNativeAdsCached("ca-app-pub-3940256099942544/2247696110")){
//                    advertiseManager.loadNativeAd(adId1);
                    SystemClock.sleep(1000);
                }
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        NativeAdvertiseManager.getInstance().showNativeAds("ca-app-pub-3940256099942544/2247696110",
//                                group, NativeAdvertiseManager.STYLE_NORMAL, Color.RED);
//                    }
//                });
            }
        });

        BannerAdvertiseManager.getInstance().init(this, "ca-app-pub-3940256099942544~3347511713");
        group.addView(BannerAdvertiseManager.getInstance().showLargeBanner("ca-app-pub-3940256099942544/6300978111"));
//
        InterstitialAdvertiseManager.getInstance().init(this, "ca-app-pub-3940256099942544~3347511713");
        InterstitialAdvertiseManager.getInstance().loadAd("ca-app-pub-3940256099942544/1033173712");

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_CHARGER_PLUG_IN);
        filter.addAction(ACTION_CHARGER_PLUG_OFF);
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                BusinessLog.d("receive charger action " + intent.getAction());
            }
        }, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        BusinessLog.stack();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

//        final ViewGroup group = findViewById(R.id.ads_container);
//        advertiseManager = NativeAdvertiseManager.getInstance();
//        final String adId1 = "ca-app-pub-3940256099942544/2247696110";
//        advertiseManager.showNativeAds(adId1, group);
//        group.postDelayed(new Runnable() {
//            @Override
//            public void run() {
                InterstitialAdvertiseManager.getInstance().showAd("ca-app-pub-3940256099942544/1033173712", new InterstitialAdvertiseManager.OnAdClosed() {
                    @Override
                    public void execute() {

                    }
                });
//            }
//        }, 5000);

    }
}
