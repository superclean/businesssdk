package com.instreet.businessdemo;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * created by yihao 2018/12/25
 */
public class CenterCustomView extends RelativeLayout {
    public CenterCustomView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.custom_view, this);
        init();
    }

    public CenterCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.custom_view, this);
        init();
    }

    public CenterCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.custom_view, this);
        init();
    }

    private void init() {
        TextView textView = findViewById(R.id.tv_title);
        textView.setText("Center View");
    }
}
