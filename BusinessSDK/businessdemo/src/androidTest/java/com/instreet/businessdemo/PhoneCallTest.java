package com.instreet.businessdemo;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Method;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.uiautomator.By;
import androidx.test.uiautomator.BySelector;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiObjectNotFoundException;
import androidx.test.uiautomator.UiSelector;
import androidx.test.uiautomator.Until;
import cn.instreet.business.advertise.FrontAdvertiseActivity;
import cn.instreet.business.remoteconfig.BusinessRemoteConfig;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static com.instreet.businessdemo.Util.dumpWindowHierarchy;
import static com.instreet.businessdemo.Util.logLongString;
import static com.instreet.businessdemo.Util.sleep;
import static junit.framework.TestCase.fail;

/**
 * created by yihao 2019/1/10
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class PhoneCallTest {
    UiDevice device;
    Context context;
    private String BASIC_SAMPLE_PACKAGE = "com.instreet.businessdemo";

    @Rule
    public GrantPermissionRule mRuntimePermissionRule = GrantPermissionRule.grant(Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_SECURE_SETTINGS,
            Manifest.permission.READ_CONTACTS, Manifest.permission.PROCESS_OUTGOING_CALLS, Manifest.permission.CALL_PHONE);

    @Before
    public void setup() throws UiObjectNotFoundException {
        context = ApplicationProvider.getApplicationContext();
        device = UiDevice.getInstance(getInstrumentation());

        BusinessRemoteConfig.init(context);
        // 拉起主应用
        Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(BASIC_SAMPLE_PACKAGE);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        sleep(5);

        // 开启来电提醒
        UiObject switcher = device.findObject(new UiSelector()
                .descriptionContains("switch_phone"));
        if (!switcher.isChecked()) {
            switcher.click();
        }

        sleep(1);
    }

    @Test
    public void testFrontAds() throws UiObjectNotFoundException {
        BusinessRemoteConfig.changePhoneAdsMode("front");
        makePhoneCall();

        // 检查是否有前置插屏广告
        UiObject ads = device.findObject(new UiSelector().descriptionContains("interstitial"));
        if (!ads.exists()) {
            fail();
        }
    }

    @Test
    public void testBackAds() throws UiObjectNotFoundException {
        BusinessRemoteConfig.changePhoneAdsMode("back");

        makePhoneCall();

        // 检查是否有前置插屏广告
        UiObject iv_back = device.findObject(new UiSelector().descriptionContains("iv_back"));
        iv_back.click();

        Util.sleep(1);

        UiObject ads = device.findObject(new UiSelector().descriptionContains("interstitial"));
        if (!ads.exists()) {
            fail();
        }
    }

    @Test
    public void testNativeAds() throws UiObjectNotFoundException {
        BusinessRemoteConfig.changePhoneAdsMode("native");

        makePhoneCall();

        // 检查是否有原生广告
        Util.sleep(5);

        UiObject ads_close = device.findObject(new UiSelector().descriptionContains("native_ads_close"));
        if (!ads_close.exists()) {
            fail();
        }
    }

    private void makePhoneCall() throws UiObjectNotFoundException {
        // 多等一下比较保险
        Util.sleep(5);
        // 拨打电话
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri data = Uri.parse("tel:" + "112");
        intent.setData(data);
        context.startActivity(intent);
        device.waitForIdle();
        Util.sleep(5);
        Util.logLongString(dumpWindowHierarchy());
        UiObject dial = device.findObject(new UiSelector()
                .description("dial"));
        if(dial.exists()){
            dial.click();
        }
        else {
            UiObject call = device.findObject(new UiSelector()
                    .description("Call"));
            if(call.exists()){
                call.click();
            }
            else{
                fail();
            }
        }

        Util.sleep(5);
        Util.logLongString(dumpWindowHierarchy());
        device.findObject(new UiSelector()
                .descriptionContains("End Call")).click();
        Util.sleep(5);

        // 挂断
//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.P){
//
//        }
//        else{
//            try {
//                // Get the boring old TelephonyManager
//                TelephonyManager telephonyManager =
//                        (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//
//                // Get the getITelephony() method
//                Class classTelephony = Class.forName(telephonyManager.getClass().getName());
//                Method methodGetITelephony = classTelephony.getDeclaredMethod("getITelephony");
//
//                // Ignore that the method is supposed to be private
//                methodGetITelephony.setAccessible(true);
//
//                // Invoke getITelephony() to get the ITelephony interface
//                Object telephonyInterface = methodGetITelephony.invoke(telephonyManager);
//
//                // Get the endCall method from ITelephony
//                Class telephonyInterfaceClass =
//                        Class.forName(telephonyInterface.getClass().getName());
//                Method methodEndCall = telephonyInterfaceClass.getDeclaredMethod("endCall");
//
//                // Invoke endCall()
//                methodEndCall.invoke(telephonyInterface);
//
//            } catch (Exception ex) { // Many things can go wrong with reflection calls
//                ex.printStackTrace();
//            }
//        }

    }
}
