package com.instreet.businessdemo;

import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;

import androidx.test.uiautomator.UiDevice;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

/**
 * created by yihao 2019/1/10
 */
public class Util {
    public static void sleep(int seconds) {
        try {
            Thread.currentThread().sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String dumpWindowHierarchy() {
        UiDevice device = UiDevice.getInstance(getInstrumentation());
        final StringBuffer buffer = new StringBuffer();
        OutputStream os = new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                buffer.append((char) b);
            }
        };
        try {
            device.dumpWindowHierarchy(os);
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer.toString();
    }

    public static void logLongString(String str) {
        if (TextUtils.isEmpty(str)) {
            return;
        }
        String line;
        BufferedReader reader = new BufferedReader(new StringReader(str));
        try {
            while (null != (line = reader.readLine())) {
                Log.e("autotest", line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
