package com.instreet.businessdemo;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Button;
import android.widget.Switch;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiObjectNotFoundException;
import androidx.test.uiautomator.UiSelector;
import androidx.test.uiautomator.Until;
import ezy.assist.compat.SettingsCompat;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by 易昊 on 2018/12/20.
 * 测试如下功能：
 * 1 开启应用安装监控后
 * 2 安装Halor.apk，会进行弹窗
 * 3 再安装其他应用，会进行弹窗
 * 4 卸载Halor.apk，再安装其他应用，不弹窗
 * 5 安装Halor.apk，然后卸载Halor.apk，1分钟内，再重装，会进行弹窗
 * 6 安装Halor.apk，然后卸载Halor.apk，1分钟内，不重装，会进行弹窗
 * <p>
 * 注意：测试时手机要关闭充电。
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class AppInstallTest {
    UiDevice device;
    Context context;
    private String BASIC_SAMPLE_PACKAGE = "com.instreet.businessdemo";

    @Rule
    public GrantPermissionRule mRuntimePermissionRule = GrantPermissionRule.grant(Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_SECURE_SETTINGS);

    @Before
    public void setup() throws UiObjectNotFoundException {
        context = ApplicationProvider.getApplicationContext();
        device = UiDevice.getInstance(getInstrumentation());

        // 授予权限
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            PackageManager pm = context.getPackageManager();
            //授予应用安装权限
            if (!pm.canRequestPackageInstalls()) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse("package:" + BASIC_SAMPLE_PACKAGE));
                context.startActivity(intent);
                device.waitForIdle();
                sleep(5);
                UiObject object = device.findObject(new UiSelector()
                        .className(Switch.class));
                if (object.exists() && !object.isChecked()) {
                    object.click();
                }
            }

            if (!SettingsCompat.canDrawOverlays(context)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse("package:" + BASIC_SAMPLE_PACKAGE));
                context.startActivity(intent);
                device.waitForIdle();
                sleep(5);
                UiObject object = device.findObject(new UiSelector()
                        .className(Switch.class));
                if (object.exists() && !object.isChecked()) {
                    object.click();
                }
            }
        }
        else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            try {
                if(0 == Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.INSTALL_NON_MARKET_APPS)){
                    Settings.Secure.putInt(context.getContentResolver(), Settings.Secure.INSTALL_NON_MARKET_APPS, 1);
                }
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            if (!SettingsCompat.canDrawOverlays(context)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse("package:" + BASIC_SAMPLE_PACKAGE));
                context.startActivity(intent);
                device.waitForIdle();
                UiObject object = device.findObject(new UiSelector()
                        .className(Switch.class));
                if (object.exists() && !object.isChecked()) {
                    object.click();
                }
            }
        }

        // 拷贝要安装的apk
        File file = new File("mnt/sdcard/test/Halor.apk");
        copyApkToSdcard("Halor.apk");
        copyApkToSdcard("demo.apk");
        copyApkToSdcard("Super-Clean.apk");

        // 拉起主应用
        Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(BASIC_SAMPLE_PACKAGE);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        sleep(5);

        // 开启来电拦截
        UiObject switcher = device.findObject(new UiSelector()
                .descriptionContains("switch_appinstall"));
        if (!switcher.isChecked()) {
            switcher.click();
        }
    }

    @Test
    public void testCase1() throws UiObjectNotFoundException {
        // 安装应用
        installApk("Halor.apk");

        sleep(10);

        // 等待弹窗
        checkPopupWindow();

        device.findObject(new UiSelector()
                .textContains("DONE").className(Button.class)).click();

        // 安装其他应用，等待弹窗
        installApk("demo.apk");

        sleep(10);

        // 等待弹窗
        checkPopupWindow();

        device.findObject(new UiSelector()
                .textContains("DONE").className(Button.class)).click();

        // 卸载应用，
    }

    private void checkPopupWindow() throws UiObjectNotFoundException {
        UiObject window = device.findObject(new UiSelector().descriptionContains("sdk_popup_window"));
        if (!window.exists()) {
            fail("popup window not exists after app install");
        }
        device.findObject(new UiSelector().descriptionContains("close")).click();
    }

    private void copyApkToSdcard(String filePath) {
        Context context = ApplicationProvider.getApplicationContext();
        File file = new File("mnt/sdcard/test");
        if (!file.exists()) {
            file.mkdir();
        }
        try {
            InputStream inputStream = context.getAssets().open(filePath);
            if (null != inputStream) {
                FileOutputStream fileOutputStream = new FileOutputStream(Environment.
                        getExternalStorageDirectory().getAbsolutePath() + File.separator + "test" + File.separator + filePath);
                byte[] buffer = new byte[1024];
                int readLength;
                while (-1 != (readLength = inputStream.read(buffer))) {
                    fileOutputStream.write(buffer, 0, readLength);
                }
                fileOutputStream.close();
                inputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void sleep(int seconds) {
        try {
            Thread.currentThread().sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    private void installApk(String apkName) throws UiObjectNotFoundException {
        Context context = ApplicationProvider.getApplicationContext();
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.setDataAndType(Uri.fromFile(new File("mnt/sdcard/test/" + apkName)), "application/vnd.android.package-archive");
        } else {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            File file = new File("mnt/sdcard/test/" + apkName);
            Uri contentUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileProvider", file);
            intent.setDataAndType(contentUri, "application/vnd.android.package-archive");
        }
        context.startActivity(intent);
        device.waitForIdle();
        device.findObject(new UiSelector()
                .textContains("INSTALL").className(Button.class)).click();
        device.waitForIdle();
    }
}
