package com.instreet.businessdemo;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiObjectNotFoundException;
import androidx.test.uiautomator.UiSelector;
import androidx.test.uiautomator.Until;

import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class LockscreenTest {
    UiDevice mDevice;
    int LAUNCH_TIMEOUT = 5000;
    private String BASIC_SAMPLE_PACKAGE = "com.instreet.businessdemo";

    @Before
    public void startMainActivityFromHomeScreen() throws UiObjectNotFoundException {
        // Initialize UiDevice instance
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // Start from the home screen
        mDevice.pressHome();

        // Wait for launcher
        final String launcherPackage = mDevice.getLauncherPackageName();
        assertThat(launcherPackage, notNullValue());
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)),
                LAUNCH_TIMEOUT);

        // Launch the app
        Context context = ApplicationProvider.getApplicationContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(BASIC_SAMPLE_PACKAGE);
        // Clear out any previous instances
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        // Wait for the app to appear
        mDevice.wait(Until.hasObject(By.pkg(BASIC_SAMPLE_PACKAGE).depth(0)),
                LAUNCH_TIMEOUT);

        // 开启应用安装监听
        UiObject switcher = mDevice.findObject(new UiSelector()
                .descriptionContains("switch_lockscreen"));
        if (!switcher.isChecked()) {
            switcher.click();
        }
    }

    @Test
    public void testLockscreen() throws InterruptedException {
        try {
            Thread.currentThread().sleep(2000);
            mDevice.sleep();
            Thread.currentThread().sleep(2000);
            mDevice.wakeUp();
            Thread.currentThread().sleep(2000);

            UiObject imageIcon = mDevice.findObject(new UiSelector()
                    .description("lockscreen_icon"));
            if(!imageIcon.exists()){
                fail();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
