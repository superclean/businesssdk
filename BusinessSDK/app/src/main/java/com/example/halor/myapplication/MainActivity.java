package com.example.halor.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import cn.instreet.business.BusinessSDK;
import cn.instreet.business.lockscreen.LockScreenPreferenceActivity;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_DISABLE_KEYGUARD = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        // 申请解除锁屏权限
//        requestKeyguardPermission();
//

//
//        Intent intent = new Intent(this, PhoneCallService.class);
//        startService(intent);

        BusinessSDK.InitSDK(this, new BusinessSDK.OnSDKInitListener() {
            @Override
            public void onInit(BusinessSDK sdk) {
                Intent intent = new Intent(MainActivity.this, LockScreenPreferenceActivity.class);
                startActivity(intent);
            }
        });

    }
}
