package com.example.halor.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

public class AppInstallReceiver extends BroadcastReceiver {
    private Context mContext;
    PackageInfo packageInfo = null;
    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
//        String action = intent.getAction();
//        // 获取包名
//        String packageName = intent.getData().getSchemeSpecificPart();
//        try {
//            //获取应用名
//            packageInfo = mContext.getPackageManager().getPackageInfo(packageName, 0);
//            String appName = packageInfo.applicationInfo.loadLabel(mContext.getPackageManager())
//                    .toString();
//            Log.d(Constants.TAG, "app changed " + appName);
//        } catch (PackageManager.NameNotFoundException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        if (intent.getAction().equals(Intent.ACTION_PACKAGE_ADDED)) {
            String packageName = intent.getData().getSchemeSpecificPart();
            Toast.makeText(context, "安装成功"+packageName, Toast.LENGTH_LONG).show();
        }
        if (intent.getAction().equals(Intent.ACTION_PACKAGE_REMOVED)) {
            String packageName = intent.getData().getSchemeSpecificPart();
            Toast.makeText(context, "卸载成功"+packageName, Toast.LENGTH_LONG).show();
        }
        if (intent.getAction().equals(Intent.ACTION_PACKAGE_REPLACED)) {
            String packageName = intent.getData().getSchemeSpecificPart();
            Toast.makeText(context, "替换成功"+packageName, Toast.LENGTH_LONG).show();
        }
    }
}
