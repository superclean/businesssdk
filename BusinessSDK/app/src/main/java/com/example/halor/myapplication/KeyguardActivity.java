package com.example.halor.myapplication;

import android.app.Activity;
import android.app.KeyguardManager;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import static com.example.halor.myapplication.Constants.TAG;

/**
 * 自定义锁屏页面demo
 */
public class KeyguardActivity extends Activity {

    private Button mButtonKeygurad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

        KeyguardManager manager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        if (null != manager && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // 隐藏锁屏界面
            dismissKeyguard(manager);
        }
        setContentView(R.layout.activity_keyguard);
        mButtonKeygurad = findViewById(R.id.btnKey);
        mButtonKeygurad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void dismissKeyguard(KeyguardManager manager) {
        manager.requestDismissKeyguard(this, new KeyguardManager.KeyguardDismissCallback() {
            @Override
            public void onDismissError() {
                super.onDismissError();
                Log.v(TAG, "onDismissError");
            }

            @Override
            public void onDismissSucceeded() {
                super.onDismissSucceeded();
                Log.v(TAG, "onDismissSucceeded");
            }
        });
    }
}
